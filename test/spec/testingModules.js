beforeEach(inject(function ($rootScope, $compile) {
    elm = angular.element('<input type="text" class="sandata-datepicker" value="">');

    scope = $rootScope;
    $compile(elm)(scope)
    scope.$digest();

}));

it("It should Create a Calendar Widget", inject(function($compile,$rootScope){
    var cals = elm.find(".k-datepicker");
    console.log(cals)
    expect(cals.prevObject.length).toEqual(1);
    expect(cals.prevObject[0]).toContain("input");
}))


/*describe('Testing a thDoes controller', function() {
  var $scope = null;
  var ctrl = null;

  //you need to indicate your module in a test
  beforeEach(module('santrax'));

  beforeEach(inject(function($rootScope, $controller) {
    $scope = $rootScope.$new();

    ctrl = $controller('Cinema', {
      $scope: $scope
    });
  }));

  it('It Should Return 10', function() {
    expect($scope.movies.localMoviesDB.length).toEqual(10);
  });
});*/