/**
 * Created by anthonyc on 1/16/14.
 */

exports.config = {
    specs:[
        './e2e/**/*.test.js'
    ],
    //suites: {
    //    smoke: './UNIT_TEST_LIBRARY/**/*.test.js',
    //    full: './UNIT_TEST_LIBRARY/**/*.test.js'
    //},
    multiCapabilities: [{
        name: "Windows 7 IE 8",
        platform:"Windows 7",
        enableElementCacheCleanup:true,
        ie:{ensureCleanSession:true},
        version:"8.0",
        'browserName': 'internet explorer' // 'firefox' or 'safari' or 'chrome'


    },{
        name: "Windows 7 IE 9",
        platform:"Windows 7",
        enableElementCacheCleanup:true,
        ie:{ensureCleanSession:true},
        version:"9.0",
        'browserName': 'internet explorer'


    },{
        name: "Windows 7 IE 10",
        platform:"Windows 7",
        enableElementCacheCleanup:true,
        ie:{ensureCleanSession:true},
        version:"10.0",
        'browserName': 'internet explorer'


    },{
        name: "Windows 7 Firefox",
        platform:"Windows 7",
        'browserName': 'firefox'


    },{
        name: "Windows 7 Chrome",
        platform:"Windows 7",
        'browserName': 'chrome'


    }],

    //Use this for testing in multiple browser at once
    chromeOnly: false,
   /* multiCapabilities: [{
        device:"win7",
        'browserName': 'firefox'
    }, {
        'browserName': 'chrome'
    }],*/
    //Sauce Labs Credentials.
    sauceUser: "subant05",
    sauceKey: "23515038-09fb-4ec4-8631-831a15cf522f",
    allScriptsTimeout: 1200000,
   jasmineNodeOpts:{defaultTimeoutInterval : 1200000},

    baseUrl : 'http://localhost:8081/portal/index#/'
};