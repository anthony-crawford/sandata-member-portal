/**
 * Created by anthonyc on 4/16/14.
 */
describe("Visits",function(){

    ptor = protractor.getInstance();
    browser.get("/portal/index.html#/?su=N61V74XtxuzNkh6hiWfuLv3mYkJcpHpr&sp=XOlU0m949l0%3D&soid=wLND8iY6wZc%3D");



    describe("Get Page Title ", function(){
        it("should display the correct title - Dashboard", function(){

            expect(browser.getTitle()).toBe("Santrax — Dashboard");
        });

    });

    describe("Go To Visit Page", function(){

        it("should direct user to Visit page with a title - Visits", function(){

            /* ptor.actions().mouseMove(ptor.findElement(protractor.By.xpath('//*[@id="sidebar-left"]/div/ul/li[2]/a'))).perform();
             ptor.element.all(by.css('button#unknownCallConfirmButton_0')).then(function(elm){
             elm[0].click();
             });*/
            element(by.css("a[href='#/visits']")).click();
            expect(browser.getTitle()).toBe("Santrax — Visits");

        });

    });


    var old_adjIn = element(by.repeater('row in Visits.VISIT_GRID.grid_row_components  | orderBy:columns:reverse').
        row(0).column('{{row.components.visitStartTimeLabelComponent.text}}'));
    var old_adjOut = element(by.repeater('row in Visits.VISIT_GRID.grid_row_components  | orderBy:columns:reverse').
        row(0).column('{{row.components.visitEndTimeLabelComponent.text}}'));

    describe("Select Edit Visit Button", function(){
        it("There Should be some Visits edit", function(){
            expect(element.all(by.repeater('row in Visits.VISIT_GRID.grid_row_components  | orderBy:columns:reverse')).count()).toBeGreaterThan(0);
        });

        it("click edit button on first visit and modal should pop up", function(){
            element(by.xpath('//*[@id="content"]/div[4]/div/span/div[2]/table/tbody/tr[1]/td[8]/button[5]')).click();

            var modalHeader = element(by.xpath('//*[@id="visitModalFocus"]')).getText();
            expect(modalHeader).toContain("Edit");

        });

    });

    describe("Enter AdjIn", function(){
        it("send text to adj time in input field", function(){
            var adjIn ="12:00 AM";
            element(by.css('input#VisitADJIn')).clear();
            element(by.css('input#VisitADJIn')).sendKeys(adjIn);
            expect(element(by.model("Visits.VisitADJIn")).getAttribute('value') ).toEqual(adjIn);
        });
    });

    describe("Enter AdjOut", function(){
        it("send text to adj time out input field", function(){
            var adjOut ="12:00 PM";
            element(by.css('input#VisitADJOut')).clear();
            element(by.css('input#VisitADJOut')).sendKeys(adjOut);
            expect(element(by.model("Visits.VisitADJOut")).getAttribute('value') ).toEqual(adjOut);
        });
    });

    describe("Change Reason on Visit", function(){
        it("should change the reason", function(){
            element(by.model('Visits.VisitModal.addVisitReasonsModel')).click();
            element.all(by.css("select#selectReasonCode option")).then(function(arr){
                arr[2].click();
            })
        })
    })

    describe("Submit Edit", function(){
        it("should click the update button", function(){
            element(by.xpath('//*[@id="ng-app"]/body/div[3]/div[2]/span/span/span/span/button[1]')).click();
        })
    })


    var new_adjIn = element(by.repeater('row in Visits.VISIT_GRID.grid_row_components  | orderBy:columns:reverse').
        row(0).column('{{row.components.visitStartTimeLabelComponent.text}}'));
    var new_adjOut = element(by.repeater('row in Visits.VISIT_GRID.grid_row_components  | orderBy:columns:reverse').
        row(0).column('{{row.components.visitEndTimeLabelComponent.text}}'));

    expect(new_adjIn).not.toEqual(old_adjIn)
    expect(new_adjOut).not.toEqual(old_adjOut)

    it("Go To Message Center.", function(){
        ptor.actions().mouseMove(ptor.findElement(protractor.By.xpath('//*[@id="messageTab"]/span/span/span/div'))).perform();
    });

    it("There Should be message in the message center", function(){
        element.all(by.repeater('message in messageCenter')).then(function(arr){
            var lastIndex = arr.length - 1;
            var message = arr[lastIndex].getText()
            expect(message).toContain("Visit has been updated.");
        });
    });


});

