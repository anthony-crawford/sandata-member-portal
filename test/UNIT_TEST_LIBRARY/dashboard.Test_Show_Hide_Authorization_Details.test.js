/**
 * Created by anthonyc on 3/12/14.
 */

describe("Dashboard",function(){

    ptor = protractor.getInstance();
    browser.get("/portal/index.html#/?su=N61V74XtxuzNkh6hiWfuLv3mYkJcpHpr&sp=XOlU0m949l0%3D&soid=wLND8iY6wZc%3D");



    describe("Get Page Title ", function(){
        it("should display the correct title", function(){

            expect(browser.getTitle()).toBe("Santrax — Dashboard");
        });

    });



    describe("Show and Hide Details Section in Auths table ", function(){
        it("click toggle button should change arrow", function(){
            element( by.css("a#service-details_0") ).click();
            var clientLoaded = element(by.css("tr#details-template td[headers='header_auth_0']")).getText();
            expect( clientLoaded).toContain( 'Reference'  );

        });
    });


});
