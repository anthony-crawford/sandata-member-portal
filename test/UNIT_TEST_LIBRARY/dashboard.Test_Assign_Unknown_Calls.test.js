/**
 * Created by anthonyc on 3/12/14.
 */

describe("Dashboard",function(){

    ptor = protractor.getInstance();
    browser.get("/portal/index.html#/?su=N61V74XtxuzNkh6hiWfuLv3mYkJcpHpr&sp=XOlU0m949l0%3d&soid=wLND8iY6wZc%3d");



    describe("Get Page Title ", function(){
        it("should display the correct title", function(){

            expect(browser.getTitle()).toBe("Santrax — Dashboard");
        });

    });



    describe("Change Client ", function(){
        it("should get value of for clientModel", function(){
            element( by.model("Dashboard.clientModel")    ).click();
            element( by.css("option[value='07128766']") ).click();
            var clientLoaded = element(by.css("select#clientModelSelect_0")).getText();
            expect( clientLoaded ).not.toEqual( '07128766'  );

        });
    });

    describe("Assign Unknown Call ", function(){
        it("There Should be some unknown calls to assign", function(){
            expect(element.all(by.repeater('row in currentlyShownUnknownCalls')).count()).toBeGreaterThan(0);
        });

        it("Retrieve the first row from Unknown calls", function(){
            var unknownCall  = element.all(by.repeater('row in currentlyShownUnknownCalls')).get(0);
            element( by.css("select#clientModelSelect_0")       ).click();
            element( by.css("option[value='07128766']")         ).click();
            var clientLoaded = element(by.css("select#clientModelSelect_0")).getText();
        });



        it("Click confirm button for unknown call.", function(){


            ptor.actions().mouseMove(ptor.findElement(protractor.By.xpath('//*[@id="unknown-calls"]/tbody/tr[1]/td[9]'))).perform();
            ptor.element.all(by.css('button#unknownCallConfirmButton_0')).then(function(elm){
                elm[0].click();
            });

        });

        it("Should get an error.", function(){
            expect(element(by.css("ul.parsley-error-list li.custom-error-message")).getText()).toContain("Please select a reason code.");

        });

        it("Select Reason", function(){
            element( by.css("select#reasonModelSelect_0")       ).click();
            element( by.css("option[value='01']")         ).click();
        });

        it("Click confirm button for unknown call.", function(){


            ptor.actions().mouseMove(ptor.findElement(protractor.By.xpath('//*[@id="unknown-calls"]/tbody/tr[1]/td[9]'))).perform();
            ptor.element.all(by.css('button#unknownCallConfirmButton_0')).then(function(elm){
                elm[0].click();
            });
        });


        it("Should open a confirmation dialog", function(){
            expect(element(by.xpath('//*[@id="ng-app"]/body/div[3]')).getText()).toContain("Confirmation");
        });

        it("Should open a confirmation dialog", function(){
            ptor.actions().mouseMove(ptor.findElement(protractor.By.xpath('//*[@id="ng-app"]/body/div[3]'))).perform();
            ptor.element.all(by.css("button[title='OK']")).then(function(elm){
                elm[0].click();
            });
        });

        it("Go To Message Center.", function(){
            ptor.actions().mouseMove(ptor.findElement(protractor.By.xpath('//*[@id="widgets-area"]/span/span/span/div'))).perform();
        });

        /*it("There Should be message in the message center", function(){
         var message = element.all(by.repeater('message in messageCenter')).evaluate("messageCenter");
         expect(JSON.stringify(message.getText())).toContain("Client was successfully assigned to the selected visit.");


         });*/

    });


});
