/**
 * Created by anthonyc on 1/16/14.
 */

describe("Dashboard",function(){

    ptor = protractor.getInstance();
    function selectOption(selector, item){
        var selectList, desiredOption;

        selectList = this.findElement(selector);
        selectList.click();

        selectList.findElements(protractor.By.tagName('option'))
            .then(function findMatchingOption(options){
                options.some(function(option){
                    option.getText().then(function doesOptionMatch(text){
                        if (item === text){
                            desiredOption = option;
                            return true;
                        }
                    });
                });
            })
            .then(function clickOption(){
                if (desiredOption){
                    desiredOption.click();
                }
            });
    }
    ptor.selectOption = selectOption.bind(ptor);
    browser.get("/portal/index.html#/?su=N61V74XtxuzNkh6hiWfuLv3mYkJcpHpr&sp=XOlU0m949l0%3D&soid=wLND8iY6wZc%3D");



    describe("Get Page Title ", function(){
       it("should display the correct title", function(){

            expect(browser.getTitle()).toBe("Santrax — Dashboard");
       });

    });



    describe("Change Client ", function(){
        it("should get value of for clientModel", function(){
            var firstLoad,secondLoad, initialIndex =0;
            browser.driver.isElementPresent(by.css("select#clientModelSelect_0")).then(function(present){
                if(!present){
                    element( by.model("Dashboard.clientModel")    ).click();
                    element( by.css("select#clientSelect option[value='07181093']") ).click();
                }
                var firstLoad =  element(by.css("select#clientModelSelect_0")).getText();
                element( by.model("Dashboard.clientModel")    ).click();
                element( by.css("option[value='07128766']") ).click();
                var secondLoad = element(by.css("select#clientModelSelect_0")).getText();
                if(firstLoad =='undefined' && secondLoad =='undefined'){
                    expect( firstLoad ).not.toEqual( secondLoad  );
                }else{
                    expect( firstLoad ).not.toEqual( secondLoad  );
                }

                //expect(present).toBe(false);
            })


        });
    });






});