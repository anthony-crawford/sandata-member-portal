/**
 * Created by anthonyc on 4/16/14.
 */
describe("Visits",function(){

    ptor = protractor.getInstance();
    browser.get("/portal/index.html#/?su=N61V74XtxuzNkh6hiWfuLv3mYkJcpHpr&sp=XOlU0m949l0%3D&soid=wLND8iY6wZc%3D");



    describe("Get Page Title ", function(){
        it("should display the correct title - Dashboard", function(){

            expect(browser.getTitle()).toBe("Santrax — Dashboard");
        });

    });

    describe("Go To Visit Page", function(){

        it("should direct user to Visit page with a title - Visits", function(){

            /* ptor.actions().mouseMove(ptor.findElement(protractor.By.xpath('//*[@id="sidebar-left"]/div/ul/li[2]/a'))).perform();
             ptor.element.all(by.css('button#unknownCallConfirmButton_0')).then(function(elm){
             elm[0].click();
             });*/
            element(by.css("a[href='#/visits']")).click();
            expect(browser.getTitle()).toBe("Santrax — Visits");

        });

    });

    describe("Select Employee", function(){

        it("should be able to select a new employee and update grid", function(){

            element( by.model("employeeModel")    ).click();
            ptor.waitForAngular();
            var lEmployeeOptions = element.all(by.css('select#employee option'));
            lEmployeeOptions.get(4).click()
            ptor.waitForAngular();
            var firstRow = element(by.xpath('//*[@id="content"]/div[4]/div/span/div[2]/table/tbody/tr[1]'))
            expect(firstRow.isPresent()).toEqual(true);
            /*lEmployeeOptions.then(function(arr){
             function isRowThere(){
             this.present = 0;

             }
             isRowThere.prototype.check = function(){
             var that = this;
             ptor.findElements(protractor.By.css('#content > div:nth-child(7) > div > span > div.box-content.no-padding.ng-scope > table > tbody > tr:nth-child(1)')).then(function(arr){
             console.log(arr.length);
             that.present = arr.length;
             console.log("Inside Present : ",that.present)
             return that.present
             });
             console.log("Outside Present : ",that.present)
             }

             isRowThere.prototype.doubleChceck = function(){
             return this.present;
             }

             for(var i=0; i < arr.length; i++){
             arr[i].click()
             ptor.waitForAngular();
             var row = new isRowThere();
             row.check();
             ptor.waitForAngular();
             console.log("NOW IS IT THERE : ", row.doubleChceck() );


             }
             })**/

            /*for(var i =2; i<12;i++){
             // var exits = element(by.xpath('//*[@id="content"]/div[4]/div/span/div[2]/table/tbody/tr[1]')).isPresent();
             var rows = element.all(by.repeater('row in VISIT_GRID.grid_row_components  | orderBy:columns:reverse'));
             if(rows.count() > 0 ){
             break;
             }else{
             runThroughEmployees(i);
             }

             }*/

        });

    });

    var nonupdatedColumn = element(by.repeater('row in VISIT_GRID.grid_row_components  | orderBy:columns:reverse').
        row(0).column('{{row.components.visitServiceNameLabelComponent.text}}'));

    describe("Select Edit Visit Button", function(){
        it("There Should be some Visits edit", function(){
            expect(element.all(by.repeater('row in VISIT_GRID.grid_row_components  | orderBy:columns:reverse')).count()).toBeGreaterThan(0);
        });

        it("click edit button on first visit and modal should pop up", function(){
            element(by.xpath('//*[@id="content"]/div[4]/div/span/div[2]/table/tbody/tr[1]/td[8]/button[5]')).click();

            var modalHeader = element(by.xpath('//*[@id="visitModalFocus"]')).getText();
            expect(modalHeader).toContain("Edit");

        });

    });

    describe("Change Service on Visit", function(){
        it("should change the service", function(){
            var oldvalue = element(by.model('VisitModal.addVisitService')).getText();
            element(by.model('VisitModal.addVisitService')).click();
            element.all(by.css("select#addVisitService option")).then(function(arr){
                arr[2].click();
            })
            var newvalue = element(by.model('VisitModal.addVisitService')).getText();
            expect(oldvalue).not.toEqual(newvalue);
        })
    })

    describe("Change Reason on Visit", function(){
        it("should change the reason", function(){
            element(by.model('VisitModal.addVisitReasonsModel')).click();
            element.all(by.css("select#selectReasonCode option")).then(function(arr){
                arr[2].click();
            })
        })
    })

    describe("Submit Edit", function(){
        it("should click the update button", function(){
            element(by.xpath('//*[@id="ng-app"]/body/div[3]/div[2]/span/span/span/span/button[1]')).click();
        })
    })


            var updatedColumn = element(by.repeater('row in VISIT_GRID.grid_row_components  | orderBy:columns:reverse').
                row(0).column('{{row.components.visitServiceNameLabelComponent.text}}'));
            expect(updatedColumn).not.toEqual(nonupdatedColumn)


    it("Go To Message Center.", function(){
        ptor.actions().mouseMove(ptor.findElement(protractor.By.xpath('//*[@id="messageTab"]/span/span/span/div'))).perform();
    });

    it("There Should be message in the message center", function(){
        element.all(by.repeater('message in messageCenter')).then(function(arr){
            var lastIndex = arr.length - 1;
            var message = arr[lastIndex].getText()
            expect(message).toContain("Visit has been updated.");
        });
    });


});

