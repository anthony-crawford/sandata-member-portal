/**
 * Created by anthonyc on 4/16/14.
 */
describe("Visits",function(){

    ptor = protractor.getInstance();
    browser.get("/portal/index.html#/?su=N61V74XtxuzNkh6hiWfuLv3mYkJcpHpr&sp=XOlU0m949l0%3D&soid=wLND8iY6wZc%3D");



    describe("Get Page Title ", function(){
        it("should display the correct title - Dashboard", function(){

            expect(browser.getTitle()).toBe("Santrax — Dashboard");
        });

    });

    describe("Go To Visit Page", function(){

        it("should direct user to Visit page with a title - Visits", function(){

            /* ptor.actions().mouseMove(ptor.findElement(protractor.By.xpath('//*[@id="sidebar-left"]/div/ul/li[2]/a'))).perform();
             ptor.element.all(by.css('button#unknownCallConfirmButton_0')).then(function(elm){
             elm[0].click();
             });*/
            element(by.css("a[href='#/visits']")).click();
            expect(browser.getTitle()).toBe("Santrax — Visits");

        });

    });

    describe("There Should be visit added to the grid", function(){
        element.all(by.repeater('row in VISIT_GRID.grid_row_components  | orderBy:columns:reverse')).then(function(arr){

            var initialCount = arr.length;

            describe("Launch Add Visit", function(){
                it("click edit button on add visit button", function(){
                    element(by.xpath('//*[@id="content"]/div[3]/div/form/span/button[2]')).click();

                    var modalHeader = element(by.xpath('//*[@id="visitModalFocus"]')).getText();
                    expect(modalHeader).toContain("Add");
                });
            });

            describe("Enter Date", function(){
                it("send text to date input field", function(){
                    var date = new Date();
                    var currentDate =(date.getMonth() + 1) + '/' + (date.getDate()-1) + '/' +  date.getFullYear() ;
                    element(by.model("VisitDate")).sendKeys(currentDate);
                    expect(element(by.model("VisitDate")).getAttribute('value') ).toEqual(currentDate);
                });
            });

            describe("Enter AdjIn", function(){
                it("send text to adj time in input field", function(){
                    var adjIn ="12:00 AM";
                    element(by.xpath('//*[@id="visitModalForm"]/div/ul/li[4]/div/div/div[4]/div/span/button')).click();
                    element(by.xpath('//*[@id="visitModalForm"]/div/ul/li[4]/div/div/div[4]/div/span/button')).click();
                    expect(element(by.model("VisitADJIn")).getAttribute('value') ).toEqual(adjIn);
                });
            });

            describe("Enter AdjOut", function(){
                it("send text to adj time out input field", function(){
                    var adjOut ="12:00 AM";
                    element(by.xpath('//*[@id="visitModalForm"]/div/ul/li[4]/div/div/div[5]/div/span/button')).click();
                    element(by.xpath('//*[@id="visitModalForm"]/div/ul/li[4]/div/div/div[5]/div/span/button')).click();
                    expect(element(by.model("VisitADJOut")).getAttribute('value') ).toEqual(adjOut);
                });
            });

            describe("Adj Hours Total Changes", function(){
                it("send text to adj hours total should equal 24.00", function(){
                    var adjTotal = "24.00";
                    expect(element(by.xpath('//*[@id="visitModalForm"]/div/ul/li[4]/div/div/div[6]/div')).getText()).toContain(adjTotal);
                });
            });

            describe("Change Reason on Visit", function(){
                it("should change the reason", function(){
                    element(by.model('VisitModal.addVisitReasonsModel')).click();
                    element.all(by.css("select#selectReasonCode option")).then(function(arr){
                        arr[2].click();
                    });
                });
            });

            describe("Submit Add Visit", function(){
                it("should click the save button", function(){
                    element(by.xpath('//*[@id="ng-app"]/body/div[3]/div[2]/span/span/span/span/button[1]')).click();
                });
            });

            ptor.waitForAngular();
            describe("Select Client", function(){

                it("should be able to select a new client", function(){
                    element( by.model("clientModel")    ).click();
                });

            });

             element.all(by.css('table tbody tr')).then(function(newarr){
                    expect(newarr.length).toBeGreaterThan(initialCount);
            })

        });
    });

});

