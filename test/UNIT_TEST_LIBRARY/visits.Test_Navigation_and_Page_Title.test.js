/**
 * Created by anthonyc on 4/16/14.
 */
describe("Visits",function(){

    ptor = protractor.getInstance();
    browser.get("/portal/index.html#/?su=N61V74XtxuzNkh6hiWfuLv3mYkJcpHpr&sp=XOlU0m949l0%3D&soid=wLND8iY6wZc%3D");



    describe("Get Page Title ", function(){
        it("should display the correct title - Dashboard", function(){

            expect(browser.getTitle()).toBe("Santrax — Dashboard");
        });

    });

    describe("Go To Visit Page", function(){

        it("should direct user to Visit page with a title - Visits", function(){

               /* ptor.actions().mouseMove(ptor.findElement(protractor.By.xpath('//*[@id="sidebar-left"]/div/ul/li[2]/a'))).perform();
                ptor.element.all(by.css('button#unknownCallConfirmButton_0')).then(function(elm){
                    elm[0].click();
                });*/
                element(by.css("a[href='#/visits']")).click();
                expect(browser.getTitle()).toBe("Santrax — Visits");

        });

    });

});

