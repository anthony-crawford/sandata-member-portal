/**
 * Created by anthonyc on 4/16/14.
 */
describe("Visits",function(){

    ptor = protractor.getInstance();
    browser.get("/portal/index.html#/?su=N61V74XtxuzNkh6hiWfuLv3mYkJcpHpr&sp=XOlU0m949l0%3D&soid=wLND8iY6wZc%3D");



    describe("Get Page Title ", function(){
        it("should display the correct title - Dashboard", function(){

            expect(browser.getTitle()).toBe("Santrax — Dashboard");
        });

    });

    describe("Go To Visit Page", function(){

        it("should direct user to Visit page with a title - Visits", function(){

            /* ptor.actions().mouseMove(ptor.findElement(protractor.By.xpath('//*[@id="sidebar-left"]/div/ul/li[2]/a'))).perform();
             ptor.element.all(by.css('button#unknownCallConfirmButton_0')).then(function(elm){
             elm[0].click();
             });*/
            element(by.css("a[href='#/visits']")).click();
            expect(browser.getTitle()).toBe("Santrax — Visits");

        });

    });

    describe("Select Employee", function(){

        it("should be able to select a new employee", function(){
            var oldValue = element( by.model("employeeModel")    ).getText();
            element( by.model("employeeModel")    ).click();
            element( by.css("select#employee option:nth-of-type(2)") ).click();
            var newValue = element( by.model("employeeModel")    ).getText();
            expect(oldValue).not.toEqual(newValue);

        });

    });



});

