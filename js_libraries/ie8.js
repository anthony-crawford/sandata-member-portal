/*
*   IE 8 work around for  missing or poorly implemented functions 
*/


//indexOf for IE 8 support
if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

//filter for IE 8 support
if(!Array.prototype.filter){
    Array.prototype.filter=function(g,f,j,i,h){
        j=this;i=[];
        for(h in j){~~h+""==h&&h>=0&&g.call(f,j[h],+h,j)&&i.push(j[h])}
        return i
    }
}
