/**
 * Created by anthonyc on 5/6/14.
 */
var parsleyDir = angular.module("parsley",[]);
parsleyDir.directive("parsleyRequiredField",function($timeout){

    return {

        controller: function($scope,$element,$attrs,$parse,$timeout){
            var bElementExistInForm = false;
            this.id;
            this.checkIfNodeExists = function(){

                var result = $element.closest('form').parsley().items.filter(function( obj ) {
                    if(obj.$element.context.id == $attrs.id){
                        return obj;
                    }
                });
                if(result.length > 0){
                    bElementExistInForm = true;
                }
                return bElementExistInForm;
            }

            this.getElementSelector = function(){

                if($attrs.id && $attrs.id != ""){
                    this.id = $($element).prop('tagName');
                    this.id = this.id.toLowerCase() + "#" + $attrs.id;


                }else if($attrs.name && $attrs.name != ""){
                    this.id = $($element).prop('tagName');
                    this.id = this.id.toLowerCase() + "[name='" + $attrs.name.trim() + "']";

                }else{ this.id = null;
                        //console.log("ID or Name attribute must be associated with parselyRequireField Directive for element:",$element);
                }


                return this.id;
            }

            this.removeItemFromParsley = function(){
                var id = this.getElementSelector();
                var exists = this.checkIfNodeExists();
                if(exists){
                    $element.closest('form').parsley('removeItem', id);
                }
            }

            this.addItemFromParsley = function(){
                var id = this.getElementSelector();
                var exists = this.checkIfNodeExists();
                if(exists){
                    $element.closest('form').parsley('addItem', id);
                }
            }

            this.toggleRequiredField = function(bVal){
                var that = this;
                if(bVal){
                    //console.log(bVal)
                    $timeout(function(){
                        that.removeItemFromParsley();
                        $element.attr("required","true"  );
                        that.addItemFromParsley();
                    },0);
                }else{
                    //console.log(bVal)
                    $timeout(function(){
                        $element.removeAttr("required"  );
                        that.removeItemFromParsley();
                    },0);
                }
            }

            this.setup = function(){
                if(this.getElementSelector() != null){

                    $element.removeAttr("required");
                    var requiredModel = $parse($attrs.parsleyRequiredField);
                    var required = angular.copy( requiredModel($scope));

                    this.removeItemFromParsley();

                    this.toggleRequiredField(required);
                }
            }
        },
        link: function(scope, element, attrs,ctrls){

                ctrls.setup();

                scope.$watch(attrs.parsleyRequiredField, function(val){
                    if(ctrls.getElementSelector() != null){
                        ctrls.removeItemFromParsley();
                        ctrls.toggleRequiredField(val);
                    }

                });


        }
    }
});
