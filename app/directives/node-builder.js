var builder = angular.module("builder",[])


builder.directive("sandataCheckbox", function ($parse,$timeout) {
    return {
        controller: function($scope,$element,$attrs,$parse,$timeout){

            this.checkbox = $($($element).children()[0]);
            this.label = $($($element).children()[1]);
            this.checkBoxGif = $(this.label).children([0]);

            if($attrs.family  || $attrs.parent){
                this.familyModel = $parse($attrs.family);
                this.family = this.familyModel($scope);
            }

            if($attrs.family || $attrs.parent){
                this.broadcaster =  $attrs.family + "_update_all";
            }

            if(!$attrs.parent && $attrs.family){
                this.keyLabels = $attrs.keyLabels.split(",");
                this.keys = $attrs.keyValues.split(',');
                var tmp_keyModel;
                for(var i = 0; i < this.keys.length; i++){
                    tmp_keyModel = $parse(this.keys[i])
                    this.keys[i] = angular.copy(tmp_keyModel($scope));
                }

                this.slot ="";

                if($attrs.adopt){
                    this.dataModel = $parse($attrs.adopt);
                    this.data = this.dataModel($scope);
                    if($attrs.isChecked && $attrs.isChecked === "true" ){
                        this.data.checked =  true;
                    }else{
                        this.data.checked = false;
                    }


                    this.slot = (this.family.length - 1);
                    this.family.push(this.data);
                }
            }


            this.parentBeforeCheckboxes = function(){
                this.familyModel = $parse($attrs.family);
                this.family = this.familyModel($scope);
            }

            this.getFamily = function(){
                return this.family;
            }

            this.getFamilyPosition = function(){
                var position ="";
                for(var i = 0; i < this.family.length; i++){
                    isThisTheCheckboxChecked = false;
                    for(var a = 0; a < this.keyLabels.length; a++){
                        if(this.family[i] && this.family[i][this.keyLabels[a]] && this.family[i][this.keyLabels[a]] == this.keys[a]){
                            isThisTheCheckboxChecked = true;
                        }else{
                            isThisTheCheckboxChecked = false;
                        }
                    }

                    if(isThisTheCheckboxChecked){
                        position = i;
                        break;
                    }
                }
                return position;
            }

            this.updateCheckbox = function(bValue){
                var isThisTheCheckboxChecked;
                if(typeof bValue != "undefined" && typeof bValue == "boolean" ){
                    if($attrs.parent && $attrs.family){
                        if(this.family.length ==0){
                            this.parentBeforeCheckboxes();
                        }
                        for(var i = 0; i < this.family.length; i++){
                            this.family[i].checked = bValue;
                        }
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        }
                        $scope.$broadcast(this.broadcaster);
                    }else if($attrs.family && !$attrs.parent){
                        for(var i = 0; i < this.family.length; i++){
                            isThisTheCheckboxChecked = false;
                            for(var a = 0; a < this.keyLabels.length; a++){
                                if(this.family[i] && this.family[i][this.keyLabels[a]] && this.family[i][this.keyLabels[a]] == this.keys[a]){
                                    isThisTheCheckboxChecked = true;
                                }else{
                                    isThisTheCheckboxChecked = false;
                                }
                            }

                            if(isThisTheCheckboxChecked){
                                this.family[i]["checked"] = bValue;
                                if(!$scope.$$phase) {
                                    $scope.$apply();
                                }
                                break;
                            }
                        }
                    }else{}



                    this.checkbox.prop("checked", bValue);

                    if ($.browser.version < 10.0 && !this.checkbox.prop("checked")) {
                        $(this.checkBoxGif).css("background-position", "0px 0px");
                    } else if ($.browser.version < 10.0 && this.checkbox.prop("checked")) {
                        $(this.checkBoxGif).css("background-position", "-27px 0px");
                    } else { }
                }else{
                    if($attrs.parent && $attrs.family){
                        if(this.family.length ==0){
                            this.parentBeforeCheckboxes();
                        }
                        for(var i = 0; i < this.family.length; i++){
                            this.family[i].checked = !this.checkbox.prop("checked");
                        }
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        }
                        $scope.$broadcast(this.broadcaster);
                    }else if($attrs.family && !$attrs.parent){
                        for(var i = 0; i < this.family.length; i++){
                            isThisTheCheckboxChecked = false;
                            for(var a = 0; a < this.keyLabels.length; a++){
                                if(this.family[i] && this.family[i][this.keyLabels[a]] && this.family[i][this.keyLabels[a]] == this.keys[a]){
                                    isThisTheCheckboxChecked = true;
                                }else{
                                    isThisTheCheckboxChecked = false;
                                }
                            }

                            if(isThisTheCheckboxChecked){
                                this.family[i]["checked"] = !this.checkbox.prop("checked");
                                if(!$scope.$$phase) {
                                    $scope.$apply();
                                }
                                break;
                            }
                        }
                    }else{}



                    this.checkbox.prop("checked", !this.checkbox.prop("checked"));

                    if ($.browser.version < 10.0 && !this.checkbox.prop("checked")) {
                        $(this.checkBoxGif).css("background-position", "0px 0px");
                    } else if ($.browser.version < 10.0 && this.checkbox.prop("checked")) {
                        $(this.checkBoxGif).css("background-position", "-27px 0px");
                    } else { }
                }


            }

            this.updateGraphic = function(bValue){
                this.checkbox.prop("checked", bValue);

                if ($.browser.version < 10.0 && bValue == false) {
                    $(this.checkBoxGif).css("background-position", "0px 0px");
                } else if ($.browser.version < 10.0 && bValue == true) {
                    $(this.checkBoxGif).css("background-position", "-27px 0px");
                } else { }
            }

            this.focusElement = function(){$(this.checkBoxGif).css("border", "1px #000 dotted");}

            this.blurElement = function(){$(this.checkBoxGif).css("border", "none");}
        },
        replace: true,
        template: '<span data-role="checkbox" tabindex="0"  aria-checked="{{isChecked}}" aria-label="{{adalabel}}" title="{{adalabel}}"><input type="checkbox" id="{{name}}" ng-class="{{class}}" name="{{name}}" value="{{value}}" title="{{adalabel}}" aria-label="{{adalabel}}"/><label for="{{name}}" ng-model="sandataModel"><span></span>{{label}}</label></span>',
        link: function (scope, element, attrs, ctrls) {

            if(!attrs.parent && attrs.family){
                scope.$on(ctrls.broadcaster, function(){
                    var checkboxFamilyPosition = ctrls.getFamilyPosition();
                    ctrls.updateGraphic( ctrls.family[checkboxFamilyPosition].checked)
                })
            }

            if(attrs.family){
                scope.$watch(attrs.family,function(){
                    if(attrs.parent){
                        ctrls.parentBeforeCheckboxes();
                    }
                },true);
            }



            if(attrs.isChecked && typeof attrs.isChecked === "string"){
                attrs.$observe('isChecked',function(){
                    if(attrs.isChecked.trim().toLowerCase() === "true"){
                        var yes = true;
                        ctrls.updateCheckbox(yes);
                    }else if(attrs.isChecked.trim().toLowerCase() === "false"){
                        var no = false;
                        ctrls.updateCheckbox(no);
                    }else{
                        scope.$watch(attrs.isChecked.trim(),function(scopedChecked){
                            if(scopedChecked == true){
                                var yes = true;
                                ctrls.updateCheckbox(yes);
                            }else if(scopedChecked == false){
                                var no = false;
                                ctrls.updateCheckbox(no);
                            }else{}
                        })
                    }
                });
            }



            //Click Event
            $(element).click(function () {
                ctrls.updateCheckbox();
            });

            //Keyup Event
            $(element).on("keyup", function (evt) {
                if (evt.keyCode == 32) {
                    $(element).click();
                }
            });

            //Focus Event
            $(element).focus(function () {
                ctrls.focusElement();
            })

            //Blur Event
            $(element).blur(function () {
                ctrls.blurElement();
            })
        }

    }

});

builder.directive("sandataOverrideCheckbox", function ($parse,$timeout) {
    return {
        require: ["?ngClick", "?ngModel"],
        restrict: "AC",
        scope: {
            isChecked: "@",
            name: "@",
            click: "&",
            sandataModel: "=",
            value: "@",
            adalabel:"@",
            "class": "@",
            isReadonly:"@"

        },
        replace: true,
        template: '<span ng-click="click()" tabindex="0" aria-checked="{{isChecked}}" aria-label="{{adalabel}}" title="{{adalabel}}"><input type="checkbox" id="{{name}}" class="{{class}}" name="{{name}}" value="{{value}}" title="{{adalabel}}" aria-label="{{adalabel}}"/><span class="checkboxGraphic"></span></span>',
        link: function (scope, element, attrs) {

            var checkbox = $($(element).children()[0]);
            var checkBoxGif = $($(element).children()[1]);


            if ($.browser.version < 9.0) {
                $(checkBoxGif).html("&#xf096;&nbsp;");
            }

            if(attrs.color){
                $(checkBoxGif).css("color",attrs.color);
            }

            if (attrs.isChecked && attrs.isChecked == "true") {
                checkbox.prop("checked", true);
                // console.log("Checkbox Set To: ",checkbox.prop("checked"))

                if ($.browser.version < 9.0 && !checkbox.prop("checked")) {
                    $(checkBoxGif).html("&#xf096;&nbsp;");
                }else if ($.browser.version < 9.0 && checkbox.prop("checked")) {
                    $(checkBoxGif).html("&#xf046;&nbsp;");
                }else { }

            }else{

            }

            if(attrs.isReadonly && attrs.isReadonly == "true"){

            }else{
                $(element).click(function () {

                    // console.log("Whats the status: ",checkbox.prop("checked"))
                    checkbox.prop("checked", !checkbox.prop("checked"));
                    attrs.isChecked = checkbox.prop("checked");
                    // console.log("Whats the status after: ",checkbox.prop("checked"))

                    if ($.browser.version < 9.0 && !checkbox.prop("checked")) {
                        $(checkBoxGif).html("&#xf096;&nbsp;");
                    }else if ($.browser.version < 9.0 && checkbox.prop("checked")) {
                        $(checkBoxGif).html("&#xf046;&nbsp;");
                    }else { }

                });


                $(element).on("keyup", function (evt) {
                    if (evt.keyCode == 32 || evt.keyCode ==68 ) {
                        $(element).click();
                    }
                });
            }



            $(element).focus(function () {
                $(checkBoxGif).css("border", "1px #000 dotted");
            })
            $(element).blur(function () {
                $(checkBoxGif).css("border", "none");
            })

            //if(attrs.sandataModel){console.log("Whats the Model?: ",attrs.sandataModel)}
            // var scopedCheckedModel = scope.$parse(attrs.sandataModel)

        }

    }

});

builder.directive("sandataCheckboxNoLabel", function ($parse,$timeout) {
    return {
        require: ["?ngClick", "?ngModel"],
        restrict: "AC",
        scope: {
            isChecked: "@",
            name: "@",
            click: "&",
            sandataModel: "=",
            value: "@",
            adalabel:"@",
            "class": "@"

        },
        replace: true,
        template: '<span ng-click="click()" tabindex="0" aria-checked="{{isChecked}}" aria-label="{{adalabel}}" title="{{adalabel}}"><input type="checkbox" id="{{name}}" class="{{class}}" name="{{name}}" value="{{value}}" title="{{adalabel}}" aria-label="{{adalabel}}"/><span class="checkboxGraphic"></span></span>',
        link: function (scope, element, attrs) {

            var checkbox = $($(element).children()[0]);
            var checkBoxGif = $($(element).children()[1]);


            if ($.browser.version < 9.0) {
                $(checkBoxGif).html("&#xf096;&nbsp;");
            }

            if(attrs.color){
                $(checkBoxGif).css("color",attrs.color);
            }

            if (attrs.isChecked && attrs.isChecked == "true") {
                checkbox.prop("checked", true);
               // console.log("Checkbox Set To: ",checkbox.prop("checked"))

                if ($.browser.version < 9.0 && !checkbox.prop("checked")) {
                    $(checkBoxGif).html("&#xf096;&nbsp;");
                }else if ($.browser.version < 9.0 && checkbox.prop("checked")) {
                    $(checkBoxGif).html("&#xf046;&nbsp;");
                }else { }

            }else{

            }


            $(element).click(function () {

               // console.log("Whats the status: ",checkbox.prop("checked"))
                checkbox.prop("checked", !checkbox.prop("checked"));
                attrs.isChecked = checkbox.prop("checked");
               // console.log("Whats the status after: ",checkbox.prop("checked"))

                if ($.browser.version < 9.0 && !checkbox.prop("checked")) {
                    $(checkBoxGif).html("&#xf096;&nbsp;");
                }else if ($.browser.version < 9.0 && checkbox.prop("checked")) {
                    $(checkBoxGif).html("&#xf046;&nbsp;");
                }else { }

            });


            $(element).on("keyup", function (evt) {
                if (evt.keyCode == 32 || evt.keyCode ==68 ) {
                    $(element).click();
                }
            });

            $(element).focus(function () {
                $(checkBoxGif).css("border", "1px #000 dotted");
            })
            $(element).blur(function () {
                $(checkBoxGif).css("border", "none");
            })

           // if(attrs.sandataModel){console.log("Whats the Model?: ",attrs.sandataModel)}
           // var scopedCheckedModel = scope.$parse(attrs.sandataModel)

        }

    }

});
builder.directive("sandataCheckboxPassive", function () {
    return {
        require: ["?ngClick", "?ngModel"],
        restrict: "AC",
        scope: {
            isChecked: "@",
            name: "@",
            click: "&",
            sandataModel: "=",
            value: "@",
            adalabel:"@",
            "class": "@"

        },
        replace: true,
        template: '<div ng-click="click()" aria-checked="{{isChecked}}" aria-disabled="false" tabindex="0" aria-label="{{adalabel}}" title="{{adalabel}}"><input type="checkbox" id="{{name}}" class="{{class}}" name="{{name}}" value="{{value}}" title="{{adalabel}}" aria-label="{{adalabel}}"/><span class="checkboxGraphic"></span></div>',
        link: function (scope, element, attrs) {

            var checkbox = $($(element).children()[0]);
            var checkBoxGif = $($(element).children()[1]);



            attrs.$observe("isChecked",function(){
                if (attrs.isChecked && attrs.isChecked == "true") {
                    checkbox.prop("checked", true);
                    // console.log("Checkbox Set To: ",checkbox.prop("checked"))
                }else{
                    checkbox.prop("checked", false);
                    // console.log("Checkbox Set To: ",checkbox.prop("checked"))
                }

                if ($.browser.version < 9.0 && !checkbox.prop("checked")) {
                    $(checkBoxGif).html("&#xf096;&nbsp;");
                }else if ($.browser.version < 9.0 && checkbox.prop("checked")) {
                    $(checkBoxGif).html("&#xf046;&nbsp;");
                }else { }

            });




            $(element).on("keyup", function (evt) {

                if (evt.keyCode == 32  ) {
                    $(element).click();
                }
            });


            $(element).focus(function () {
                $(checkBoxGif).css("border", "1px #000 dotted");
            })
            $(element).blur(function () {
                $(checkBoxGif).css("border", "none");
            })

        }

    }

});

builder.directive("sandataSelectParams", function ($compile) {
    return {
        require:["?ngModel"],
        compile: function (elem, attr) {
            elem[0].removeAttribute('sandata-select-params');
            return function (scope) {

                var params = scope.$eval(attr.sandataSelectParams),
                    filterby;

                //test
                //params.required = true;
                //params.notblank = true;
                //params.trigger = 'submit';
                params.valuetype = 'alphanum';
                //acc -- params.model = 'reportnameNode';
                //acc -- params.subscribe = 'reporttypeNode';
                //test

                elem[0].setAttribute("name", params.name);
                elem[0].setAttribute("id", params.name);
                elem[0].setAttribute("data-ng-show", params.show);
                //dmr elem[0].setAttribute("data-required", params.required);
                //dmr elem[0].setAttribute("data-notblank", params.notblank);
                //dmr elem[0].setAttribute("data-trigger", params.trigger);
                elem[0].setAttribute("data-type", params.valuetype);
                elem[0].setAttribute("data-error-message", params.error_message);

                filterby = (attr.filterby) ? attr.filterby: "group";

                if (attr.model) {
                   // console.log("There is a Model Name:",attr.model)
                    elem[0].setAttribute("ng-model", attr.model)
                }

                if (attr.subscribe) {
                   // console.log("There is a Subscription to:",attr.subscribe)
                    var option_components = "c.label for c in ";

                    if (attr.section) {
                        option_components += attr.section + ".";
                    }

                    option_components += attr.field + ".option_components | filter:{" + filterby + ":" + attr.subscribe + "." + filterby + "}";
                    elem[0].setAttribute('ng-options', option_components);

                } else {
                    var option_components = "c.label for c in ";

                    if (attr.section) {
                        option_components += attr.section + ".";
                    }

                    option_components += attr.field + ".option_components";
                    elem[0].setAttribute('ng-options', option_components);

                }
                $compile(elem[0])(scope);

            }
        }
    }
});

builder.directive("sandataProgressInfo", function () {
    return {
        restrict: "AC",
        scope: {
            "markers": "="
        },
        tranclude: true,
        replace: true,
        controller: ['$scope','$attrs','$element',function(){

            this.toInteger = function(number){
              return Math.floor(
                Number(number)
              );
            };

        }],
        link: function (scope, element, attrs,ctrls) {
            var dial = $(element).children()[3];
            var calculatedValue = (scope.markers.value * 12)/100;
            var offsetValue = "offset" + ctrls.toInteger(calculatedValue);
            $(dial).addClass(offsetValue);

        },
        template: '<div class="span12"><span class="span4">{{markers.start}}</span> <span class="span2 offset2">{{markers.median}}</span> <span class="span1 offset3">{{markers.end}}</span><div class="span12" style="margin-top: -10px; margin-bottom:-15px;width: 0; height: 0; border-left: 7px solid transparent;	border-right: 7px solid transparent;border-top: 7px solid #000;"></div></div>'

    }

})

builder.directive("sandataMultiSelect", function () {
    return {
        link: function (scope, element, attrs) {
            $(element).multiselect({
                buttonClass: 'btn'
            });
        }
    }
});


builder.directive("sandataStatusInfo", function ($templateCache) {
    return {

        restrict:"AC",
        scope:{
            value: "@",
            label:"@",
            adalabel: "@",
            link: "@",
            title: "@",
            index:"@"
        },
        replace:true,
        template: $templateCache.get("sandataStatusInfo.cache"),
        link: function (scope, element, attr) {
            var linking = $(element).children()[0];
            var fillIn = $(linking).children()[0];
            scope.$watch('value', function (value) {
                $(fillIn).html(attr.value);
            });
        }
    }
});

//dmr 8-22-2013: http://stackoverflow.com/questions/14995884/select-text-on-input-focus-in-angular-js
builder.directive('selectOnClick', function () {
    // Linker function
    return function (scope, element, attrs) {
        element.click(function () {
            element.select();
        });
    };
});

builder.directive('sandataIFrame', function(){
    var linkFn = function(scope, element, attrs) {
        element.find('iframe').bind('load', function (event) {
            event.target.contentWindow.scrollTo(0,400);
        });
    };
    return {
        restrict: 'EA',
        scope: {
            src:'@src',
            height: '@height',
            width: '@width',
            scrolling: '@scrolling'
        },
        template: '<iframe class="frame" height="{{height}}" width="{{width}}" frameborder="0" border="0" marginwidth="0" marginheight="0" scrolling="{{scrolling}}" src="{{src}}"></iframe>',
        link : linkFn
    };
});

builder.directive("sandataDoughnutChart", function () {
    return {
        restrict: "AC",
        scope: {
            maxValue: "@",
            minValue: "@",
            name: "@",
            label: "@",
            title: "@",
            size: "@",
            id: "@",

            source: "="
        },
        controller: ['$scope', '$element', '$attrs', '$transclude', function ($scope, $element, $attrs, $transclude) {
            this.adjustSize = function () {
                switch ($attrs.size) {
                    case "small": $element.css("height", "150px").css("width", "150px")
                        break;
                    case "medium": $element.css("height", "300px").css("width", "300px").css("display", "inline-block");
                        break;
                    case "large": $element.css("height", "600px").css("width", "600px").css("display", "inline-block");
                        break;
                    default: $element.css("height", "300px").css("width", "300px").css("display", "inline-block");
                        break;
                }
                $element.css("-webkit-transform", "translate3d(0, 0, 0)");
            }

            this.adjustTitle = function (g) {
                switch ($attrs.size) {
                    case "small":
                                    g.txtTitle.attr({ "font-size": "14px", "font-family": "'Open Sans', sans-serif", "font-weight": "600" });
                                     $($($($element).children()).children()[4]).attr("y", "145");
                        break;
                    case "medium":
                                    g.txtTitle.attr({ "font-size": "16px", "font-family": "'Open Sans', sans-serif", "font-weight": "600" });
                                     $($($($element).children()).children()[4]).attr("y", "280");
                        break;
                    case "large":
                                    g.txtTitle.attr({ "font-size": "32px", "font-family": "'Open Sans', sans-serif", "font-weight": "600" });
                                    $($($($element).children()).children()[4]).attr("y", "555");
                        break;
                    default:        g.txtTitle.attr({ "font-size": "16px", "font-family": "'Open Sans', sans-serif", "font-weight": "600" });
                                     $($($($element).children()).children()[4]).attr("y", "280");
                        break;
                }
            }

        } ],
        link: function (scope, element, attrs, ctrls) {
            //Adjust Size of the Gauge
            ctrls.adjustSize();

            var g = null;
           // console.log(attrs.id)
            g = new JustGage(config = {
                id: attrs.id,
                value: scope.source.value,
                gaugeWidthScale: .4,
                shadowOpacity: 0,
                shadowSize: 0,
                counter: false,
                min: scope.source.min_value,
                max: scope.source.max_value,
                label: scope.source.label,
                title: scope.source.title,
                titleFontColor: "#000",
                levelColors: ["#7fab25"],
                donut: true

            });

            ctrls.adjustTitle(g);
        }

    }

});

builder.directive('focus', function ($timeout, $parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.$watch(attrs.focus, function (newValue, oldValue) {
                if (newValue) {
                        $(element).focus();
                   }
            });
            element.bind("blur", function (e) {
                $timeout(function () {
                    scope.$apply(attrs.focus + "=false");
                }, 0);
            });
            element.bind("focus", function (e) {
                $timeout(function () {
                    scope.$apply(attrs.focus + "=true");
                }, 0);
            })
        }
    }
});

builder.directive('sandataModalFocus', function ($timeout, $parse) {
    return {
        require: ["?focus"],
        link: function (scope, element, attrs) {
            attrs.$observe('sandataSkip', function (val) {
                if (attrs.element && attrs.element != "") {

                    $(element).bind("click", function () {
                        var node = attrs.element;
                        //Is there a Type
                        if (attrs.type) {
                            switch (attrs.type) {
                                case "id":
                                    node = node + "#";
                                    break;
                                case "class":
                                    node = node + ".";
                                    break;
                            }
                        }

                        if (attrs.label) {
                            node = node + attrs.label;
                        }
                            $(node).focus().focus();
                    }).bind("keyup", function (e) {
                        if (e.keyCode == 13) {
                            var node = attrs.element;
                            //Is there a Type
                            if (attrs.type) {
                                switch (attrs.type) {
                                    case "id":
                                        node = node + "#";
                                        break;
                                    case "class":
                                        node = node + ".";
                                        break;
                                }
                            }

                            if (attrs.label) {
                                node = node + attrs.label;
                            }
                            $(node).focus().focus();

                        }
                    })

                }
            });
        }
    }

})
builder.directive('sandataSkip', function ($timeout, $parse) {
    return {
        require: ["?focus"],
        scope: {
            element: "@",
            type: "@",
            label: "@",
            content: "@",
            index: "@",
            click: "@"
        },
        restrict: 'A',
        template: '<a class="skipnavigation"  aria-label="{{content}}" title="{{content}}" tabindex="{{index}}">{{content}}</a>',
        replace: true,
        link: function (scope, element, attrs) {
            attrs.$observe('sandataSkip', function (val) {
                if (attrs.element && attrs.element != "") {

                    $(element).bind("click", function () {
                        var node = attrs.element;
                        //Is there a Type
                        if (attrs.type) {
                            switch (attrs.type) {
                                case "id":
                                    node = node + "#";
                                    break;
                                case "class":
                                    node = node + ".";
                                    break;
                            }
                        }

                        if (attrs.label) {
                            node = node + attrs.label;
                        }

                        //$(node).focus();
                        $(node).attr("tabindex","0");
                        $(node).focus().focus();
                        $(node).blur(function(){
                             $(node).removeAttr("tabindex")
                        });


                       // $($("div#content select")[0]).focus();
                        if (attrs.click) {
                           $(node).click()
                            //$($("div#content select")[0]).focus();
                        }
                    }).bind("keyup", function (e) {
                        if (e.keyCode == 13) {
                            var node = attrs.element;
                            //Is there a Type
                            if (attrs.type) {
                                switch (attrs.type) {
                                    case "id":
                                        node = node + "#";
                                        break;
                                    case "class":
                                        node = node + ".";
                                        break;
                                }
                            }

                            if (attrs.label) {
                                node = node + attrs.label;
                            }

                            //$(node).focus();
                            $(node).attr("tabindex","0");
                            $(node).focus().focus();
                            $(node).blur(function(){
                                 $(node).removeAttr("tabindex")
                            });

                            //$($("div#content select")[0]).focus();
                            if (attrs.click) {
                                $(node).click()
                               //$($("div#content select")[0]).focus();
                            }
                        }
                    })

                }
            });
        }
    }
});

builder.directive('sandataNoOptions', function ($timeout, $parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            attrs.$observe("readonly", function (value) {
                if (value) {
                     $(element).find("option:not(:checked)").each(function(){
                         $(this).remove();
                        });
                  }
            });
        }
    }
 });

 builder.directive('sandataModalTabBackToTop', function () {
     return {
         link: function (scope, element, attrs) {
             attrs.$observe("sandataModalTabBackToTop", function (value) {
                 element.bind("keydown", function (e) {
                     var keyCode = e.keyCode || e.which;
                     if (!e.shiftKey && keyCode == 9 || keyCode == 40) {
                         $(attrs.sandataModalTabBackToTop).focus().focus();
                     }
                 });
             });

         }

     }
 });

 builder.directive('sandataNoShiftTab', function () {
     return {
         link: function (scope, element, attrs) {
             attrs.$observe("sandataNoShiftTab", function (value) {
                 element.bind("keydown",function(e){  var keyCode = e.keyCode || e.which;
	                    if(e.shiftKey && keyCode == 9) {
 		                    e.preventDefault();
                            if(attrs.sandataNoShiftTab){
                                $(attrs.sandataNoShiftTab).focus().focus();
                                return true;
                            }



	                    }
                    });
             });

         }

     }
 });


 builder.directive('sandataLargeSelect', function ($timeout, $parse) {

     return {
         require: ["?ngModel", "?ngChange"],
         link: function (scope, element, attrs) {
             //console.log(scope);
             var optionList = '';
             var loaded = false;


             scope.$watch(attrs.dataSource, function (value) {
                 attrs.label = (!attrs.label) ? "label" : attrs.label;
                 attrs.value = (!attrs.value) ? "value" : attrs.value;




                 if (attrs.sandataReadonly && attrs.sandataReadonly == "true") {
                     for (var i = 0; i < scope[attrs.dataSource].length; i++) {
                         if (scope[attrs.dataSource][i][attrs.value] == attrs.selected) {
                             optionList = optionList + "<option value='" + scope[attrs.dataSource][i][attrs.value] + "'>" + scope[attrs.dataSource][i][attrs.label] + "</option>";
                             initialOptionList = optionList;
                             break;
                         }

                     }
                 } else {
                     for (var i = 0; i < scope[attrs.dataSource].length; i++) {
                         if (scope[attrs.dataSource][i][attrs.value] == attrs.selected) {
                             initialOptionList = optionList + "<option value='" + scope[attrs.dataSource][i][attrs.value] + "'>" + scope[attrs.dataSource][i][attrs.label] + "</option>";
                         }
                         optionList = optionList + "<option value='" + scope[attrs.dataSource][i][attrs.value] + "'>" + scope[attrs.dataSource][i][attrs.label] + "</option>";

                     }
                 }

                 $(element).html(initialOptionList);
                 if (attrs.selected) {
                     $(element).find("option[value='" + attrs.selected + "']").prop('selected', true);
                 }
             });




             element.change(function () {

                 if (attrs.prefix && attrs.suffix && typeof scope[attrs.prefix][attrs.suffix][attrs.value] == 'undefined' && typeof scope[attrs.prefix][attrs.suffix] != 'undefined') {
                     jQuery.grep(scope[attrs.dataSource], function (passesObj, index) {
                         if (passesObj[attrs.value] == scope[attrs.prefix][attrs.suffix]) {
                             scope[attrs.prefix][attrs.suffix] = passesObj;
                         }
                     });

                 } else if (typeof scope[attrs.ngModel][attrs.value] == 'undefined' && typeof scope[attrs.ngModel] != 'undefined') {

                     jQuery.grep(scope[attrs.dataSource], function (passesObj, index) {
                         if (passesObj[attrs.value] == scope[attrs.ngModel]) {
                             scope[attrs.ngModel] = passesObj;
                         }
                     });
                 } else {

                 }

             });

             element.bind("mouseover focus", function () {
                 if (!loaded) {
                     $(element).append(optionList);
                     if (attrs.selected) {
                         $(element).find("option[value='" + attrs.selected + "']").prop('selected', true);
                     }
                     loaded = true;
                 }
             });
             element.bind("mouseover change focus", function () {
                 var tmp = $(element).find("option").filter(function () {
                     return this.value.match(/\?/);
                 });
                 $(tmp).remove();
             })

         }

     }
 });


 builder.directive('sandataToggleShowHide', function ($timeout, $parse) {

     return {

         link: function (scope, element, attrs) {

             element.click(function (e) {
                 e.preventDefault();
             })
             attrs.$observe("selected", function (value) {
                 $(element).find("option[value='" + attrs.selected + "']").prop('selected', true);
             })
             scope[attrs.sandataToggleShowHide] = {};
             if(attrs.adaLabel){scope[attrs.sandataToggleShowHide].adaLabel = attrs.adaLabel;}else{scope[attrs.sandataToggleShowHide].adaLabel ="";}

             if (attrs.show == "true") {
                 scope[attrs.sandataToggleShowHide].show = true;
                 scope[attrs.sandataToggleShowHide].toggleIndicator = attrs.showClass;
             } else if (attrs.show== 0) {
                 scope[attrs.sandataToggleShowHide].show = false;
                 scope[attrs.sandataToggleShowHide].toggleIndicator = attrs.hideClass;
             } else {
                 scope[attrs.sandataToggleShowHide].show = false;
                 scope[attrs.sandataToggleShowHide].toggleIndicator = attrs.hideClass;
             }


             scope[attrs.sandataToggleShowHide]["toggle"] = function (show) {
                 if (show && Boolean(show) == true){
                     scope[attrs.sandataToggleShowHide].show = true;
                     scope[attrs.sandataToggleShowHide].toggleIndicator = attrs.showClass;

                     if(scope[attrs.sandataToggleShowHide].adaLabel && scope[attrs.sandataToggleShowHide].adaLabel.match(/Show/gi)){
                         scope[attrs.sandataToggleShowHide].adaLabel = scope[attrs.sandataToggleShowHide].adaLabel.replace(/Show/gi,"Hide");
                     }
                 } else {
                     if (scope[attrs.sandataToggleShowHide].show == true) {
                         scope[attrs.sandataToggleShowHide].show = false;
                         scope[attrs.sandataToggleShowHide].toggleIndicator = attrs.hideClass;
                         if(scope[attrs.sandataToggleShowHide].adaLabel && scope[attrs.sandataToggleShowHide].adaLabel.match(/Hide/gi)){
                             scope[attrs.sandataToggleShowHide].adaLabel = scope[attrs.sandataToggleShowHide].adaLabel.replace(/Hide/gi,"Show");
                         }

                     } else {
                         scope[attrs.sandataToggleShowHide].show = true;
                         scope[attrs.sandataToggleShowHide].toggleIndicator = attrs.showClass;
                         if(scope[attrs.sandataToggleShowHide].adaLabel && scope[attrs.sandataToggleShowHide].adaLabel.match(/Show/gi)){
                             scope[attrs.sandataToggleShowHide].adaLabel = scope[attrs.sandataToggleShowHide].adaLabel.replace(/Show/gi,"Hide");
                         }
                     }
                 }
             }
         }

     }
 });

/*builder.directive('sandataSelectLazyLoader',function($parse,$timeout){

    return{
        controller: function($scope,$element,$attrs,$parse,$timeout){

            this.model = $parse($attrs.ngModel);
            this.scopedModel = angular.copy(this.model($scope));
            this.ds = $parse($attrs.load);
            this.dataSource = angular.copy(this.ds($scope));

            if(Boolean($attrs.blank)){
                var blankObj = {};
                blankObj[$attrs.optionValue] = "";
                blankObj[$attrs.optionLabel] = "";
                this.dataSource.unshift(blankObj);
            }

            this.loaded = false;
            this.optionList = '';

            //Initilize Select With A Selected Value --acc
            this.initializeSelect =  function(){

                var innerHTML = "<option value='"+ this.scopedModel[$attrs.optionValue] +"'>" +  this.scopedModel[$attrs.optionLabel] +"</option>"
                $($element).append(innerHTML);

                $timeout(function(){
                        $($element).find("option[value^='?']").remove();
                    },0)

            }

            this.change = function(){
                var that = this;
                var updatedIndex = 0;
                var elementValue =  $($element).val()
                for(var i= 0, dsLength = this.dataSource.length; i < dsLength; i++){
                //jQuery.grep(this.dataSource, function (passesObj, index) {
                    if ( this.dataSource[i][$attrs.optionValue] == elementValue) {

                        updatedIndex = i;
                            that.model.assign($scope,angular.copy(this.dataSource[i]));
                            that.scopedModel = angular.copy(that.model($scope));

                        if($attrs.change){
                            var change = $parse($attrs.change);
                            var applyChange = change($scope);
                            this.$scope = $scope;
                            applyChange();
                        }

                        $timeout(function(){
                            $($element).find("option[value^='?']").remove();
                            $($($element).children()).prop('selected', false);

                            $($($element).children()[updatedIndex]).prop('selected', true);
                         },0);
                    }
                }
                //});

            }

            //Load the rest of the options into the select --acc
            this.lazyLoader = function(){

                $($element).find("option[value^='?']").remove();

                if(!this.loaded && $attrs.id  ){

                    for( var i = 0, initLength = document.getElementById($attrs.id).options.length; i < initLength; i++){
                        document.getElementById($attrs.id).options[i] = null;
                    }
                    for (var i = 0, dsLength = this.dataSource.length; i < dsLength; i++ ) {
                        if (this.scopedModel[$attrs.optionValue] && this.dataSource[i][$attrs.optionValue] == this.scopedModel[$attrs.optionValue]) {
                            document.getElementById($attrs.id).options[i] = new Option(this.dataSource[i][$attrs.optionLabel],this.dataSource[i][$attrs.optionValue],true,true);
                        }else{
                            document.getElementById($attrs.id).options[i] = new Option(this.dataSource[i][$attrs.optionLabel],this.dataSource[i][$attrs.optionValue]);
                        }

                    }
                    this.loaded = true;
                }else if (!this.loaded) {
                    for (var i = 0, dsLength = this.dataSource.length; i < dsLength; i++ ) {
                        if (this.dataSource[i][$attrs.optionValue] == initialSelection) {
                            //console.log("What are the Values : ", this.dataSource[i][$attrs.optionValue])
                            this.optionList =  this.optionList + "<option value='" + this.dataSource[i][$attrs.optionValue] + "' selected>" + this.dataSource[i][$attrs.optionLabel] + "</option>";
                        }else{
                            this.optionList =  this.optionList + "<option value='" + this.dataSource[i][$attrs.optionValue] + "'>" + this.dataSource[i][$attrs.optionLabel] + "</option>";
                        }
                    }
                    $($element).html("");
                    $($element).append(this.optionList);
                    this.loaded = true;
                }else{
                    //console.log("Doing Nothing");
                }
            }
        },
        link:function(scope,element,attrs,ctrls){

            var model = $parse(attrs.ngModel);
            var scopedModel = angular.copy(model(scope));
            var ds = $parse(attrs.load);
            var dataSource = angular.copy(ds(scope));

            ctrls.initializeSelect();

            scope.$watch(attrs.ngModel,function(){
                ctrls.change();
            })

            if(attrs.delay && !isNaN(parseInt( attrs.delay )) ){
                $timeout(function(){
                    ctrls.lazyLoader();
                },attrs.delay);
            }

            element.bind("mouseover focus", function () {
                var readOnly = $parse(attrs.ngReadonly)
                if(!readOnly(scope))
                    ctrls.lazyLoader();
            });
        }

    }

});*/
builder.directive('sandataSelectLazyLoader',function($parse,$timeout,$compile){
    return {
        controller: function($scope,$element,$attrs,$parse,$timeout){
            this.model = $parse($attrs.ngModel);
            this.scopedModel = angular.copy(this.model($scope));
            this.ds = $parse($attrs.load);
            this.dataSource = angular.copy(this.ds($scope));

            this.ieLoader = function(){

                $($element).find("option[value^='?']").remove();
                    for( var i = 0, initLength = document.getElementById($attrs.id).options.length; i < initLength; i++){
                        document.getElementById($attrs.id).options[i] = null;
                    }
                    for (var i = 0, dsLength = this.dataSource.length; i < dsLength; i++ ) {
                        if (this.scopedModel.value && this.dataSource[i].value== this.scopedModel.value) {
                            document.getElementById($attrs.id).options[i] = new Option(this.dataSource[i].label,this.dataSource[i].value,true,true);
                        }else{
                            document.getElementById($attrs.id).options[i] = new Option(this.dataSource[i].label,this.dataSource[i].value);
                        }

                    }
            }
            this.selectOption =function(value){
                var selection = 0;
                //console.log("Controller Value Passed: ", value)

                if(typeof(value) == "number"){
                    selection =value;
                }else{
                    if(typeof(value) == "string"){
                        if(value.charAt(0) == "{"){
                                selection =JSON.parse(value).value;

                        }else{
                            selection =value;
                        }
                    }

                }
                if(typeof selection == "number"){
                    selection = selection.toString();
                }
                //console.log(selection, document.getElementById($attrs.id).value );
                if ($.browser.msie && $.browser.version < 10.0){
                    $($element).val(selection);
                }else{
                    document.getElementById($attrs.id).value = selection;
                }

                //$($element).val(selection);
            }
            if(Boolean($attrs.blank)){
                var blankObj = {};
                blankObj[$attrs.optionValue] = "";
                blankObj[$attrs.optionLabel] = "";
                this.dataSource.unshift(blankObj);
            }

            this.loaded = false;
            this.optionList = '';

            //Initilize Select With A Selected Value --acc
            this.initializeSelect =  function(){

              /*  var innerHTML = "<option value='"+ this.scopedModel[$attrs.optionValue] +"'>" +  this.scopedModel[$attrs.optionLabel] +"</option>"
                $($element).append(innerHTML);

                $timeout(function(){
                    $($element).find("option[value^='?']").remove();
                },0)
                */
                for( var i = 0, initLength = document.getElementById($attrs.id).options.length; i < initLength; i++){
                    document.getElementById($attrs.id).options[i] = null;
                }

                        document.getElementById($attrs.id).options[0] = new Option(this.scopedModel[$attrs.optionLabel] ,this.scopedModel[$attrs.optionValue] ,true,true);



            }

            this.change = function(){
                var that = this;
                var updatedIndex = 0;
                var elementValue =  $($element).val()
                for(var i= 0, dsLength = this.dataSource.length; i < dsLength; i++){
                    //jQuery.grep(this.dataSource, function (passesObj, index) {
                    if ( this.dataSource[i][$attrs.optionValue] == elementValue) {

                        updatedIndex = i;
                        that.model.assign($scope,angular.copy(this.dataSource[i]));
                        that.scopedModel = angular.copy(that.model($scope));

                        if($attrs.change){
                            var change = $parse($attrs.change);
                            var applyChange = change($scope);
                            this.$scope = $scope;
                            applyChange();
                        }

                        $timeout(function(){
                            $($element).find("option[value^='?']").remove();
                            $($($element).children()).prop('selected', false);

                            $($($element).children()[updatedIndex]).prop('selected', true);
                        },0);
                    }
                }
                //});

            }

            //Load the rest of the options into the select --acc
            this.lazyLoader = function(){

                $($element).find("option[value^='?']").remove();

                if(!this.loaded && $attrs.id  ){

                    if($.browser.msie && $.browser.version >= 9.0){
                        var clientOptionList ="";

                        for (var i = 0, dsLength = this.dataSource.length; i < dsLength; i++ ) {
                            if (this.dataSource[i][$attrs.optionValue] == this.scopedModel[$attrs.optionValue]) {
                                clientOptionList =  clientOptionList + "<option value='" + this.dataSource[i][$attrs.optionValue] + "' selected>" + this.dataSource[i][$attrs.optionLabel] + "</option>";
                            }else{
                                clientOptionList =  clientOptionList + "<option value='" + this.dataSource[i][$attrs.optionValue] + "'>" + this.dataSource[i][$attrs.optionLabel]  + "</option>";
                            }
                        }
                        if($.browser.version == 9.0){
                            $($element).html(clientOptionList)
                        }else{
                            document.getElementById($attrs.id).innerHTML = clientOptionList;
                        }

                        //$(element).html(optionList);
                        if($attrs.selectOption){
                            this.selectOption($attrs.selectOption);
                        }


                        if($attrs.selectOption){
                            $attrs.$observe("selectOption",function(value){
                                this.selectOption(value);
                            });
                        }
                    }else{
                        for( var i = 0, initLength = document.getElementById($attrs.id).options.length; i < initLength; i++){
                            document.getElementById($attrs.id).options[i] = null;
                        }
                        for (var i = 0, dsLength = this.dataSource.length; i < dsLength; i++ ) {
                            if (this.scopedModel[$attrs.optionValue] && this.dataSource[i][$attrs.optionValue] == this.scopedModel[$attrs.optionValue]) {
                                document.getElementById($attrs.id).options[i] = new Option(this.dataSource[i][$attrs.optionLabel],this.dataSource[i][$attrs.optionValue],true,true);
                            }else{
                                document.getElementById($attrs.id).options[i] = new Option(this.dataSource[i][$attrs.optionLabel],this.dataSource[i][$attrs.optionValue]);
                            }

                        }
                    }

                    this.loaded = true;
                }else if (!this.loaded) {
                    for (var i = 0, dsLength = this.dataSource.length; i < dsLength; i++ ) {
                        if (this.dataSource[i][$attrs.optionValue] == initialSelection) {
                            //console.log("What are the Values : ", this.dataSource[i][$attrs.optionValue])
                            this.optionList =  this.optionList + "<option value='" + this.dataSource[i][$attrs.optionValue] + "' selected>" + this.dataSource[i][$attrs.optionLabel] + "</option>";
                        }else{
                            this.optionList =  this.optionList + "<option value='" + this.dataSource[i][$attrs.optionValue] + "'>" + this.dataSource[i][$attrs.optionLabel] + "</option>";
                        }
                    }
                    $($element).html("");
                    $($element).append(this.optionList);
                    this.loaded = true;
                }else{
                    //console.log("Doing Nothing");
                }
            }
            this.ie10ModelUpdate = function(){

                var nodeModel;
                for(var i = 0,dsLength = this.dataSource.length;i<dsLength;i++){
                    if( this.dataSource[i].value == $($element).val() ){
                        nodeModel =this.dataSource[i];
                        this.model.assign(angular.copy(nodeModel),$scope);
                        //console.log("changed", this.model($scope) );
                        break;
                    }

                }


            }
        },
        scope:true,
        link:function(scope,element,attrs,ctrls){

            $timeout(function(){
                if ($.browser.msie && $.browser.version >= 10.0){
                    /****/
                    ctrls.lazyLoader();
                    /****/

                    //$(element).html(optionList)
                   // $(element).find("option[value^='?']").remove();
                }else{


                    ctrls.initializeSelect();

                    scope.$watch(attrs.ngModel,function(){
                        ctrls.change();
                        var el = element;
                        $timeout(function(){
                            if($(el).find("option[value^='?']")){
                                $(el).find("option[value^='?']").remove();
                            }

                        },0);

                    })

                    if(attrs.delay && !isNaN(parseInt( attrs.delay )) ){
                        $timeout(function(){
                            ctrls.lazyLoader();
                        },attrs.delay);
                    }

                    element.bind("mouseover focus", function () {
                        var readOnly = $parse(attrs.ngReadonly)
                        if(!readOnly(scope))
                            ctrls.lazyLoader();
                    });

                }


            },0);
        }
    }
});
builder.directive('sandataSelectRemoveBlankOption',function($parse,$timeout){

    return {
        link:function(scope,element,attrs,ctrls){
            $timeout(function(){
                $(element).find("option[value^='?']").remove();
            },0)

        }
    }
})


builder.directive("loadLazyLoadingSelects", function($timeout){

    return{
        require:["?sandataSelectLazyLoader"],
        link:function(scope,element,attrs){
            var delay = (attrs.delay) ? attrs.delay : 4000;
            var loadingFloor = (attrs.loadingFloor) ? attrs.loadingFloor : 1000;

            var selectFields =  attrs.loadLazyLoadingSelects.split(",");
            $timeout(function(){
                for(var i = 0; i < selectFields.length; i++){

                    if($("#" + selectFields[i]).data().$sandataSelectLazyLoaderController.dataSource.length >= loadingFloor){
                        $("#" + selectFields[i]).data().$sandataSelectLazyLoaderController.lazyLoader();
                    }

                }
            },delay);

        }
    }


});
builder.directive('sandataOnKeydown', function($timeout) {
    return{
        link:function(scope, elm, attrs) {
            if(attrs.keyType){
                $(elm).bind('keydown', attrs.keyType, function (e) {

                    scope.$apply(attrs.sandataOnKeydown);


                    /*var that = this;
                    var attributes = attrs;
                    $timeout(function(){
                        console.log(that)
                        if(attributes.id){
                            var element = $(this).prop("nodeName") + "#" + attributes.id;
                            $(element).focus().focus();
                        }else{
                            $(that).focus().focus();
                        }

                    },1000)*/
                    $(elm).focus().focus();

                    if(attrs.preventDefault){
                        return false;
                    }

                });
            }else{
                elm.bind("keydown", function() {
                    scope.$apply(attrs.sandataOnKeydown);
                    $(elm).focus().focus();
                });
            }

            if(attrs.preventDefault){
                elm.on("click", function() {
                        return false;
                });
            }

        }

    }
});


builder.directive("sandataClassToggler",function(){

    return{
        restrict:"AC",
        link: function(scope,element,attrs){
            $(element).addClass(attrs.on);
            $(element).bind(attrs.action,function(e) {
                if(attrs.preventDefault &&  Boolean(attrs.preventDefault))
                    e.preventDefault();

                if ( $( this ).hasClass(attrs.on ) ) {
                    $(this).addClass(attrs.off).removeClass(attrs.on);

                } else {
                    $(this).addClass(attrs.on).removeClass(attrs.off);

                }


                if ( $( this ).hasClass(attrs.on ) ) {
                    if($(this).attr("name") &&  $(this).attr("name").match(/Hide/gi)){
                        $(this).attr("name" ,$(this).attr("name").replace(/Hide/gi, "Show") );
                    }
                    if($(this).attr("title") &&  $(this).attr("title").match(/Hide/gi)){
                        $(this).attr("title", $(this).attr("title").replace(/Hide/gi, "Show") );
                    }
                    if($(this).attr("aria-label") &&  $(this).attr("aria-label").match(/Hide/gi)){
                        $(this).attr("aria-label", $(this).attr("aria-label").replace(/Hide/gi, "Show") );
                    }

                }
                if ( $( this ).hasClass(attrs.off ) ) {
                    if($(this).attr("name") &&  $(this).attr("name").match(/Show/gi)){
                        $(this).attr("name", $(this).attr("name").replace(/Show/gi, "Hide") );
                    }
                    if($(this).attr("title") &&  $(this).attr("title").match(/Show/gi)){
                        $(this).attr("title", $(this).attr("title").replace(/Show/gi, "Hide") );
                    }
                    if($(this).attr("aria-label") &&  $(this).attr("aria-label").match(/Show/gi)){
                        $(this).attr("aria-label", $(this).attr("aria-label").replace(/Show/gi, "Hide") );
                    }

                }

            });
        }
    }
});


builder.directive("pageLoader",function($timeout,$location){
    return {
        link:function(scope,element,attrs){
            element.click(function(){
                var location = "#" + $location.path();
                if(location != attrs.href){
                    Sandata.Utility.ShowPageLoaderGraphic();
                }
            })
        }
    }

});

builder.directive("sandataClickOnEnter",function(){
    return {

        link:function(scope , element , attrs){
            element.keydown(function(evt){
                if(evt.keyCode == 13){
                    element.click();
                }
            });
        }
    }
});

builder.directive("sandataDisplayToggler",function($parse,$timeout){
    return {
        controller: function($scope,$element,$attrs,$parse,$timeout){


            this.toggleTarget = function(){
                if($attrs.sandataDisplayToggler){
                    this.model = $parse($attrs.sandataDisplayToggler);
                    this.scopedModel = this.model($scope);
                    //console.log("Whats the model to be updated: Before: ",this.scopedModel)
                    if(this.scopedModel.expanded === "true"){
                        this.scopedModel.expanded = "false";

                        if(this.scopedModel.label){
                            this.scopedModel.label = this.scopedModel.label.replace("Hide","Show");
                        }
                        if(this.scopedModel.title){
                            this.scopedModel.title = this.scopedModel.title.replace("Hide","Show");
                        }
                        if(this.scopedModel.name){
                            this.scopedModel.name = this.scopedModel.name.replace("Hide","Show");
                        }
                    }else{
                        this.scopedModel.expanded = "true";
                        if(this.scopedModel.label){
                            this.scopedModel.label = this.scopedModel.label.replace("Show","Hide");
                        }
                        if(this.scopedModel.title){
                            this.scopedModel.title = this.scopedModel.title.replace("Show","Hide");
                        }
                        if(this.scopedModel.name){
                            this.scopedModel.name = this.scopedModel.name.replace("Show","Hide");
                        }

                    }
                    //console.log($attrs.ariaLabel)
                    //console.log("Whats the model to be updated: After: ",this.scopedModel)
                    this.model.assign($scope, this.scopedModel);



                    $scope.$apply();
                }


                        $timeout(function(){
                            var hidePattern = /Hide/gi;
                            var showPattern = /Show/gi;

                           if($attrs.focusOn && $attrs.ariaExpanded && ($attrs.ariaExpanded==="true")){


                               if(!$attrs.sandataDisplayToggler){
                                   $($element).attr("aria-label", $attrs.ariaLabel.replace("Show","Hide") );
                                   $($element).attr("title", $attrs.title.replace("Show","Hide") );
                                   $($element).attr("name", $attrs.name.replace("Show","Hide") );
                               }



                                //console.log("Focus Targeet: ",$attrs.focusOn);
                                $($attrs.focusOn).focus().focus();
                            }else if($attrs.ariaExpanded && ($attrs.ariaExpanded==="true")){
                               if(!$attrs.sandataDisplayToggler){
                                   $($element).attr("aria-label", $attrs.ariaLabel.replace("Show","Hide") );
                                   $($element).attr("title", $attrs.title.replace("Show","Hide") );
                                   $($element).attr("name", $attrs.name.replace("Show","Hide") );
                               }


                            }else{
                               if(!$attrs.sandataDisplayToggler){
                                   $($element).attr("aria-label", $attrs.ariaLabel.replace("Hide","Show") );
                                   $($element).attr("title", $attrs.title.replace("Hide","Show") );
                                   $($element).attr("name", $attrs.name.replace("Hide","Show"));
                               }

                            }
                            //console.log("Label Switch: ",$($element).attr("aria-label"));
                        },0)



            }

        },
        link:function(scope , element , attrs, ctrls){
            element.click(function(){
                ctrls.toggleTarget();
                return false;
            });

            element.keydown(function(evt){
                if(evt.keyCode == 13){
                    element.click();
                }
            });
        }
    }
});

builder.directive("sandataDatePicker",function(){
    return {
        require:["?parsleyRequiredField","?ngModel","?ngReadonly"],
        controller:function($scope,$element,$attrs){},
        replace:true,
        template:' <input  parsley-required-field="{{attrs.required}}" title="{{attrs.title}}" placeholder="{{attrs.placeholder}}" data-regexp="{{attrs.regexp}}" data-error-header="{{attrs.errorHeader}}" data-error-message="{{attrs.errorMessage}}"  data-trigger="{{attrs.trigger}}"  data-error-container="{{attrs.errorContainer}}" type="text" value="{{attrs.value}}" id="{{attrs.id}}" name="{{attrs.name}}" ng-model="{{attrs.ngModel}}" class="{{attrs.class}}" data-ng-readonly="{{attrs.ngReadonly}}"   data-date-type="{{attrs.dateType}}" aria-label="{{attrs.ariaLabel}}"/>',
        link:function(scope,element,attrs){}

    }
});

builder.directive("sandataTimepicker",function(){
    return {
        require:["?parsleyRequiredField","?ngModel","?ngReadonly"],
        controller:function($scope,$element,$attrs){},
        template:' <input  parsley-required-field="{{attrs.required}}" title="{{attrs.title}}" placeholder="{{attrs.placeholder}}" data-regexp="{{attrs.regexp}}" data-error-header="{{attrs.errorHeader}}" data-error-message="{{attrs.errorMessage}}"  data-trigger="{{attrs.trigger}}"  data-error-container="{{attrs.errorContainer}}" type="text" value="{{attrs.value}}" id="{{attrs.id}}" name="{{attrs.name}}" class="{{attrs.class}}" data-ng-readonly="{{attrs.ngReadonly}}"   data-date-type="{{attrs.dateType}}" aria-label="{{attrs.ariaLabel}}"/>',
        link:function(scope,element,attrs){}

    }
});


builder.directive("sandataInputText",function($parse,$timeout){
    return{

        scope:false,
        controller: function($scope,$element,$attrs,$parse,$timeout){
            var bElementExistInForm = false;
            this.id;
            this.checkIfNodeExists = function(){

                var result = $element.closest('form').parsley().items.filter(function( obj ) {
                    if(obj.$element.context.id == $attrs.id){
                        return obj;
                    }
                });
                if(result.length > 0){
                    bElementExistInForm = true;
                }
                return bElementExistInForm;
            }

            this.getElementSelector = function(){

                if($attrs.id && $attrs.id != ""){
                    this.id = $($element).prop('tagName');
                    this.id = this.id.toLowerCase() + "#" + $attrs.id;


                }else if($attrs.name && $attrs.name != ""){
                    this.id = $($element).prop('tagName');
                    this.id = this.id.toLowerCase() + "[name='" + $attrs.name.trim() + "']";

                }else{ this.id = null;
                    //console.log("ID or Name attribute must be associated with parselyRequireField Directive for element:",$element);
                }


                return this.id;
            }

            this.removeItemFromParsley = function(){
                var id = this.getElementSelector();
                var exists = this.checkIfNodeExists();
                if(exists){
                    $element.closest('form').parsley('removeItem', id);
                }
            }

            this.addItemFromParsley = function(){
                var id = this.getElementSelector();
                var exists = this.checkIfNodeExists();
                if(exists){
                    $element.closest('form').parsley('addItem', id);
                }
            }

            this.toggleRequiredField = function(bVal){
                var that = this;
                if(bVal){
                    //console.log(bVal)
                    $timeout(function(){
                        that.removeItemFromParsley();
                        $element.attr("required","true"  );
                        that.addItemFromParsley();
                    },0);
                }else{
                    //console.log(bVal)
                    $timeout(function(){
                        $element.removeAttr("required"  );
                        that.removeItemFromParsley();
                    },0);
                }
            }

            this.setup = function(){
                if(this.getElementSelector() != null){

                    $element.removeAttr("required");
                    var requiredModel = $parse($attrs.sandataRequiredField);
                    var required = angular.copy( requiredModel($scope));

                    this.removeItemFromParsley();

                    this.toggleRequiredField(required);
                }
            }

            this.autoCorrectTime = function(bMilitaryTime){
                var that ={};
                that.$parse = $parse;
                that.$attrs =$attrs;
                that.$scope = $scope;
                that.$timeout = $timeout;
                that.militaryTime = bMilitaryTime;
                $($element).on("keyup focus",function(evt){
                    var position =  $($element).get(0).selectionStart;
                    //console.log("Start Position: ", $($element).get(0).selectionStart);
                    if((evt.keyCode != 8) && (evt.keyCode != 46) && (evt.keyCode != 17) && (evt.keyCode != 37) && (evt.keyCode != 38) && (evt.keyCode != 39) && (evt.keyCode != 40) && (evt.keyCode != 27)  ){
                        var value = $(this).val();
                        value = value.replace(/[\.?]/gi,":");
                        //value = value.replace(/[\s?]/gi,"");
                        if(that.militaryTime === "true"){
                            value = value.replace(/[aA](.*)/gi,"");
                            value = value.replace(/[pP](.*)/gi,"");
                            value = value.replace(/(AM)|(am)|(aM)|(Am)|(AMM)|(AMm)/g,"");
                            value = value.replace(/(PM)|(pm)|(pM)|(Pm)|(PMM)|(PMm)/g,"");
                            value = value.replace("AMM","");
                            value = value.replace("PMM","");
                            value = value.replace("AMm","");
                            value = value.replace("PMm","");
                        }else{
                            value = value.replace(/[aA](.*)/gi,"AM");
                            value = value.replace(/[pP](.*)/gi,"PM");
                            value = value.replace(/(AM)|(am)|(aM)|(Am)|(AMM)|(AMm)/g,"AM");
                            value = value.replace(/(PM)|(pm)|(pM)|(Pm)|(PMM)|(PMm)/g,"PM");
                            value = value.replace("AMM","AM");
                            value = value.replace("PMM","PM");
                            value = value.replace("AMm","AM");
                            value = value.replace("PMm","PM");
                        }



                        $(this).val(value);
                        var timedModel = $parse($attrs.ngModel);
                        timedModel.assign($scope, $(this).val());
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        }
                        //console.log("Updated: ",timedModel($scope));

                        if($attrs.action){
                            $scope.$eval($attrs.action);
                        }

                        //var position = position + 1;
                        if($($element).get(0).setSelectionRange){
                            $($element).get(0).setSelectionRange(position, position);
                        }else{
                            var start = 0, end = 0, normalizedValue, range,
                                textInputRange, len, endRange;

                            if (typeof document.getElementById($($element).attr("id")).selectionStart == "number" && typeof document.getElementById($($element).attr("id")).selectionEnd == "number") {
                                start = document.getElementById($($element).attr("id")).selectionStart;
                                end = document.getElementById($($element).attr("id")).selectionEnd;
                            } else {
                                        range = document.selection.createRange();

                                        if (range && range.parentElement() == document.getElementById($($element).attr("id"))) {
                                            len = document.getElementById($($element).attr("id")).value.length;
                                            normalizedValue = document.getElementById($($element).attr("id")).value.replace(/\r\n/g, "\n");

                                            // Create a working TextRange that lives only in the input
                                            textInputRange = document.getElementById($($element).attr("id")).createTextRange();
                                            textInputRange.moveToBookmark(range.getBookmark());

                                            // Check if the start and end of the selection are at the very end
                                            // of the input, since moveStart/moveEnd doesn't return what we want
                                            // in those cases
                                            endRange = document.getElementById($($element).attr("id")).createTextRange();
                                            endRange.collapse(false);

                                            if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
                                                start = end = len;
                                            } else {
                                                start = -textInputRange.moveStart("character", -len);
                                                start += normalizedValue.slice(0, start).split("\n").length - 1;

                                                if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                                                    end = len;
                                                } else {
                                                    end = -textInputRange.moveEnd("character", -len);
                                                    end += normalizedValue.slice(0, end).split("\n").length - 1;
                                                }
                                            }
                                        }
                                    }

                                    var positions = {
                                        start: start,
                                        end: end
                                    };
                                    ///////////////////////////////////////////////////////////////////
                                    var range = document.getElementById($($element).attr("id")).createTextRange();
                                    range.collapse(true);
                                    /*if(pos < 0) {
                                     pos = $(this).val().length + pos;
                                     }*/
                                    range.moveEnd('character', 0);
                                    range.moveStart('character', positions.start  );
                                    range.select();

                        }

                    }
                });

                $($element).on("blur",function(){
                    var value = $(this).val();
                    if(that.militaryTime === "true"){

                    }else{
                        if(value.match(/^(0?[1-9]|1[012]):([0-5]\d)(\s?)$/) != null &&  value.match(/[apmAPM]/) == null){
                            value = value + "AM";
                        }

                        $(this).val(value);
                        var timedModel = $parse($attrs.ngModel);
                        timedModel.assign($scope, $(this).val());
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        }
                        if($attrs.action){
                            $scope.$eval($attrs.action);
                        }
                    }

                })


            }

            this.autoCorrectDate = function(){
                var that ={};
                that.$parse = $parse;
                that.$attrs =$attrs;
                that.$scope = $scope;
                that.$timeout = $timeout;
                $($element).on("keyup focus",function(evt){
                    var position =  $($element).get(0).selectionStart;
                   // console.log("Start Position: ", position);
                    if((evt.keyCode != 8) && (evt.keyCode != 46) && (evt.keyCode != 17) && (evt.keyCode != 37) && (evt.keyCode != 38) && (evt.keyCode != 39) && (evt.keyCode != 40) && (evt.keyCode != 27)  ){
                        var value = $(this).val();
                        $(this).val(value);
                        var dateModel = $parse($attrs.ngModel);
                        dateModel.assign($scope, $(this).val());
                        if(!$scope.$$phase) {
                            $scope.$apply();
                        };
                        //console.log("Updated: ",timedModel($scope));

                        if($attrs.action){
                            $scope.$eval($attrs.action);
                        }

                        //var position = position + 1;
                        $($element).get(0).setSelectionRange(position, position);
                    }
                })
            }
        },
        link:function(scope,element,attrs,ctrls){

            /**Start Check Required*/
            if(attrs.sandataRequiredField){
                ctrls.setup();

                scope.$watch(attrs.sandataRequiredField, function(val){
                    if(ctrls.getElementSelector() != null){
                        ctrls.removeItemFromParsley();
                        ctrls.toggleRequiredField(val);
                    }

                });
            }
            /**End Check Required*/

            if(attrs.sandataInputText == "time"){
                ctrls.autoCorrectTime(attrs.militaryTime);

               if(attrs.militaryTime === "true"){
                   $(element).kendoTimePicker({
                       format: "HH:mm"
                   });
                }else{
                   $(element).kendoTimePicker({
                       format: "hh:mm tt"
                   });
               }

                var timepicker = $(element).data("kendoTimePicker");
                $(element).attr("role","textbox").removeAttr("aria-expanded").removeAttr("aria-owns");

                    scope.$watch(attrs.ngReadonly,function(value){
                        if(value == true){
                            timepicker.readonly();
                        }else{
                            timepicker.enable();
                        }
                    });
            }

            if(attrs.sandataInputText == "date"){
              //  ctrls.autoCorrectDate();
                $(element).kendoDatePicker({
                    open: function () {
                        var dateViewCalendar = this.dateView.calendar;
                        if (dateViewCalendar) {
                            dateViewCalendar.element.width(200);
                        }
                    },
                    format:'MM/dd/yyyy'

                });
                var datepicker = $(element).data("kendoDatePicker");
               // console.log(element);
                $(element).attr("role","textbox").removeAttr("aria-expanded").removeAttr("aria-owns");

                scope.$watch(attrs.ngReadonly,function(value){
                    if(value == true){
                        datepicker.readonly();
                    }else{
                        datepicker.enable();
                    }
                });
            }


        }
    }
})

builder.directive("sandataAdaTabs",function($timeout){
    return {

        controller:function($scope,$element,$attrs){},
        link:function(scope,element,attrs){
            $timeout(function(){
                var parent = element.find("ul.nav-tabs");
                var kidCount = element.find("ul.nav-tabs").children("li")
                element.find("ul.nav-tabs").children("li").each(function(index,item){

                    $(this).bind("keydown","right",function(){
                        var next = index+1;
                        if(next != kidCount){
                            $($(parent).children()[next]).find("a").click().focus().focus();
                        }
                    });
                    $(this).bind("keydown","down",function(){
                        var next = index+1;
                        if(next != kidCount){
                            $($(parent).children()[next]).find("a").click().focus().focus();
                        }
                    })
                    $(this).bind("keydown","left",function(){
                        var prev = index-1;
                        if(prev >= 0){
                            $($(parent).children()[prev]).find("a").click().focus().focus();
                        }
                    })
                    $(this).bind("keydown","up",function(){
                        var prev = index-1;
                        if(prev >= 0){
                            $($(parent).children()[prev]).find("a").click().focus().focus();
                        }
                    })
                });
            },0);

        }

    }
});
