app.run(['$templateCache', '$timeout', '$http', '$rootScope', '$q', '$route', '$location', 'SandataCache', function ($templateCache, $timeout, $http, $rootScope, $q, $route, $location, SandataCache) {

    $templateCache.put("sandataStatusInfo.cache", '<div class="PlainStats {{class}}" aria-label={{adalabel}}><a href="{{link}}" title="{{title}}" class="stat-item"><div class="plainStat"></div><span>{{label}}</span></a></div>');

    var lastDigestRun = new Date();
    var warning = 25 * 60000;
    var expiration = 30 * 60000;
    var warningDialog;

    var interval;

    this.defineWarning = function () {
        var now = new Date();
        if (now - lastDigestRun > warning) {
            
            if (warningDialog) {
                $(warningDialog).dialog("open")
            } else {
                warningDialog = $("<p>Would you like to continue working? If you do not respond to the prompt, then you will be automatically logged out.</p>").dialog({
                    modal: false, title: 'Warning!', zIndex: 99999999999, autoOpen: true,closeOnEscape: false,
                    width: 'auto', resizable: false,
                    buttons: {
                        Yes: function () {
                            detectIdle();
                            $(this).dialog("close");
                        },
                        No: function () {
                            $(this).dialog("close");
                            SandataCache.removeAll()
                            Sandata.Utility.StatusErrorCodeHandler(0);
                        }
                    },
                    close: function (event, ui) {
                        $(this).hide();
                    },
                    open :  function (event, ui) {
                        $(this).show();
                    }
                })
            }


        }

        
        if (now - lastDigestRun > expiration) {
            $(warningDialog).dialog("close")
            $(warningDialog).remove();
            detectIdle();
        }
    }

    interval = setInterval(this.defineWarning, 1000);

    var setWarning = function () {
        interval = setInterval(this.defineWarning, 1000);
    }
    var clearWarning = function () {
        clearInterval(interval);
    }
    var detectIdle = function () {
        clearWarning();
        setWarning();

        var now = new Date();
        
        if (now - lastDigestRun > expiration) {
            // logout here, like delete cookie, navigate to login 
            SandataCache.removeAll()
            Sandata.Utility.StatusErrorCodeHandler(0);
        }
        lastDigestRun = now;

        if ($(warningDialog).hasClass('ui-dialog-content')) {
            $(warningDialog).dialog("close");
        }
    }

    $rootScope.$watch(function () { detectIdle() });

    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        if(current.$$route && current.$$route.title)
            $rootScope.title = current.$$route.title;
    });

    $rootScope.$on('$locationChangeStart', function(e,next,previous){

    });

    $rootScope.$on('$locationChangeSuccess',function(e,next,previous){

    });


    $rootScope.$watch(function () { return $location.path() }, function (newLocation, oldLocation) {
    });


} ]);
