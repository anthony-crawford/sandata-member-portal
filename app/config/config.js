//window.onbeforeunload = function () {return false;}
var app = angular.module("santrax", ['ngRoute','ngResource','$strap.directives','ui.bootstrap', "parsley","builder",'kendo.directives'])
                    .config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

                        $routeProvider
                            .when("/", {
                                title:"..Loading",
                                template: "loading",
                                controller: "Sandata.Controller.AppLoader.Initializer",
                                resolve: {
                                    preloader                       :   Sandata.Utility.ShowPageLoaderGraphicPromise,
                                    crendentialLoader               :   Sandata.Utility.Initialize_CredentialsLoader,
                                    initializeClientEmployeeCache   :   Sandata.Utility.Initialize_ClientEmpoyeeCache,
                                    initializePayorCache            :   Sandata.Utility.Initialize_PayorCache
                                }

                            })
                            .when('/dashboard', {
                                title:"Dashboard",
                                templateUrl: 'templates/dashboard.html',
                                controller: "Sandata.Controller.Dashboard.Init",
                                resolve: {
                                    loadClientEmployee: Sandata.Utility.DashboardClientEmployeeLoader,
                                    loadClientPayors: Sandata.Utility.DashboardPayorLoader
                                }
                            })
                            .when('/visits', {
                                title:"Visits",
                                templateUrl: "templates/visits.html",
                                controller: "Sandata.Controller.Visits.Init",
                                resolve: {
                                    loadClientEmployee: Sandata.Utility.ClientEmployeeLoader,
                                    loadPayor: Sandata.Utility.PayorLoader
                                }
                            })
                            .when('/visits/form', {
                                title:"Visits Form",
                                templateUrl: "templates/visit_form.html",
                                controller: "Sandata.Controller.VisitForm.Init",
                                resolve: {
                                    loadClientEmployee: Sandata.Utility.ClientEmployeeLoader,
                                    loadPayor: Sandata.Utility.PayorLoader
                                }
                            })
                            .when('/reporting', {
                                title:"Reporting",
                                templateUrl: 'templates/reporting.html',
                                controller: "Sandata.Controller.Reporting.Init",
                                resolve: {
                                     loadReportData: Sandata.Utility.ReportDataLoader,  //dmr
                                     loadReportView: Sandata.Utility.ReportViewLoader
                                }
                            }
                                   )
                            .otherwise({ template: '404' });

                        $locationProvider.html5Mode(false);

                    } ]);
// MOCK -- Uncomment Out Mocks in E2E Testing section in Index.html before using below.
/*
app.constant("REST_API", { 
                            "urls": {
                                        "accountdata": "cxf/up/service/account/accountdata", 
                                        "payors":"cxf/up/service/payor",     
                                        "view_vists" : "view/visits"                   
                                    }
                        }
);
*/

//LIVE DATA
app.constant("REST_API", { 
                            "urls": {
                                        "accountdata"          : "/cxf/up/service/account/accountdata", 
                                        "payors"               : "/cxf/up/service/payor",
                                        "dashboard_metadata"   : "/cxf/up/service/dashBoardMetadata",
                                        "confirm_unknown_call" : "/cxf/up/service/confirmUnknownCall",
                                        "view_vists"           : "/cxf/up/view/visits",
                                        "add_visit"            : "/cxf/up/service/addvisit",
                                        "edit_visit"           : "/cxf/up/service/editVisit",
                                        "view_visit"           : "/cxf/up/service/viewVisit",
                                        "approve_visit"        : "/cxf/up/service/approve_visit",
                                        "ignore_visit"         : "/cxf/up/service/ignore_visit",
                                        "view_reports"         : "/cxf/up/view/reports",                         
                                        "reports_token"         : "/cxf/up/service/report/token"
                                    }
                        }
);
app.constant("REGEXP_FORMATS",{

                            date_regex_pattern:"^(0[1-9]|1[012])[//](0[1-9]|[12][0-9]|3[01])[//](19|20)\\d\\d$",
                            time_regex_pattern:"^(0?[1-9]|1[012]):([0-5]\\d)(\\s?)([APap][mM])$",
                            military_time_regex_pattern:"^(0?[0-9]|1[0-9]|2[0123]):([0-5]\\d)(\\s?)$"

});


//  Test Local API
/*
app.constant("REST_API", { 
                            "urls": {
                                        "accountdata": "http://localhost:8181/cxf/up/service/account/accountdata",
                                        "payors":"http://localhost:8181/cxf/up/service/payor",
                                        "view_vists" : "http://localhost:8181/cxf/up/view/visits"      
                                    }
                        }
);
*/