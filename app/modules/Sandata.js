﻿var Sandata;
(function (Sandata) {
    /// <reference path="../definitions/angularjs/angular.d.ts" />
    /// <reference path="../definitions/bootstrap/bootstrap.d.ts" />
    /// <reference path="../definitions/jquery/jquery.d.ts" />
    (function (Utility) {
        Utility.BaseProtocol = {};
        Utility.BaseDomain = {};
        Utility.BaseURL = {};
        Utility.ReportServiceURL = {};

        // Persist display name cookielessly using window.name
        function Display_Name(name) {
            // window object
            var win = window.top || window;

            // session store
            var store = (window.name) ? JSON.parse(window.name) : {};

            // set default
            if (store['name'] == undefined) {
                store['name'] = 'Me';
            }

            // set name
            if (name != undefined) {
                store['name'] = name;
                win.name = JSON.stringify(store);
            }

            return store['name'];
        }
        Utility.Display_Name = Display_Name;

        Utility.AccountDataReadyListeners = {};

        function ShowPageLoaderGraphicPromise($q, $timeout, DashboardService) {
            var defer = $q.defer();
            var timeout = $timeout(function () {
                $("#loader").css("display", "block");
                defer.resolve('preloader');
            }, 500);

            return defer.promise;
        }
        Utility.ShowPageLoaderGraphicPromise = ShowPageLoaderGraphicPromise;

        function ShowPageLoaderGraphic() {
            $("#loader").css("display", "block");
        }
        Utility.ShowPageLoaderGraphic = ShowPageLoaderGraphic;

        function HidePageLoaderGraphic() {
            $("#loader").css("display", "none");
        }
        Utility.HidePageLoaderGraphic = HidePageLoaderGraphic;

        function ReloadCheck() {
            /********** Reload Warning ***********/
            $(window).bind('beforeunload', function (e) {
                if (1) {
                    return "By reloading this page, you will be logged out of the application; and redirected to the log in page. Are you sure you want to reload this page?";
                    e.preventDefault();
                }
            });
        }
        Utility.ReloadCheck = ReloadCheck;

        function Initialize_CredentialsLoader($q, $location, REST_API, SandataCache) {
            ShowPageLoaderGraphic();

            var defer = $q.defer();

            var credentials = SandataCache.get("credentials");
            var header_credentials = SandataCache.get("header_credentials");

            if (credentials && header_credentials) {
                defer.resolve("crendentialLoader");
            } else if ($location.search().su && $location.search().su != 'undefined' && $location.search().sp && $location.search().sp != 'undefined' && $location.search().soid && $location.search().soid != 'undefined') {
                if (SandataCache != undefined) {
                    var cachedData = SandataCache.get("credentials");
                    if (cachedData) {
                        var tmpObj = {
                            "su": decodeURIComponent($location.search().su),
                            "sp": decodeURIComponent($location.search().sp),
                            "soid": decodeURIComponent($location.search().soid)
                        };
                        var tmpObj_header = { headers: { 'su': decodeURIComponent($location.search().su), 'sp': decodeURIComponent($location.search().sp), 'soid': decodeURIComponent($location.search().soid), "timezone": Sandata.Utility.GetTimeZone(), "language": Sandata.Utility.GetLanguage() } };

                        SandataCache.put("credentials", tmpObj);
                        SandataCache.put("header_credentials", tmpObj_header);
                    } else {
                        var tmpObj = {
                            "su": decodeURIComponent($location.search().su),
                            "sp": decodeURIComponent($location.search().sp),
                            "soid": decodeURIComponent($location.search().soid)
                        };
                        var tmpObj_header = { headers: { 'su': decodeURIComponent($location.search().su), 'sp': decodeURIComponent($location.search().sp), 'soid': decodeURIComponent($location.search().soid), "timezone": Sandata.Utility.GetTimeZone(), "language": Sandata.Utility.GetLanguage() } };

                        SandataCache.put("credentials", tmpObj);
                        SandataCache.put("header_credentials", tmpObj_header);
                    }
                }

                defer.resolve("crendentialLoader");
            } else {
                window.location = "http://access.santrax.com"; //Sandata.Utility.BaseURL;
                defer.resolve("crendentialLoader");
            }

            return defer.promise;
        }
        Utility.Initialize_CredentialsLoader = Initialize_CredentialsLoader;

        function Initialize_ClientEmpoyeeCache($dialog, $location, $q, REST_API, DashboardService, SandataCache) {
            return Sandata.Utility.InitializeExternalData($location, $q, "initializeClientEmployeeCache", DashboardService.httpService, "get", REST_API.urls.accountdata, SandataCache, "accountdata");
        }
        Utility.Initialize_ClientEmpoyeeCache = Initialize_ClientEmpoyeeCache;

        function Initialize_PayorCache($location, $q, REST_API, DashboardService, SandataCache) {
            return Sandata.Utility.InitializeExternalData($location, $q, "initializePayorCache", DashboardService.httpService, "get", REST_API.urls.payors, SandataCache, "payor");
        }
        Utility.Initialize_PayorCache = Initialize_PayorCache;

        function Initialize_ClientPayorsCache($dialog, $location, $q, REST_API, DashboardService, SandataCache) {
            var accountdata = SandataCache.get("accountdata");
            var clientId = accountdata.sections.accountdata_parameters.base_components.CLIENT_NAMES.option_components[0].value;
            var url = REST_API.urls.payors + ";clientId=" + clientId;
            return Sandata.Utility.InitializeExternalData($location, $q, "initializeClientPayorsCache", DashboardService.httpService, "get", url, SandataCache, "clientPayors");
        }
        Utility.Initialize_ClientPayorsCache = Initialize_ClientPayorsCache;

        function DashboardClientEmployeeLoader($location, $q, REST_API, DashboardFactory, DashboardService, SandataCache) {
            return Sandata.Utility.InitializeExternalData($location, $q, "loadClientEmployee", DashboardService.httpService, "get", REST_API.urls.accountdata, SandataCache, "accountdata", DashboardService.setClientsEmployees, DashboardFactory, "Dashboard Page");
        }
        Utility.DashboardClientEmployeeLoader = DashboardClientEmployeeLoader;

        function DashboardPayorLoader($location, $q, REST_API, DashboardFactory, DashboardService, SandataCache) {
            var url = REST_API.urls.payors;

            return Sandata.Utility.InitializeExternalData($location, $q, "loadClientPayors", DashboardService.httpService, "get", url, SandataCache, "payor", DashboardService.setPayors, DashboardFactory, "Dashboard Page");
        }
        Utility.DashboardPayorLoader = DashboardPayorLoader;

        function ClientEmployeeLoader($location, $q, REST_API, VisitsFactory, VisitsService, SandataCache) {
            return Sandata.Utility.InitializeExternalData($location, $q, "loadClientEmployee", VisitsService.httpService, "get", REST_API.urls.accountdata, SandataCache, "accountdata", VisitsService.setClientsEmployees, VisitsFactory, "Visits Page");
        }
        Utility.ClientEmployeeLoader = ClientEmployeeLoader;

        //dmr
        function ReportDataLoader($location, $q, REST_API, ReportingFactory, ReportingService, SandataCache) {
            return Sandata.Utility.InitializeExternalData($location, $q, "loadReportData", ReportingService.httpService, "get", REST_API.urls.accountdata, SandataCache, "accountdata", ReportingService.setReportingData, ReportingFactory, "Report Page", true);
        }
        Utility.ReportDataLoader = ReportDataLoader;

        function ReportViewLoader($location, $q, REST_API, ReportingFactory, ReportingService, SandataCache) {
            return Sandata.Utility.InitializeExternalData($location, $q, "loadReportView", ReportingService.httpService, "get", REST_API.urls.view_reports, SandataCache, "reportViewData", ReportingService.setReportViewData, ReportingFactory, "Report Page", true);
        }
        Utility.ReportViewLoader = ReportViewLoader;

        //dmr
        function PayorLoader($location, $q, REST_API, VisitsFactory, VisitsService, SandataCache) {
            var url = REST_API.urls.payors;

            return Sandata.Utility.InitializeExternalData($location, $q, "loadPayor", VisitsService.httpService, "get", url, SandataCache, "payor", VisitsService.setPayors, VisitsFactory, "Visits Page");
        }
        Utility.PayorLoader = PayorLoader;

        // Load all payors for the first client in the client list.
        function ClientPayorsLoader($location, $q, REST_API, DashboardFactory, DashboardService, SandataCache) {
            var accountdata = SandataCache.get("accountdata");
            var clientId = accountdata.sections.accountdata_parameters.base_components.CLIENT_NAMES.option_components[0].value;
            var url = REST_API.urls.payors + ";clientId=" + clientId;
            return Sandata.Utility.InitializeExternalData($location, $q, "loadClientPayors", DashboardService.httpService, "get", url, SandataCache, "clientPayors", DashboardService.setClientPayors, DashboardFactory, "Dashboard Page");
        }
        Utility.ClientPayorsLoader = ClientPayorsLoader;

        // Dashboard metadata loader
        function DashboardMetadataLoader($location, $q, REST_API, DashboardFactory, DashboardService, SandataCache) {
            // Construct the url to load the dashboard metadata
            var visitsStartDate = "";
            var visitsEndDate = "";
            var authorizationStartDate = "";
            var authorizationEndDate = "";
            var clientId = "";
            var payor = "";

            var accountdata = SandataCache.get("accountdata");
            var payorList = SandataCache.get("payor");
            if (accountdata != undefined) {
                visitsStartDate = accountdata.sections.accountinfo_section.base_components.VISIT_HISTORY_START_DATE.display_value;
                visitsEndDate = accountdata.sections.accountinfo_section.base_components.VISIT_HISTORY_END_DATE.display_value;
                authorizationStartDate = accountdata.sections.accountinfo_section.base_components.AUTHORIZATION_HISTORY_START_DATE.display_value;
                authorizationEndDate = accountdata.sections.accountinfo_section.base_components.AUTHORIZATION_HISTORY_END_DATE.display_value;
                clientId = accountdata.sections.accountdata_parameters.base_components.CLIENT_NAMES.option_components[0].value;
            }
            if (payorList != undefined) {
                payor = payorList[0].key;
            }
            var url = REST_API.urls.dashboard_metadata + ";visitsStartDate=" + visitsStartDate + ";visitsEndDate=" + visitsEndDate + ";authorizationStartDate=" + authorizationStartDate + ";authorizationEndDate=" + authorizationEndDate + ";clientId=" + clientId + ";payor=" + payor;
            return Sandata.Utility.InitializeExternalData($location, $q, "loadDashboardMetadata", DashboardService.httpService, "get", url, SandataCache, "dashboardMetadata", DashboardService.setMetadata, DashboardFactory, "Dashboard Page");
        }
        Utility.DashboardMetadataLoader = DashboardMetadataLoader;

        function InitializeExternalData($location, $q, resolve_key, ajax, type, url, cache, cache_key, successCallback, service, page, hideLoader) {
            ShowPageLoaderGraphic();
            var deferred = $q.defer();

            var credentials = cache.get("header_credentials");

            if (!credentials) {
                $location.url("/");
                deferred.reject("An error occured while fetching items");
            } else {
                var config = cache.get("header_credentials");

                if (cache_key !== "accountdata") {
                    var accountdata = cache.get("accountdata");
                    if (typeof accountdata != "undefined") {
                        config.headers["militaryTimeFormatFlag"] = accountdata.sections.userinfo_section.base_components.MILITARY_TIME_FORMAT.display_value;
                    }
                }

                if (cache != undefined) {
                    var cachedData = cache.get(cache_key);

                    if (cachedData != undefined) {
                        //console.log("Getting '" +  cache_key + "' Cached Data for -- " + page );
                        if (successCallback && service) {
                            successCallback(cachedData, service);
                        }
                        deferred.resolve(resolve_key);
                        HidePageLoaderGraphic();
                    } else {
                        ajax[type](url, config).success(function (response, status) {
                            //console.log("Response from Account Data:", response)
                            //console.log("Getting '" +  cache_key + "' AJAX DATA  BUT Adding Key To The Cache Factory for -- " + page );
                            cache.put(cache_key, response.data);

                            if (successCallback && service) {
                                successCallback(response.data, service);
                            }

                            //dmr -- Call listeners that account data is ready
                            if (cache_key == 'accountdata') {
                                if (Utility.AccountDataReadyListeners != undefined) {
                                    for (var key in Utility.AccountDataReadyListeners) {
                                        //console.log("key: ",key, "Response: ",response)
                                        var callback = Utility.AccountDataReadyListeners[key];
                                        callback(response.data);
                                    }
                                }
                            }

                            //dmr --
                            deferred.resolve(resolve_key);
                            if (hideLoader)
                                HidePageLoaderGraphic();
                        }).error(function (response, status) {
                            StatusErrorCodeHandler(status);

                            //Sending a friendly error message in case of failure
                            HidePageLoaderGraphic();
                            deferred.reject("An error occured while fetching items");
                        });
                    }
                } else {
                    HidePageLoaderGraphic();
                    deferred.reject("An error occured while fetching items");
                    StatusErrorCodeHandler();
                }
            }

            //Returning the promise object
            return deferred.promise;
        }
        Utility.InitializeExternalData = InitializeExternalData;

        function QueryService_GET(objParam) {
            ShowPageLoaderGraphic();

            var deferred = objParam.q.defer();
            var data = {};
            var credentials = objParam.cache.get("header_credentials");

            if (!credentials) {
                deferred.reject("An error occured while fetching items");
            } else {
                for (var key in objParam.params) {
                    objParam.url = objParam.url + ";" + key + "=" + objParam.params[key];
                }

                var config = {};
                config = objParam.cache.get("header_credentials");

                config.headers["X-HTTP-Method-Override"] = "GET";
                var accountdata = objParam.cache.get("accountdata");
                config.headers["militaryTimeFormatFlag"] = accountdata.sections.userinfo_section.base_components.MILITARY_TIME_FORMAT.display_value;

                objParam.ajax.get(objParam.url, config).success(function (response, status) {
                    if (objParam.cache_key) {
                        objParam.cache.put(objParam.cache_key, response.data);
                    }

                    if (objParam.successCallback && objParam.service) {
                        objParam.successCallback(response.data, objParam.service);
                    }
                    deferred.resolve(response);
                    HidePageLoaderGraphic();
                }).error(function (response, status) {
                    StatusErrorCodeHandler(status);

                    //Sending a friendly error message in case of failure
                    HidePageLoaderGraphic();
                    deferred.reject("An error occured while fetching items");
                });
            }

            return deferred.promise;
        }
        Utility.QueryService_GET = QueryService_GET;

        function QueryService_POST(objParam) {
            ShowPageLoaderGraphic();

            var deferred = objParam.q.defer();
            var data = {};
            var credentials = objParam.cache.get("header_credentials");

            if (!credentials) {
                deferred.reject("An error occured while fetching items");
            } else {
                if (objParam.params) {
                    data = objParam.params;
                }
                var config = {};
                config = objParam.cache.get("header_credentials");
                config.headers["X-HTTP-Method-Override"] = "POST";
                var accountdata = objParam.cache.get("accountdata");
                config.headers["militaryTimeFormatFlag"] = accountdata.sections.userinfo_section.base_components.MILITARY_TIME_FORMAT.display_value;

                objParam.ajax.post(objParam.url, objParam.params, config).success(function (response, status) {
                    if (objParam.cache_key) {
                        objParam.cache.put(objParam.cache_key, response.data);
                    }

                    if (objParam.successCallback && objParam.service) {
                        objParam.successCallback(response.data, objParam.service);
                    }
                    deferred.resolve(response);
                    HidePageLoaderGraphic();
                }).error(function (response, status) {
                    StatusErrorCodeHandler(status);
                    HidePageLoaderGraphic();

                    //Sending a friendly error message in case of failure
                    deferred.reject("An error occured while fetching items");
                });
            }

            return deferred.promise;
        }
        Utility.QueryService_POST = QueryService_POST;

        function QueryService_PUT(objParam) {
            ShowPageLoaderGraphic();

            var deferred = objParam.q.defer();
            var data = {};
            var credentials = objParam.cache.get("header_credentials");

            if (!credentials) {
                deferred.reject("An error occured while fetching items");
            } else {
                if (objParam.params) {
                    data = objParam.params;
                }

                var config = {};
                config = objParam.cache.get("header_credentials");
                config.headers["X-HTTP-Method-Override"] = "PUT";
                var accountdata = objParam.cache.get("accountdata");
                config.headers["militaryTimeFormatFlag"] = accountdata.sections.userinfo_section.base_components.MILITARY_TIME_FORMAT.display_value;

                objParam.ajax.put(objParam.url, objParam.params, config).success(function (response, status) {
                    if (objParam.cache_key) {
                        objParam.cache.put(objParam.cache_key, response.data);
                    }

                    if (objParam.successCallback && objParam.service) {
                        objParam.successCallback(response.data, objParam.service);
                    }
                    deferred.resolve(response);
                    HidePageLoaderGraphic();
                }).error(function (response, status) {
                    StatusErrorCodeHandler(status);
                    HidePageLoaderGraphic();

                    //Sending a friendly error message in case of failure
                    deferred.reject("An error occured while fetching items");
                });
            }

            return deferred.promise;
        }
        Utility.QueryService_PUT = QueryService_PUT;

        function AssignToScope($scope, obj) {
            for (var key in obj) {
                $scope[key] = obj[key];
            }
        }
        Utility.AssignToScope = AssignToScope;

        function RemoveFromScope($scope, obj) {
            for (var key in obj) {
                delete $scope[key];
            }
        }
        Utility.RemoveFromScope = RemoveFromScope;

        function RemoveAnObjectFromScope($scope, key) {
            delete $scope[key];
        }
        Utility.RemoveAnObjectFromScope = RemoveAnObjectFromScope;

        function AssignObjectsInArrayToScopeObject($scope, obj, key, scope_key, erase) {
            if (!erase) {
            } else {
                $scope[scope_key] = {};
            }
            for (var i = 0; i < obj.length; i++) {
                $scope[scope_key][obj[i][key]] = obj[i];
            }
        }
        Utility.AssignObjectsInArrayToScopeObject = AssignObjectsInArrayToScopeObject;

        function GetNumberOfHours(visitDate, time_in, time_out, options) {
            var formatTme = false;
            if (time_out != "" && time_in != "") {
                if (!visitDate) {
                    visitDate = '01/01/2007';
                }
                var _date = $.datepicker.formatDate('M d, yy', new Date(visitDate));
                var _adjIn = _date + " " + time_in;
                var _adjOut = _date + " " + time_out;

                if (options) {
                    if (new Date(_adjOut) <= new Date(_adjIn) && options._24_hours) {
                        var tmp_adjOutDate = new Date(visitDate);
                        tmp_adjOutDate.setDate(tmp_adjOutDate.getDate() + 1);

                        var _futurDate = $.datepicker.formatDate('M d, yy', tmp_adjOutDate);
                        _adjOut = _futurDate + " " + time_out;
                    }

                    if (options.formatTime) {
                        formatTme = options.formatTime;
                    }
                }

                if (formatTme) {
                    var minutes = (((new Date(_adjOut) - new Date(_adjIn)) / 1000) / 60);
                    return Sandata.Utility.FormatTime({
                        format: "hm",
                        rawTime: minutes
                    });
                } else {
                    var hrs = (((new Date(_adjOut) - new Date(_adjIn)) / 1000) / 60) / 60;
                    if (isNaN(hrs)) {
                        return "";
                    } else {
                        //hrs = Math.floor(hrs * 100) / 100;
                        //return hrs.toFixed(2);
                        hrs = Sandata.Utility.RoundToTwo(hrs);
                        return hrs.toFixed(2);
                    }
                }
            } else {
                return "";
            }
        }
        Utility.GetNumberOfHours = GetNumberOfHours;

        function SetDefaultSelectValue($scope, data, variable, obj, index, byvalue) {
            if (isNaN(index) && index != undefined || byvalue == true) {
                var slot = 0;

                jQuery.grep(data, function (passesObj, i) {
                    if (passesObj.value === index) {
                        slot = i;
                        //console.log("This is the Found Slot: ", slot)
                    }
                });

                $scope[variable] = data;

                //console.log("To Be Selected: ", data[slot]);
                $scope[obj] = data[slot];
                // console.log("New Object Created:",  $scope[obj]);
            } else {
                if (index) {
                    var localIndex = index - 1;
                    $scope[variable] = data;
                    $scope[obj] = $scope[variable][localIndex];
                } else {
                    $scope[variable] = data;
                    $scope[obj] = $scope[variable][0];
                }
            }
        }
        Utility.SetDefaultSelectValue = SetDefaultSelectValue;

        function AssignToScopeAndSetModel(obj) {
            //$scope:any,data:any,variable:any,model:any,arObj:any
            if (obj.index) {
                if (!obj.$scope[obj.variable]) {
                    obj.$scope[obj.variable] = obj.data;
                }
                if (obj.byvalue) {
                    var slot = 0;
                    jQuery.grep(obj.data[obj.arObj], function (passesObj, i) {
                        if (passesObj.value === obj.index) {
                            slot = i;
                        }
                    });
                    obj.$scope[obj.model] = obj.data[obj.arObj][slot];
                } else {
                    var localIndex = obj.index - 1;
                    obj.$scope[obj.model] = obj.data[obj.arObj][localIndex];
                }
            } else {
                if (!obj.$scope[obj.variable]) {
                    obj.$scope[obj.variable] = obj.data;
                }

                obj.$scope[obj.model] = obj.data[obj.arObj][0];
            }
        }
        Utility.AssignToScopeAndSetModel = AssignToScopeAndSetModel;

        function DirectiveAttributeCleaner(attrs, scope) {
            var params = attrs;

            this.BooleanCheck = function (value) {
                if (value == "true") {
                    return true;
                } else {
                    return value;
                }
            };
            this.ConvertData = function (scope, key) {
                if ((scope[key] instanceof Array || scope[key] instanceof Object) && key == "source") {
                    scope["dataSource"] = new kendo.data.DataSource({
                        data: scope[key]
                    });
                    return scope["dataSource"];
                } else if (scope[key] instanceof Array) {
                    return scope[key];
                }
            };

            for (var key in params) {
                params[key] = this.BooleanCheck(params[key]);
            }

            for (var key in scope) {
                if (!key.match(/\$(.)/) && !key.match(/this/)) {
                    params[key] = this.ConvertData(scope, key);
                }
            }

            return params;
        }
        Utility.DirectiveAttributeCleaner = DirectiveAttributeCleaner;

        /**
        * Apply error border to controls
        */
        function setErrorBorderStyle(controlId) {
            if (controlId != undefined) {
                var cid = "#" + controlId;

                if ($(cid).is("input")) {
                    $(cid).parent().css({ "border": "1px solid red" }, { "padding": "3px" });
                } else {
                    $(cid).css({ "border": "1px solid red" });
                }
            }
        }

        /**
        * Remove error border from given control if it exists
        */
        function removeErrorBorderStyle(controlId) {
            if (controlId != undefined) {
                var cid = "#" + controlId;

                $(cid).has("[style*='border']").removeAttr('style');

                var parent = $(cid).parent();

                if (parent != undefined) {
                    parent.has("[style*='border']").removeAttr('style');
                }
            }
        }

        function removeStyles(elm) {
            for (var i = 0; i < $(elm).parsley().items.length; i++) {
                var controlId = $(elm).parsley().items[i].$element[0].id;
                removeErrorBorderStyle(controlId);
            }
        }

        function ValidateForm($scope, elm, useMesageCenter) {
            // remove styles from these controls first... styles will be added back if there is an error
            //dmr -- TODO: Not working properly... removeStyles(elm);
            var errors = [];

            if ($(elm).parsley('validate') == false) {
                //Get Error Count
                var count = 0;
                for (var i = 0; i < $(elm).parsley().items.length; i++) {
                    var controlId = $(elm).parsley().items[i].$element[0].id;

                    if ($(elm).parsley().items[i].valid == false) {
                        // console.log($(elm).parsley().items[i])
                        if (useMesageCenter) {
                            errors.push({ text: $(elm).parsley().items[i].options.errorMessage, header: $(elm).parsley().items[i].options.errorHeader });
                        }

                        count++;
                        //dmr -- setErrorBorderStyle(controlId);
                    }
                }

                $scope.toggleSidePanel(true);
                if (useMesageCenter) {
                    return errors;
                } else {
                    return false;
                }
            } else {
                $("#widgets-error-container").html("");

                if ($("#widgets-error-container").is(":visible") == true) {
                    $scope.toggleSidePanel(false);
                }

                if (useMesageCenter) {
                    return errors;
                } else {
                    return true;
                }
            }
        }
        Utility.ValidateForm = ValidateForm;

        function myRepSubmit() {
            mySubmit($("form#reporting_form"), Sandata.Utility.BaseURL + "/servlet/Parsesc?REPORTS=");
        }
        Utility.myRepSubmit = myRepSubmit;

        function Modal($scope, modal, show, title, fields, actions) {
            $scope[modal].show = show;
            if (title == undefined)
                title = "";
            $scope[modal].title = title;
            if (fields) {
                for (var key in fields) {
                    $scope[modal][key] = fields[key];
                }
            }
            if (actions) {
                if (actions.scrollToTop)
                    window.scroll(0, 0);
            }
        }
        Utility.Modal = Modal;

        function Setup_ShowHideSidePanel($scope, sidePanel, show, focusElement, nodeType, click) {
            $scope[sidePanel] = {};
            if (show == 1) {
                $scope[sidePanel].show = true;
                $scope[sidePanel].content_class = "span9";
            } else if (show == 0) {
                $scope[sidePanel].show = false;
                $scope[sidePanel].content_class = "span11";
            } else {
                $scope[sidePanel].show = false;
                $scope[sidePanel].content_class = "span11";
            }

            $scope.toggleSidePanel = function (show) {
                if (show) {
                    $scope.sidepanel = 1;
                    $scope[sidePanel].show = true;
                    $scope[sidePanel].content_class = "span9";
                    Sandata.Utility.FocusOnElement($scope, focusElement, nodeType, click);
                } else {
                    if ($scope[sidePanel].show == true) {
                        $scope.sidepanel = 0;
                        $scope[sidePanel].show = false;
                        $scope[sidePanel].content_class = "span11";
                    } else {
                        $scope.sidepanel = 1;
                        $scope[sidePanel].show = true;
                        $scope[sidePanel].content_class = "span9";
                        Sandata.Utility.FocusOnElement($scope, focusElement, nodeType, click);
                    }
                }
            };
        }
        Utility.Setup_ShowHideSidePanel = Setup_ShowHideSidePanel;

        function FocusOnElement($scope, focusElement, nodeType, click) {
            if (focusElement) {
                $scope[focusElement] = true;
                if (nodeType) {
                    var elm = nodeType + focusElement;
                    $(elm).focus().focus();
                    if (click) {
                        $(elm).click();
                    }
                } else {
                    $("#" + focusElement).focus().focus();
                    if (click) {
                        $("#" + focusElement).click();
                    }
                }
            }
        }
        Utility.FocusOnElement = FocusOnElement;

        function AssignValuesToVisitModal($scope, modalScope, service, visit) {
            if (visit) {
                if (visit.client_id) {
                    Sandata.Utility.AssignToScopeAndSetModel({
                        $scope: $scope[modalScope],
                        data: service.clients,
                        arObj: "option_components",
                        variable: "clients",
                        model: "addVisitClient",
                        index: visit.client_id,
                        byvalue: true });
                } else {
                    Sandata.Utility.AssignToScopeAndSetModel({
                        $scope: $scope[modalScope],
                        data: service.clients,
                        arObj: "option_components",
                        variable: "clients",
                        model: "addVisitClient",
                        index: 0 });
                }

                //Only Show Employees who are supposed to be shown in Modal
                var employees_Temp = angular.copy(service.employees);
                employees_Temp.option_components = jQuery.grep(employees_Temp.option_components, function (passesObj, index) {
                    return passesObj.modal == true;
                });

                if (visit.employee_id) {
                    Sandata.Utility.AssignToScopeAndSetModel({
                        $scope: $scope[modalScope],
                        data: employees_Temp,
                        arObj: "option_components",
                        variable: "employees",
                        model: "addVisitEmployee",
                        index: visit.employee_id,
                        byvalue: true });
                } else {
                    Sandata.Utility.AssignToScopeAndSetModel({
                        $scope: $scope[modalScope],
                        data: service.employees,
                        arObj: "option_components",
                        variable: "employees",
                        model: "addVisitEmployee",
                        index: 0 });
                }

                if (visit.service_id) {
                    //Sandata.Utility.SetDefaultSelectValue($scope[modalScope],service.services,"services","addVisitService",visit.service_id,true);
                    Sandata.Utility.AssignToScopeAndSetModel({
                        $scope: $scope[modalScope],
                        data: service.services,
                        arObj: "option_components",
                        variable: "services",
                        model: "addVisitService",
                        index: visit.service_id,
                        byvalue: true });
                } else {
                    //Sandata.Utility.SetDefaultSelectValue($scope[modalScope],service.services,"services","addVisitService",0);
                    Sandata.Utility.AssignToScopeAndSetModel({
                        $scope: $scope[modalScope],
                        data: service.services,
                        arObj: "option_components",
                        variable: "services",
                        model: "addVisitService",
                        index: 0 });
                }

                //Sandata.Utility.SetDefaultSelectValue($scope,service.reasons_codes,"reasons","addVisitReasonsModel",service.reasons_codes[0],true);
                Sandata.Utility.AssignToScopeAndSetModel({
                    $scope: $scope[modalScope],
                    data: service.reasons_codes,
                    arObj: "option_components",
                    variable: "reasons",
                    model: "addVisitReasonsModel",
                    index: 0 });

                visit.evv_in = (visit.evv_in == null) ? "" : visit.evv_in.toString();
                visit.evv_out = (visit.evv_out == null) ? "" : visit.evv_out.toString();
                visit.start_time = (visit.start_time == null) ? "" : visit.start_time.toString();
                visit.end_time = (visit.end_time == null) ? "" : visit.end_time.toString();

                $scope[modalScope].EVVIn = visit.evv_in_str;
                $scope[modalScope].EVVOut = visit.evv_out_str;
                $scope[modalScope].VisitADJIn = visit.start_time_str;
                $scope[modalScope].VisitADJOut = visit.end_time_str;
                $scope[modalScope].VisitDate = $.datepicker.formatDate('mm/dd/yy', new Date(visit.service_date));
                $scope[modalScope].VisitNotes = visit.visit_notes;
                $scope[modalScope].changeReasonsList = visit.reason_code_history;
                $scope[modalScope].TotalAdjHrs = visit.hours;
                $scope[modalScope].remarksModel = visit.remarks;
                $scope[modalScope].errorMessages = visit.error_messages;
                $scope[modalScope].hasErrorMessages = visit.has_error_messages;
                $scope[modalScope].changeReasonsModel = "";
                $scope[modalScope].overnight = visit.over_night_visit;
            } else {
                var add_client, add_employee;

                if ($scope.clientModel.value) {
                    add_client = $scope.clientModel.value;
                } else {
                    add_client = $scope.clientModel;
                }
                Sandata.Utility.AssignToScopeAndSetModel({
                    $scope: $scope[modalScope],
                    data: service.clients,
                    arObj: "option_components",
                    variable: "clients",
                    model: "addVisitClient",
                    index: add_client,
                    byvalue: true });

                //Only Show Employees who are supposed to be shown in Modal
                var employees_Temp = angular.copy(service.employees);
                employees_Temp.option_components = jQuery.grep(employees_Temp.option_components, function (passesObj, index) {
                    return passesObj.modal == true;
                });

                if ($scope.employeeModel.value) {
                    add_employee = $scope.employeeModel.value;
                } else {
                    add_employee = $scope.employeeModel;
                }
                Sandata.Utility.AssignToScopeAndSetModel({
                    $scope: $scope[modalScope],
                    data: employees_Temp,
                    arObj: "option_components",
                    variable: "employees",
                    model: "addVisitEmployee",
                    index: add_employee,
                    byvalue: true });

                //Sandata.Utility.SetDefaultSelectValue($scope[modalScope],service.services,"services","addVisitService",0);
                Sandata.Utility.AssignToScopeAndSetModel({
                    $scope: $scope[modalScope],
                    data: service.services,
                    arObj: "option_components",
                    variable: "services",
                    model: "addVisitService",
                    index: 0 });

                //Sandata.Utility.SetDefaultSelectValue($scope,service.reasons_codes,"reasons","addVisitReasonsModel",service.reasons_codes[0].value,true);
                Sandata.Utility.AssignToScopeAndSetModel({
                    $scope: $scope[modalScope],
                    data: service.reasons_codes,
                    arObj: "option_components",
                    variable: "reasons",
                    model: "addVisitReasonsModel",
                    index: 0 });

                $scope[modalScope].EVVIn = "";
                $scope[modalScope].EVVOut = "";
                $scope[modalScope].VisitADJIn = "";
                $scope[modalScope].VisitADJOut = "";
                $scope[modalScope].VisitDate = ""; //$.datepicker.formatDate('mm/dd/yy', new Date());
                $scope[modalScope].VisitNotes = "";
                $scope[modalScope].changeReasonsList = [];
                $scope[modalScope].TotalAdjHrs = "";
                $scope[modalScope].remarksModel = "";
                $scope[modalScope].changeReasonsModel = "";
                $scope[modalScope].errorMessages = [];
                $scope[modalScope].hasErrorMessages = [];
                $scope[modalScope].overnight = false;
            }
        }
        Utility.AssignValuesToVisitModal = AssignValuesToVisitModal;

        function DisplayMessageCenter(objParams) {
            var alertStyle = "";

            if (objParams.errorCheck.status == "FAILED") {
                objParams.type = "error";
                objParams.text = objParams.errorCheck.error_message;
            }
            switch (objParams.type) {
                case "error":
                    alertStyle = "alert alert-error";
                    break;
                case "info":
                    alertStyle = "alert alert-info";
                    break;
                case "approved":
                    alertStyle = "alert alert-block";
                    break;
                case "added":
                    alertStyle = "alert alert-block";
                    break;
                default:
                    alertStyle = "alert alert-block";
                    break;
            }

            if (objParams.messageCenter) {
                var tmp_msgCenter = objParams.messageCenter;
                if (!objParams.scope[tmp_msgCenter])
                    objParams.scope[tmp_msgCenter] = [];
            } else if (objParams.scope["messageCenter"]) {
                var tmp_msgCenter = "messageCenter";
            } else {
                objParams.scope["messageCenter"] = [];
                var tmp_msgCenter = "messageCenter";
            }

            objParams.scope[tmp_msgCenter].push({ "alert": alertStyle, "type": objParams.type, "text": objParams.text, "header": objParams.header, "hours": objParams.hours });
            objParams.scope.toggleSidePanel(true);
            if (objParams.trigger)
                $(objParams.trigger).trigger("click");

            if (objParams.actions && objParams.actions.length > 0) {
                for (var i = 0; i < objParams.actions.length; i++) {
                    objParams.actions[i]();
                }
            }

            /*if( $("#widgets-area button").length ==1){
            $($("#widgets-area button")[0]).focus().focus();
            }else if($("#widgets-area button").length > 1){
            $($("#widgets-area button")[$("#widgets-area button").length-1]).focus().focus();
            }else{}*/
            /*setTimeout(function(){
            do{
            $($("#widgets-area button")[objParams.scope.messageCenter.length-1]).focus().focus();
            }while(   $($("#widgets-area button")[objParams.scope.messageCenter.length-1]).length  == 0 )
            
            },0);*/
            if (objParams.$timeout) {
                objParams.$timeout(function () {
                    if ($("#widgets-area").length > 0) {
                        //console.log("Length", objParams.scope.messageCenter.length);
                        $($("#widgets-area button")[objParams.scope.messageCenter.length - 1]).focus().focus();
                    } else if ($("#messageTab").length > 0) {
                        $("#messageTab span span span span div:last-child button").focus().focus();
                    } else {
                    }
                }, 1000);
            }

            if (typeof objParams.scope.removeAlert == "undefined") {
                objParams.scope.removeAlert = function (index) {
                    if (objParams.scope.messageCenter.length && objParams.scope[tmp_msgCenter].length > 0) {
                        objParams.scope.messageCenter.splice(index, 1);
                    }
                };
            }
        }
        Utility.DisplayMessageCenter = DisplayMessageCenter;

        function RemoveMessages(obj) {
            if (obj.scope.messageCenter) {
                $.grep(obj.scope.messageCenter, function (message, index) {
                    for (var i = 0, msgLength = obj.messageHeaders.length; i < msgLength; i++) {
                        if (message.header == obj.messageHeaders[i]) {
                            obj.scope.messageCenter.splice(index, 1);
                        }
                    }
                });
            }
        }
        Utility.RemoveMessages = RemoveMessages;

        function DisableElement(scope, key) {
            if (scope[key] == undefined) {
                scope[key] = false;
            }

            var keyFnName = "toggle_" + key;
            scope[keyFnName] = function () {
                if (!scope[key]) {
                    scope[key] = true;
                } else {
                    scope[key] = false;
                }
            };
        }
        Utility.DisableElement = DisableElement;

        function StatusErrorCodeHandler(status) {
            switch (status) {
                case 500:
                    alert("There was a Service Error. You will be redirected to the login page. If the Error continues to persist, then please contact customer service.");
                    window.location = "http://access.santrax.com"; //Sandata.Utility.BaseURL;
                    break;
                case 401:
                    alert("You are not authorized to view this page and/or content.  You will be redirected to the login page.  If the Error continues to persist, then please contact customer service.");
                    window.location = "http://access.santrax.com"; //Sandata.Utility.BaseURL;
                    break;
                case 404:
                    alert("The page or content that you are trying to view can not be found at this time.");
                    break;
                case 403:
                    alert("The page or content that you are trying to view is Forbidden. You will be redirected to the login page. Please try again. If the Error continues to persist, then please contact customer service.");
                    window.location = "http://access.santrax.com"; //Sandata.Utility.BaseURL;
                    break;
                case 0:
                    alert("Your session has expired. You will be redirected to the log in page.");
                    window.location = "http://access.santrax.com"; //Sandata.Utility.BaseURL;
                    break;
                default:
                    alert("An error has occured. You will be redirected to the login page. If the Error continues to persist, then please contact customer service.");
                    window.location = "http://access.santrax.com"; //Sandata.Utility.BaseURL;
                    break;
            }
        }
        Utility.StatusErrorCodeHandler = StatusErrorCodeHandler;

        function FormatDate(objParam) {
            if (objParam.timeStampFormat) {
                var arStr = objParam.date.match(/([0-9])+/gi);
                var dateStr = arStr[1] + "/" + arStr[2] + "/" + arStr[0];
            } else if (objParam.convertToTimeStampFormat) {
                var arStr = objParam.date.match(/([0-9])+/gi);
                var dateStr = arStr[2] + "-" + arStr[0] + "-" + arStr[1];
            } else if (objParam.convertFromRawDate) {
                var d = new Date(objParam.date), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();

                if (month.length < 2)
                    month = '0' + month;
                if (day.length < 2)
                    day = '0' + day;

                var dateStr = [month, day, year].join('/');
            } else {
                var dateStr = objParam.date.replace(/\-/gi, "/");
            }

            if (objParam.url_encode) {
                dateStr = encodeURIComponent(dateStr);
            }

            return dateStr;
        }
        Utility.FormatDate = FormatDate;

        function DifferenceBetweenDates(objParam) {
            var returnValue = { status: false, difference: null };
            if (objParam.from && objParam.to) {
                if (objParam.convertToTimeStampFormat) {
                    objParam.from = new Date(Sandata.Utility.FormatDate({ date: objParam.from, convertToTimeStampFormat: objParam.convertToTimeStampFormat }));
                    objParam.to = new Date(Sandata.Utility.FormatDate({ date: objParam.to, convertToTimeStampFormat: objParam.convertToTimeStampFormat }));
                } else {
                    objParam.from = new Date(objParam.from);
                    objParam.to = new Date(objParam.to);
                }

                if (objParam.returnType) {
                    switch (objParam.returnType) {
                        case "seconds":
                            returnValue.difference = (objParam.to - objParam.from) / (1000);
                            break;
                        case "minutes":
                            returnValue.difference = (objParam.to - objParam.from) / (1000 * 60);
                            break;
                        case "hours":
                            returnValue.difference = (objParam.to - objParam.from) / (1000 * 60 * 60);
                            break;
                        case "days":
                            returnValue.difference = (objParam.to - objParam.from) / (1000 * 60 * 60 * 24);
                            break;
                    }

                    returnValue.status = true;
                    return returnValue;
                } else {
                    return returnValue;
                }
            } else {
                return returnValue;
            }
        }
        Utility.DifferenceBetweenDates = DifferenceBetweenDates;

        function CurrentDate() {
            var today = new Date();
            var day = today.getDate();
            var month = today.getMonth() + 1;
            var year = today.getFullYear();
            if (day < 10) {
                day = '0' + day;
            }
            if (month < 10) {
                month = '0' + month;
            }

            return (month + '/' + day + '/' + year);
        }
        Utility.CurrentDate = CurrentDate;

        function TwoWeeksAgoDate() {
            var today = new Date();
            today.setDate(today.getDate() - 14); // minus 14 days
            var day = today.getDate();
            var month = today.getMonth() + 1;
            var year = today.getFullYear();
            if (day < 10) {
                day = '0' + day;
            }
            if (month < 10) {
                month = '0' + month;
            }

            return (month + '/' + day + '/' + year);
        }
        Utility.TwoWeeksAgoDate = TwoWeeksAgoDate;

        function isValidDate(value) {
            if (!Date.valid) {
                (function () {
                    Date.prototype.valid = function () {
                        return isFinite(this);
                    };
                })();
            }

            var bIsValid = (function validStringDate(value) {
                var d = new Date(value);
                return d.valid() && value.split('/')[0] == (d.getMonth() + 1);
            })(value);

            return bIsValid;
        }
        Utility.isValidDate = isValidDate;

        function DashboardPostInitialize() {
            dashboard_template_functions();
        }
        Utility.DashboardPostInitialize = DashboardPostInitialize;

        function RoundToTwo(num) {
            return +(Math.round(num + "e+2") + "e-2");
        }
        Utility.RoundToTwo = RoundToTwo;

        function GetHours(obj) {
            var time;
            if (!isNaN(obj.rawTime)) {
                switch (obj.format) {
                    case "minutes":
                        time = Math.floor(obj.rawTime / 60);
                        break;
                    case "seconds":
                        time = Math.floor((obj.rawTime / 60) / 60);
                        break;
                    case "milliseconds":
                        time = Math.floor(((obj.rawTime / 1000) / 60) / 60);
                        break;
                }
            } else {
                time = "";
            }
            return time;
        }
        Utility.GetHours = GetHours;

        function GetMinutesFromSeconds(rawTime) {
            if (!isNaN(rawTime))
                return (rawTime / 1000) / 60;
            else
                return "";
        }
        Utility.GetMinutesFromSeconds = GetMinutesFromSeconds;

        function CrossesMidnight(obj) {
            if (obj.regex) {
                var timeFormat = new RegExp(obj.regex);

                if (timeFormat.exec(obj.start) && timeFormat.exec(obj.end) && obj.start.match(/am|pm/gi) && obj.end.match(/am|pm/gi)) {
                    var start = timeFormat.exec(obj.start)[1] + ":" + timeFormat.exec(obj.start)[2] + " " + timeFormat.exec(obj.start)[4];
                    var end = timeFormat.exec(obj.end)[1] + ":" + timeFormat.exec(obj.end)[2] + " " + timeFormat.exec(obj.end)[4];
                } else if (timeFormat.exec(obj.start) && timeFormat.exec(obj.end)) {
                    var start = obj.start;
                    var end = obj.end;
                }
            } else {
                var start = obj.start;
                var end = obj.end;
            }
            var strDate = "01/01/2007 ";
            if (obj.date) {
                strDate = obj.date + " ";
            }

            var startTime = strDate + start;
            var endTime = strDate + end;
            var dStart = new Date(startTime);
            var dEnd = new Date(endTime);

            //console.log("Start: ",dStart, "End: ",dEnd, "Crosses over 12AM: ",dStart >= dEnd);
            return dStart >= dEnd;
        }
        Utility.CrossesMidnight = CrossesMidnight;

        function GetMinutesRemaining(obj) {
            //assume minutes for now
            var time;
            if (isNaN(obj.rawTime)) {
                time = 0;
            } else {
                time = (obj.rawTime % 60);
            }
            return time;
        }
        Utility.GetMinutesRemaining = GetMinutesRemaining;

        function GetTimeDifference(time_in, time_out, options) {
            var visitDate;
            if (time_out != "" && time_in != "") {
                if (!options.visitDate) {
                    visitDate = '01/01/2007';
                } else {
                    visitDate = options.visitDate;
                }
                var _date = $.datepicker.formatDate('M d, yy', new Date(visitDate));
                var _adjIn = _date + " " + time_in;
                var _adjOut = _date + " " + time_out;

                if (options) {
                    if (new Date(_adjOut) <= new Date(_adjIn) && options._24_hours) {
                        var tmp_adjOutDate = new Date(visitDate);
                        tmp_adjOutDate.setDate(tmp_adjOutDate.getDate() + 1);

                        var _futurDate = $.datepicker.formatDate('M d, yy', tmp_adjOutDate);
                        _adjOut = _futurDate + " " + time_out;
                    }
                }
                var minutes = ((new Date(_adjOut) - new Date(_adjIn)) / 1000) / 60;
                if (isNaN(minutes)) {
                    return "";
                } else {
                    return minutes;
                }
            } else {
                return "";
            }
        }
        Utility.GetTimeDifference = GetTimeDifference;

        function ConvertTimeToFractions(obj) {
            //Assume minutes for now
            var time;
            if (isNaN(obj.rawTime)) {
                time = "";
            } else {
                time = (obj.rawTime / 60);
                if (obj.truncate) {
                    //Truncate
                    time = Math.floor(time * 100) / 100;
                    time = time.toFixed(2);
                } else {
                    //Default - Round
                    time = Sandata.Utility.RoundToTwo(time);
                    time = time.toFixed(2);
                }
            }

            return time;
        }
        Utility.ConvertTimeToFractions = ConvertTimeToFractions;
        function ConvertMilitaryTime(time) {
            time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

            if (time.length > 1) {
                time = time.slice(1); // Remove full string match value
                time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
                time[0] = +time[0] % 12 || 12; // Adjust hours
                return time.join('');
            } else {
                return "12:00 AM";
            }
        }
        Utility.ConvertMilitaryTime = ConvertMilitaryTime;
        function FormatTime(obj) {
            if ((isNaN(obj.rawTime) || !obj.difference) && !obj.format) {
                return "";
            } else if (obj.time_in && obj.time_out && obj.difference) {
                var time;
                if (obj.difference) {
                    var timeFormat = /^(0?[1-9]|1[012])(:[0-5]\d)(\s?)([APap][mM])$/;
                    if (obj.time_in.match(timeFormat) != null && obj.time_out.match(timeFormat) != null) {
                        obj.time_in = obj.time_in.match(timeFormat)[1] + obj.time_in.match(timeFormat)[2] + " " + obj.time_in.match(timeFormat)[4];
                        obj.time_out = obj.time_out.match(timeFormat)[1] + obj.time_out.match(timeFormat)[2] + " " + obj.time_out.match(timeFormat)[4];
                    }

                    obj.rawTime = Sandata.Utility.GetTimeDifference(obj.time_in, obj.time_out, obj.differenceOpts);
                }
                switch (obj.format) {
                    case "hm":
                        if (obj.partial == "h") {
                            if (obj.numberOnly)
                                time = Sandata.Utility.GetHours({ rawTime: obj.rawTime, format: "minutes" }); //Math.floor( obj.rawTime / 60);
                            else
                                time = Sandata.Utility.GetHours({ rawTime: obj.rawTime, format: "minutes" }) + "h";
                        } else if (obj.partial == "m") {
                            if (obj.numberOnly)
                                time = Sandata.Utility.GetMinutesRemaining({ rawTime: obj.rawTime }); //(obj.rawTime % 60);
                            else
                                time = Sandata.Utility.GetMinutesRemaining({ rawTime: obj.rawTime }) + "m";
                        } else {
                            time = Sandata.Utility.GetHours({ rawTime: obj.rawTime, format: "minutes" }) + "h " + Sandata.Utility.GetMinutesRemaining({ rawTime: obj.rawTime }) + "m ";
                        }
                        break;
                    case "decimal":
                        time = Sandata.Utility.ConvertTimeToFractions({ rawTime: obj.rawTime });
                        break;
                    case "none":
                        time = obj.rawTime;
                        if (obj.round) {
                            if (obj.roundingFormat === "decimal") {
                                time = Sandata.Utility.RoundToTwo(time);
                                time = time.toFixed(2);
                            }
                        }
                        break;
                    case "12h":
                        time = Sandata.Utility.ConvertMilitaryTime(obj.rawTime);
                        break;
                    default:
                        time = "";
                        break;
                }

                return time;
            } else if (obj.millitarTime && obj.format) {
                var time;
                var timestampString = "01/01/2007 " + obj.millitarTime;
                var dDate = new Date(timestampString);
                obj.rawTime = dDate.getTime();

                switch (obj.format) {
                    case "hm":
                        if (obj.partial == "h") {
                            if (obj.numberOnly)
                                time = Sandata.Utility.GetHours({ rawTime: obj.rawTime, format: "minutes" }); //Math.floor( obj.rawTime / 60);
                            else
                                time = Sandata.Utility.GetHours({ rawTime: obj.rawTime, format: "minutes" }) + "h";
                        } else if (obj.partial == "m") {
                            if (obj.numberOnly)
                                time = Sandata.Utility.GetMinutesRemaining({ rawTime: obj.rawTime }); //(obj.rawTime % 60);
                            else
                                time = Sandata.Utility.GetMinutesRemaining({ rawTime: obj.rawTime }) + "m";
                        } else {
                            time = Sandata.Utility.GetHours({ rawTime: obj.rawTime, format: "minutes" }) + "h " + Sandata.Utility.GetMinutesRemaining({ rawTime: obj.rawTime }) + "m ";
                        }
                        break;
                    case "decimal":
                        time = Sandata.Utility.ConvertTimeToFractions({ rawTime: obj.rawTime });
                        break;
                    case "none":
                        time = obj.rawTime;
                        if (obj.round) {
                            if (obj.roundingFormat === "decimal") {
                                time = Sandata.Utility.RoundToTwo(time);
                                time = time.toFixed(2);
                            }
                        }
                        break;
                    case "12h":
                        time = Sandata.Utility.ConvertMilitaryTime(obj.millitarTime);
                        break;
                    default:
                        time = "";
                        break;
                }

                return time;
            } else if (!isNaN(obj.rawTime) && obj.format) {
                var time;

                switch (obj.format) {
                    case "hm":
                        if (obj.partial == "h") {
                            if (obj.numberOnly)
                                time = Sandata.Utility.GetHours({ rawTime: obj.rawTime, format: "minutes" }); //Math.floor( obj.rawTime / 60);
                            else
                                time = Sandata.Utility.GetHours({ rawTime: obj.rawTime, format: "minutes" }) + "h";
                        } else if (obj.partial == "m") {
                            if (obj.numberOnly)
                                time = Sandata.Utility.GetMinutesRemaining({ rawTime: obj.rawTime }); //(obj.rawTime % 60);
                            else
                                time = Sandata.Utility.GetMinutesRemaining({ rawTime: obj.rawTime }) + "m";
                        } else {
                            time = Sandata.Utility.GetHours({ rawTime: obj.rawTime, format: "minutes" }) + "h " + Sandata.Utility.GetMinutesRemaining({ rawTime: obj.rawTime }) + "m ";
                        }
                        break;
                    case "decimal":
                        time = Sandata.Utility.ConvertTimeToFractions({ rawTime: obj.rawTime });
                        break;
                    case "none":
                        time = obj.rawTime;
                        if (obj.round) {
                            if (obj.roundingFormat === "decimal") {
                                time = Sandata.Utility.RoundToTwo(time);
                                time = time.toFixed(2);
                            }
                        }
                        break;
                    case "12h":
                        time = Sandata.Utility.ConvertMilitaryTime(obj.rawTime);
                        break;
                    default:
                        time = "";
                        break;
                }

                return time;
            } else {
                return 0.0;
            }
        }
        Utility.FormatTime = FormatTime;

        function GetTimeZone() {
            var date = new Date().toString();
            var tmz = (date.match(/\(([^\)]+)\)$/) || date.match(/([A-Z]+) [\d]{4}$/));
            if (tmz)
                tmz = tmz[1].match(/[A-Z]/g).join("");
            else {
                tmz = "unknown";
            }

            return tmz;
        }
        Utility.GetTimeZone = GetTimeZone;

        function GetLanguage() {
            return (window.navigator.userLanguage || navigator.language);
        }
        Utility.GetLanguage = GetLanguage;
    })(Sandata.Utility || (Sandata.Utility = {}));
    var Utility = Sandata.Utility;
})(Sandata || (Sandata = {}));
app.factory("SandataCache", function ($cacheFactory) {
    return $cacheFactory('SandataCache');
});
var Sandata;
(function (Sandata) {
    /// <reference path="../utilities/Sandata.Utility.ts" />
    (function (Factory) {
        Factory.AccountDataFactory = {};
    })(Sandata.Factory || (Sandata.Factory = {}));
    var Factory = Sandata.Factory;
})(Sandata || (Sandata = {}));
app.factory("AccountDataFactory", function () {
    return Sandata.Factory.AccountDataFactory;
});
var Sandata;
(function (Sandata) {
    /// <reference path="../utilities/Sandata.Utility.ts" />
    (function (Factory) {
        Factory.MainMenuFactory = { show: true, disabled: false, expanded: true, label: "Click to Hide Main Navigation", controls: "sidebar-left" };
    })(Sandata.Factory || (Sandata.Factory = {}));
    var Factory = Sandata.Factory;
})(Sandata || (Sandata = {}));
app.factory("MainMenuFactory", function () {
    return Sandata.Factory.MainMenuFactory;
});
var Sandata;
(function (Sandata) {
    /// <reference path="../utilities/Sandata.Utility.ts" />
    (function (Factory) {
        Factory.ClientPayorFactory = {};
    })(Sandata.Factory || (Sandata.Factory = {}));
    var Factory = Sandata.Factory;
})(Sandata || (Sandata = {}));
app.factory("ClientPayorFactory", function () {
    return Sandata.Factory.ClientPayorFactory;
});
var Sandata;
(function (Sandata) {
    /// <reference path="../utilities/Sandata.Utility.ts" />
    (function (Factory) {
        Factory.SharedDataFactory = {
            current_state: {
                client: null,
                payor: null,
                employee: null,
                pay_period: null
            },
            pages: {
                visits: {
                    filters_section: {
                        ignoreVisitsModel: null,
                        approvedVisitModel: null,
                        incompleteVisitModel: null,
                        visitApprovalReadyModel: null,
                        fromDateModel: null,
                        toDateModel: null
                    },
                    message_center: [],
                    checkboxes: [],
                    scrollPosition: null
                },
                visit_form: null
            }
        };
    })(Sandata.Factory || (Sandata.Factory = {}));
    var Factory = Sandata.Factory;
})(Sandata || (Sandata = {}));
app.factory("SharedDataFactory", function () {
    return Sandata.Factory.SharedDataFactory;
});
var Sandata;
(function (Sandata) {
    /// <reference path="../utilities/Sandata.Utility.ts" />
    (function (Factory) {
        Factory.ReportingFactory = { form: {}, clients: [], contracts: [], programs: [], services: [], supervisors: [], employees: [], departments: [], time_format: {} };
    })(Sandata.Factory || (Sandata.Factory = {}));
    var Factory = Sandata.Factory;
})(Sandata || (Sandata = {}));

app.factory("ReportingFactory", function () {
    return Sandata.Factory.ReportingFactory;
});
var Sandata;
(function (Sandata) {
    /// <reference path="../utilities/Sandata.Utility.ts" />
    (function (Factory) {
        Factory.DashboardFactory = { payors: [], clients: [], dashboard: {}, time_format: {} };
    })(Sandata.Factory || (Sandata.Factory = {}));
    var Factory = Sandata.Factory;
})(Sandata || (Sandata = {}));

app.factory("DashboardFactory", function () {
    return Sandata.Factory.DashboardFactory;
});
var Sandata;
(function (Sandata) {
    /// <reference path="../utilities/Sandata.Utility.ts" />
    (function (Factory) {
        Factory.VisitsFactory = { ignore_visits: {}, employees: [], payors: [], clients: [], grid: [], filters: {}, pay_period: [] };
    })(Sandata.Factory || (Sandata.Factory = {}));
    var Factory = Sandata.Factory;
})(Sandata || (Sandata = {}));

app.factory("VisitsFactory", function () {
    return Sandata.Factory.VisitsFactory;
});
var Sandata;
(function (Sandata) {
    /// <reference path="../utilities/Sandata.Utility.ts" />
    (function (Factory) {
        Factory.VisitMessageFactory = {};
    })(Sandata.Factory || (Sandata.Factory = {}));
    var Factory = Sandata.Factory;
})(Sandata || (Sandata = {}));
app.factory("VisitMessageFactory", function () {
    return Sandata.Factory.VisitMessageFactory;
});
var Sandata;
(function (Sandata) {
    (function (Service) {
        /// <reference path="../factories/Sandata.Factory.AccountData.ts" />
        (function (AppLoader) {
            /*********************
            INITIALIZER SERVICE
            **********************/
            var Initializer = (function () {
                function Initializer($http) {
                    this.httpService = $http;
                }
                Initializer.$inject = ['$http'];
                return Initializer;
            })();
            AppLoader.Initializer = Initializer;
        })(Service.AppLoader || (Service.AppLoader = {}));
        var AppLoader = Service.AppLoader;
    })(Sandata.Service || (Sandata.Service = {}));
    var Service = Sandata.Service;
})(Sandata || (Sandata = {}));

app.service("InitializerService", [
    '$http', function ($http) {
        return new Sandata.Service.AppLoader.Initializer($http);
    }]);
var Sandata;
(function (Sandata) {
    (function (Service) {
        /// <reference path="../factories/Sandata.Factory.MainMenu.ts" />
        (function (MainMenu) {
            /*********************
            INITIALIZER SERVICE
            **********************/
            var Init = (function () {
                function Init($http, MainMenuFactory, $q) {
                    this.httpService = $http;
                    this.MainMenuSate = MainMenuFactory;
                    this.$q = $q;
                    this.defer = this.$q.defer();
                }
                Init.prototype.setVisitbility = function (state) {
                    this.MainMenuSate.show = state;
                    this.defer.notify(this.MainMenuSate.show);
                };

                Init.prototype.getVisitbilityState = function () {
                    return this.MainMenuSate.show;
                };

                Init.prototype.getMainMenuObject = function () {
                    return this.MainMenuSate;
                };

                Init.prototype.toggleVisitbilityState = function () {
                    this.MainMenuSate.show = !this.MainMenuSate.show;
                    this.MainMenuSate.expanded = !this.MainMenuSate.expanded;
                    this.defer.notify(this.MainMenuSate.show);
                };

                Init.prototype.watchVisibility = function () {
                    return this.defer.promise;
                };
                Init.$inject = ['$http', 'MainMenuFactory', '$q'];
                return Init;
            })();
            MainMenu.Init = Init;
        })(Service.MainMenu || (Service.MainMenu = {}));
        var MainMenu = Service.MainMenu;
    })(Sandata.Service || (Sandata.Service = {}));
    var Service = Sandata.Service;
})(Sandata || (Sandata = {}));

app.service("MainMenuService", [
    '$http', 'MainMenuFactory', '$q', function ($http, MainMenuFactory, $q) {
        return new Sandata.Service.MainMenu.Init($http, MainMenuFactory, $q);
    }]);
var Sandata;
(function (Sandata) {
    (function (Service) {
        /// <reference path="../factories/Sandata.Factory.Reporting.ts" />
        (function (Reporting) {
            var Init = (function () {
                function Init($http, $q, REST_URLS, SandataCache, ReportingFactory) {
                    this.$inject = ['$http', '$q', 'SandataCache', 'ReportingFactory'];
                    this.localLayout = ReportingFactory;
                    this.httpService = $http;
                    this.q = $q;
                    this.REST_URLS = REST_URLS;
                    this.SandataCache = SandataCache;
                    this.getReportingLayout(this.setLayout, ReportingFactory);
                }
                Init.prototype.setLayout = function (data, ReportingFactory) {
                    ReportingFactory.form = data;
                };

                Init.prototype.getReportingLayout = function (successCallback, ReportingFactory) {
                    /*this.httpService.post('data/reportingformfilter').success(function (response, status) {
                    
                    console.log("COMPARE: data/reportingformfilter....", response.form);
                    
                    successCallback(response.form, ReportingFactory);
                    });*/
                };

                Init.prototype.getContent = function () {
                    return angular.copy(this.localLayout.form);
                };

                Init.prototype.setReportingData = function (data) {
                    Sandata.Factory.ReportingFactory.clients = data.sections.accountdata_parameters.base_components.CLIENT_NAMES.option_components;
                    Sandata.Factory.ReportingFactory.contracts = data.sections.accountdata_parameters.base_components.CONTRACT_NAMES.option_components;
                    Sandata.Factory.ReportingFactory.programs = data.sections.accountdata_parameters.base_components.PROGRAM_NAMES.option_components;
                    Sandata.Factory.ReportingFactory.services = data.sections.accountdata_parameters.base_components.SERVICE_NAMES.option_components;
                    Sandata.Factory.ReportingFactory.supervisors = data.sections.accountdata_parameters.base_components.SUPERVISOR_NAMES.option_components;
                    Sandata.Factory.ReportingFactory.time_format = data.sections.userinfo_section.base_components.MILITARY_TIME_FORMAT;
                };

                Init.prototype.setReportViewData = function (data) {
                    //console.log("setReportViewData: form: ", data);
                    Sandata.Factory.ReportingFactory.form = data;
                };

                Init.prototype.getReportToken = function () {
                    //dmr TFS#14983: These URL params are not needed since they are included in the header.
                    //dmr var sessionId = encodeURIComponent(this.SandataCache.get("credentials").soid);
                    //dmr var username = encodeURIComponent(this.SandataCache.get("credentials").su);
                    //dmr var password = encodeURIComponent(this.SandataCache.get("credentials").sp);
                    var url = this.REST_URLS.reports_token;

                    //console.log(url);
                    return Sandata.Utility.QueryService_GET({
                        ajax: this.httpService,
                        q: this.q,
                        cache: this.SandataCache,
                        url: url
                    });
                };
                return Init;
            })();
            Reporting.Init = Init;
        })(Service.Reporting || (Service.Reporting = {}));
        var Reporting = Service.Reporting;
    })(Sandata.Service || (Sandata.Service = {}));
    var Service = Sandata.Service;
})(Sandata || (Sandata = {}));

app.service("ReportingService", [
    '$http', '$q', 'REST_API', 'SandataCache', 'ReportingFactory', function ($http, $q, REST_API, SandataCache, ReportingFactory) {
        return new Sandata.Service.Reporting.Init($http, $q, REST_API.urls, SandataCache, ReportingFactory);
    }]);
var Sandata;
(function (Sandata) {
    (function (Service) {
        /// <reference path="../factories/Sandata.Factory.Dashboard.ts" />
        (function (Dashboard) {
            var Init = (function () {
                function Init($http, $q, REST_URLS, SandataCache, DashboardFactory) {
                    this.$inject = ['$http', '$q', 'SandataCache', 'DashboardFactory'];
                    this.localLayout = DashboardFactory;
                    this.httpService = $http;
                    this.q = $q;
                    this.REST_URLS = REST_URLS;
                    this.SandataCache = SandataCache;
                    this.localLayout = DashboardFactory;
                }
                Init.prototype.setClientPayors = function (data, DashboardFactory) {
                };

                Init.prototype.setMetadata = function (data, DashboardFactory) {
                };

                Init.prototype.getPayor = function (id) {
                    return Sandata.Utility.QueryService_GET({
                        ajax: this.httpService,
                        q: this.q,
                        cache: this.SandataCache,
                        params: {
                            clientId: id
                        },
                        url: this.REST_URLS.payors,
                        cache_key: "payor"
                    });
                };

                Init.prototype.getMetadata = function (visitsStartDate, visitsEndDate, authorizationStartDate, authorizationEndDate, clientId, payor) {
                    var url = this.REST_URLS.dashboard_metadata + ";visitsStartDate=" + visitsStartDate + ";visitsEndDate=" + visitsEndDate + ";authorizationStartDate=" + authorizationStartDate + ";authorizationEndDate=" + authorizationEndDate + ";clientId=" + clientId + ";payor=" + payor;
                    return Sandata.Utility.QueryService_GET({
                        ajax: this.httpService,
                        q: this.q,
                        cache: this.SandataCache,
                        url: url,
                        cache_key: "dashboardMetadata"
                    });
                };

                Init.prototype.setLayout = function (data, DashboardFactory) {
                    var section = data.sections.accountdata_parameters.base_components;
                    DashboardFactory.dashboard_data = data;
                    DashboardFactory.payors = section.payor_names.option_components;
                    DashboardFactory.clients = section.client_names.option_components;
                    DashboardFactory.reasons_codes = section.REASONCODE_NAMES.option_components;
                    DashboardFactory.time_format = data.sections.userinfo_section.base_components.MILITARY_TIME_FORMAT;
                    this.localLayout.accountdata = DashboardFactory.dashboard_data;
                    this.localLayout.clients = DashboardFactory.clients;
                    this.localLayout.reasons_codes = DashboardFactory.reasons_codes;
                    this.localLayout.payors = DashboardFactory.payors;
                    this.localLayout.time_format = DashboardFactory.time_format;
                };

                Init.prototype.setClientsEmployees = function (data, DashboardFactory) {
                    DashboardFactory.clients = data.sections.accountdata_parameters.base_components.CLIENT_NAMES;
                    DashboardFactory.employees = data.sections.accountdata_parameters.base_components.EMPLOYEE_NAMES;
                    DashboardFactory.reasons_codes = data.sections.accountdata_parameters.base_components.REASONCODE_NAMES;
                    DashboardFactory.time_format = data.sections.userinfo_section.base_components.MILITARY_TIME_FORMAT;
                    DashboardFactory.dashboard_data = data;
                };

                Init.prototype.setPayors = function (data, DashboardFactory) {
                    DashboardFactory.payors = data;
                };

                Init.prototype.updateDashboardLayout = function (data) {
                    var deferred = this.q.defer();

                    this.httpService.post('data/dashboard', data).success(function (response, status) {
                        deferred.resolve(response);
                    }).error(function () {
                        //Sending a friendly error message in case of failure
                        deferred.reject("An error occured while fetching items");
                    });

                    //Returning the promise object
                    return deferred.promise;
                };

                Init.prototype.getContent = function (data) {
                    /*if(data){
                    this.getDashboardLayout(this.setLayout,this.localLayout,data);
                    //this.localLayout = DashboardFactory;
                    }
                    return this.localLayout;*/
                    return angular.copy(this.localLayout);
                };

                Init.prototype.confirmUnknownCall = function (id, updateId, clientId, reasonCode) {
                    var url = this.REST_URLS["confirm_unknown_call"];
                    var obj = {};

                    obj.id = parseInt(id);
                    obj.updateId = parseInt(updateId);
                    obj.clientId = clientId;
                    obj.reasonCode = reasonCode;

                    return Sandata.Utility.QueryService_PUT({ ajax: this.httpService, q: this.q, cache: this.SandataCache, params: obj, url: url });
                };
                return Init;
            })();
            Dashboard.Init = Init;
        })(Service.Dashboard || (Service.Dashboard = {}));
        var Dashboard = Service.Dashboard;
    })(Sandata.Service || (Sandata.Service = {}));
    var Service = Sandata.Service;
})(Sandata || (Sandata = {}));

app.service("DashboardService", [
    '$http', '$q', 'REST_API', 'SandataCache', 'DashboardFactory', function ($http, $q, REST_API, SandataCache, DashboardFactory) {
        return new Sandata.Service.Dashboard.Init($http, $q, REST_API.urls, SandataCache, DashboardFactory);
    }]);
var Sandata;
(function (Sandata) {
    (function (Service) {
        /// <reference path="../factories/Sandata.Factory.Visits.ts" />
        (function (Visits) {
            var Init = (function () {
                function Init($http, $q, REST_URLS, SandataCache, VisitsFactory) {
                    this.$inject = ['$http', '$q', 'SandataCache', 'VisitsFactory'];
                    this.httpService = $http;
                    this.q = $q;
                    this.REST_URLS = REST_URLS;
                    this.SandataCache = SandataCache;
                    this.localLayout = VisitsFactory;
                }
                Init.prototype.setParametersFilters = function (data, VisitsFactory) {
                    VisitsFactory.filters = data.filters;
                };

                //SET CLIENTS AND Employees
                Init.prototype.setClientsEmployees = function (data, VisitsFactory) {
                    VisitsFactory.clients = data.sections.accountdata_parameters.base_components.CLIENT_NAMES;
                    VisitsFactory.employees = data.sections.accountdata_parameters.base_components.EMPLOYEE_NAMES;
                    VisitsFactory.services = data.sections.accountdata_parameters.base_components.SERVICE_NAMES;
                    VisitsFactory.reasons_codes = data.sections.accountdata_parameters.base_components.REASONCODE_NAMES;
                    VisitsFactory.filters = data.sections.filter_section.base_components.VISIT_FILTER_TABS.tabs;
                    VisitsFactory.filter_controls = data.sections.filter_section.sub_section.VISIT_STATIC_CONTROLS.base_components;
                    VisitsFactory.pay_period = data.sections.accountdata_parameters.base_components.PAYPERIOD_LIST;
                    VisitsFactory.time_format = data.sections.userinfo_section.base_components.MILITARY_TIME_FORMAT;

                    // Labels
                    VisitsFactory.labels = {
                        clients_label: data.sections.accountdata_parameters.base_components.CLIENT_NAMES.select_label,
                        employees_label: data.sections.accountdata_parameters.base_components.EMPLOYEE_NAMES.select_label,
                        services_label: data.sections.accountdata_parameters.base_components.SERVICE_NAMES.select_label,
                        reasons_codes_label: data.sections.accountdata_parameters.base_components.REASONCODE_NAMES.select_label,
                        pay_period_label: data.sections.accountdata_parameters.base_components.PAYPERIOD_LIST.select_label
                    };
                };

                //SET PAYORS
                Init.prototype.setPayors = function (data, VisitsFactory) {
                    VisitsFactory.payors = data;
                };

                Init.prototype.getContent = function () {
                    return angular.copy(this.localLayout);
                };

                Init.prototype.getPayor = function (id) {
                    return Sandata.Utility.QueryService_GET({ ajax: this.httpService, q: this.q, cache: this.SandataCache, params: { clientId: id }, url: this.REST_URLS.payors, cache_key: "payor" });
                };

                Init.prototype.markApproved = function (arVisitIds, arVisitUpdateIds) {
                    var obj = {
                        "visitKeys": arVisitIds.join(),
                        "updateKeys": arVisitUpdateIds.join(),
                        "approve": "true"
                    };

                    //console.log("Approval Object: ",obj);
                    return Sandata.Utility.QueryService_POST({ ajax: this.httpService, q: this.q, cache: this.SandataCache, params: obj, url: this.REST_URLS["approve_visit"] });
                };

                Init.prototype.getVisit = function (id) {
                    var url = this.REST_URLS["view_visit"] + ";visitKey=" + id;

                    return Sandata.Utility.QueryService_GET({ ajax: this.httpService, q: this.q, cache: this.SandataCache, url: url });
                };

                Init.prototype.ignoreVisit = function (id, upid) {
                    var obj = {};
                    var url = this.REST_URLS["ignore_visit"] + ";visitKey=" + id + ";updateId=" + upid + ";ignore=true";
                    return Sandata.Utility.QueryService_POST({ ajax: this.httpService, q: this.q, cache: this.SandataCache, params: obj, url: url });
                };

                Init.prototype.unignoreVisit = function (id, upid) {
                    var obj = {};
                    var url = this.REST_URLS["ignore_visit"] + ";visitKey=" + id + ";updateId=" + upid + ";ignore=false";
                    return Sandata.Utility.QueryService_POST({ ajax: this.httpService, q: this.q, cache: this.SandataCache, params: obj, url: url });
                };

                Init.prototype.unApproveVisit = function (id, upid) {
                    var obj = {
                        "visitKeys": id,
                        "updateKeys": upid,
                        "approve": "false"
                    };

                    return Sandata.Utility.QueryService_POST({ ajax: this.httpService, q: this.q, cache: this.SandataCache, params: obj, url: this.REST_URLS["approve_visit"] });
                };
                Init.prototype.getVisits = function (page, client, payor, employee, ignoreVisits, incompleteVisit, visitApprovalReady, approvedVisit, fromDate, toDate) {
                    var queryString = ";showIgnored=" + ignoreVisits;
                    queryString = queryString + ";showApproved=" + approvedVisit;
                    queryString = queryString + ";showIncomplete=" + incompleteVisit;
                    queryString = queryString + ";showReady=" + visitApprovalReady;

                    //queryString = queryString + ";showAll="             +   false;
                    queryString = queryString + ";fromDate=" + fromDate;
                    queryString = queryString + ";toDate=" + toDate;
                    queryString = queryString + ";clientId=" + client;
                    queryString = queryString + ";employeeId=" + employee;
                    queryString = queryString + ";payor=" + payor;

                    // queryString = queryString + ";showunknownCalls="    +   false;
                    var url = this.REST_URLS.view_vists + queryString;

                    return Sandata.Utility.QueryService_GET({ ajax: this.httpService, q: this.q, cache: this.SandataCache, url: url });
                };
                Init.prototype.editVisit = function (obj) {
                    return Sandata.Utility.QueryService_PUT({ ajax: this.httpService, q: this.q, cache: this.SandataCache, params: obj, url: this.REST_URLS["edit_visit"] });
                };

                Init.prototype.addVisit = function (obj) {
                    return Sandata.Utility.QueryService_POST({ ajax: this.httpService, q: this.q, cache: this.SandataCache, params: obj, url: this.REST_URLS["add_visit"] });
                };
                return Init;
            })();
            Visits.Init = Init;
        })(Service.Visits || (Service.Visits = {}));
        var Visits = Service.Visits;
    })(Sandata.Service || (Sandata.Service = {}));
    var Service = Sandata.Service;
})(Sandata || (Sandata = {}));
app.service("VisitsService", [
    '$http', '$q', 'REST_API', 'SandataCache', 'VisitsFactory', function ($http, $q, REST_API, SandataCache, VisitsFactory) {
        return new Sandata.Service.Visits.Init($http, $q, REST_API.urls, SandataCache, VisitsFactory);
    }]);
var Sandata;
(function (Sandata) {
    (function (Service) {
        /// <reference path="../factories/Sandata.Factory.SharedData.ts" />
        (function (SharedData) {
            var Init = (function () {
                function Init(SharedDataFactory) {
                    this.$inject = ['SharedDataFactory'];
                    this.current_state = SharedDataFactory.current_state;
                    this.pages = SharedDataFactory.pages;
                }
                Init.prototype.setPayor = function (id) {
                    this.current_state.payor = id;
                };
                Init.prototype.getPayor = function () {
                    return this.current_state.payor;
                };
                Init.prototype.setClient = function (id) {
                    this.current_state.client = id;
                };
                Init.prototype.getClient = function () {
                    return this.current_state.client;
                };
                Init.prototype.setEmployee = function (id) {
                    this.current_state.employee = id;
                };
                Init.prototype.getEmployee = function () {
                    return this.current_state.employee;
                };
                Init.prototype.setPayPeriod = function (id) {
                    this.current_state.pay_period = id;
                };
                Init.prototype.getPayPeriod = function () {
                    return this.current_state.pay_period;
                };
                Init.prototype.setVisitPageFilter = function (filter, value) {
                    this.pages.visits.filters_section[filter] = value;
                };
                Init.prototype.getVisitPageFilter = function (filter) {
                    return this.pages.visits.filters_section[filter];
                };
                Init.prototype.getVisitPageFilters = function () {
                    return this.pages.visits.filters_section;
                };
                Init.prototype.getVisitPageMessages = function () {
                    return this.pages.visits.message_center;
                };
                Init.prototype.setVisitPageMessages = function (messages) {
                    this.pages.visits.message_center = messages;
                };
                Init.prototype.pushVisitPageMessages = function (message) {
                    this.pages.visits.message_center.push(message);
                };
                Init.prototype.popVisitPageMessages = function () {
                    return this.pages.visits.message_center.pop();
                };
                Init.prototype.resetVisitPageMessages = function () {
                    this.pages.visits.message_center = [];
                };
                Init.prototype.getVisitForm = function () {
                    return this.pages.visit_form;
                };
                Init.prototype.setVisitForm = function (form) {
                    this.pages.visit_form = form;
                };
                Init.prototype.resetVisitForm = function () {
                    this.pages.visit_form = null;
                };
                Init.prototype.setVisitCheckboxes = function (value) {
                    this.pages.visits.checkboxes = value;
                };
                Init.prototype.getVisitCheckboxes = function () {
                    return this.pages.visits.checkboxes;
                };
                Init.prototype.resetVisitCheckboxes = function () {
                    this.pages.visits.checkboxes.length = 0;
                };
                Init.prototype.setVisitPageScrollPosition = function (value) {
                    this.pages.visits.scrollPosition = value;
                };
                Init.prototype.getVisitPageScrollPosition = function () {
                    return this.pages.visits.scrollPosition;
                };
                Init.prototype.resetVisitPageScrollPosition = function () {
                    this.pages.visits.scrollPosition = null;
                };
                return Init;
            })();
            SharedData.Init = Init;
        })(Service.SharedData || (Service.SharedData = {}));
        var SharedData = Service.SharedData;
    })(Sandata.Service || (Sandata.Service = {}));
    var Service = Sandata.Service;
})(Sandata || (Sandata = {}));
app.service("SharedDataService", [
    'SharedDataFactory', function (SharedDataFactory) {
        return new Sandata.Service.SharedData.Init(SharedDataFactory);
    }]);
var Sandata;
(function (Sandata) {
    (function (Controller) {
        /// <reference path="../services/Sandata.Service.AppLoader.ts" />
        (function (AppLoader) {
            /**
            * INIT
            */
            var Initializer = (function () {
                function Initializer($scope, $location, SandataCache, InitializerService) {
                    Sandata.Utility.ShowPageLoaderGraphic();
                    this.credentials = SandataCache.get("credentials");

                    if (this.credentials != undefined) {
                        $location.url("/dashboard");
                    } else {
                        window.location = "http://access.santrax.com"; //Sandata.Utility.BaseURL
                    }
                }
                Initializer.$inject = ['$scope', '$location', 'SandataCache', 'InitializerService'];
                return Initializer;
            })();
            AppLoader.Initializer = Initializer;
        })(Controller.AppLoader || (Controller.AppLoader = {}));
        var AppLoader = Controller.AppLoader;
    })(Sandata.Controller || (Sandata.Controller = {}));
    var Controller = Sandata.Controller;
})(Sandata || (Sandata = {}));
var Sandata;
(function (Sandata) {
    (function (Controller) {
        /// <reference path="../services/Sandata.Service.AppLoader.ts" />
        (function (Header) {
            /**
            * Index Controller
            */
            var Init = (function () {
                function Init($scope, SandataCache, MainMenuService) {
                    this.$inject = ['$scope', 'SandataCache', 'MainMenuService'];
                    this.ToggleLeftNav = function () {
                        this.MainMenuService.toggleVisitbilityState();
                        //this.leftNavToggler.aria.expanded = this.MainMenuService.getVisitbilityState();
                    };
                    this.SandataCache = SandataCache;
                    this.MainMenuService = MainMenuService;
                    this.$scope = $scope;

                    this.displayed_name = Sandata.Utility.Display_Name();

                    this.base_url = "http://access.santrax.com"; // TODO...

                    var that = this;
                    Sandata.Utility.AccountDataReadyListeners['Index'] = function (data) {
                        //console.log("Sandata.Utility.AccountDataReadyListeners['Index'] .... :", data);
                        var username = Sandata.Utility.Display_Name(data.sections.userinfo_section.base_components.USER_NAME.display_value);
                        that.displayed_name = username;

                        // set report service url
                        var environmentParams = data.sections.accountinfo_section.base_components.ENVIRONMENT_PARAMS.option_components;

                        for (var index = 0; index < environmentParams.length; index++) {
                            if (environmentParams[index].label == "ReportsRenderURL") {
                                Sandata.Utility.ReportServiceURL = environmentParams[index].value;
                            } else if (environmentParams[index].label == "BaseProtocol") {
                                Sandata.Utility.BaseProtocol = environmentParams[index].value;
                            } else if (environmentParams[index].label == "BaseDomain") {
                                Sandata.Utility.BaseDomain = environmentParams[index].value;
                            }
                        }

                        // set the BaseURL
                        Sandata.Utility.BaseURL = Sandata.Utility.BaseProtocol + "://" + Sandata.Utility.BaseDomain;

                        // set report url
                        var reportUrl = Sandata.Utility.ReportServiceURL.replace("${BaseDomain}", Sandata.Utility.BaseDomain);
                        Sandata.Utility.ReportServiceURL = reportUrl;

                        //console.log("Sandata.Utility.ReportServiceURL", Sandata.Utility.ReportServiceURL);
                        //console.log("Sandata.Utility.BaseProtocol", Sandata.Utility.BaseProtocol);
                        //console.log("Sandata.Utility.BaseDomain", Sandata.Utility.BaseDomain);
                        //console.log("Sandata.Utility.BaseURL", Sandata.Utility.BaseURL);
                        that.base_url = Sandata.Utility.BaseURL;
                    };
                    this.leftNavToggler = {
                        aria: this.MainMenuService.getMainMenuObject()
                    };

                    this.MessageCenter = {
                        expanded: this.leftNavToggler.aria.expanded,
                        disabled: this.leftNavToggler.aria.disabled,
                        controls: "sidebar-left",
                        label: "Click to Hide Main Navigation",
                        name: "Hide Main Navigation",
                        title: "Hide Main Navigation"
                    };

                    $scope.Header = this;
                }
                return Init;
            })();
            Header.Init = Init;
        })(Controller.Header || (Controller.Header = {}));
        var Header = Controller.Header;
    })(Sandata.Controller || (Sandata.Controller = {}));
    var Controller = Sandata.Controller;
})(Sandata || (Sandata = {}));
var Sandata;
(function (Sandata) {
    (function (Controller) {
        /// <reference path="../services/Sandata.Service.MainMenu.ts" />
        (function (MainMenu) {
            /**
            * Index Controller
            */
            var Init = (function () {
                function Init($scope, MainMenuService) {
                    this.$inject = ['$scope', 'MainMenuService'];
                    this.MainMenuService = MainMenuService;
                    this.$scope = $scope;
                    this.menuShow = this.MainMenuService.getVisitbilityState();
                    var that = this;
                    this.MainMenuService.watchVisibility().then(null, null, function (show) {
                        that.menuShow = show;
                        if (show) {
                            var span = $('#content').attr('class');
                            var spanNum = parseInt(span.replace(/^\D+/g, ''));
                            var newSpanNum = spanNum - 1;
                            var newSpan = 'span' + newSpanNum;

                            $('#sidebar-left').fadeIn();
                            $('#content').removeClass('span' + spanNum);
                            $('#content').removeClass('full-radius');
                            $('#content').addClass(newSpan);
                            // $("#sidebar-left div ul li:first-child a").focus().focus();C
                        } else {
                            var span = $('#content').attr('class');
                            var spanNum = parseInt(span.replace(/^\D+/g, ''));
                            var newSpanNum = spanNum + 1;
                            var newSpan = 'span' + newSpanNum;

                            $('#content').removeClass('span' + spanNum);
                            $('#content').addClass(newSpan);
                            $('#content').addClass('full-radius');
                        }
                    });
                    $scope.MainMenu = this;
                }
                return Init;
            })();
            MainMenu.Init = Init;
        })(Controller.MainMenu || (Controller.MainMenu = {}));
        var MainMenu = Controller.MainMenu;
    })(Sandata.Controller || (Sandata.Controller = {}));
    var Controller = Sandata.Controller;
})(Sandata || (Sandata = {}));
var Sandata;
(function (Sandata) {
    (function (Controller) {
        /// <reference path="../services/Sandata.Service.AppLoader.ts" />
        (function (Footer) {
            var Init = (function () {
                function Init($scope, SandataCache) {
                    this.$inject = ['$scope', 'SandataCache'];
                    Sandata.Utility.AccountDataReadyListeners['Footer'] = function (data) {
                        $scope.current_unix_date = new Date().getTime();
                        var accountdata = SandataCache.get("accountdata");

                        if (accountdata != null) {
                            $scope.custSupportNumber = accountdata.sections.accountinfo_section.base_components.PHONE_CUST_SUPPORT.display_value;
                            $scope.custSupportNumberLabel = accountdata.sections.accountinfo_section.base_components.PHONE_CUST_SUPPORT.text;
                        }
                    };
                }
                return Init;
            })();
            Footer.Init = Init;
        })(Controller.Footer || (Controller.Footer = {}));
        var Footer = Controller.Footer;
    })(Sandata.Controller || (Sandata.Controller = {}));
    var Controller = Sandata.Controller;
})(Sandata || (Sandata = {}));
var Sandata;
(function (Sandata) {
    (function (Controller) {
        /// <reference path="../services/Sandata.Service.Reporting.ts" />
        (function (Reporting) {
            /**
            * Reports
            */
            var Init = (function () {
                function Init($scope, SharedDataService, ReportingService, SandataCache, REGEXP_FORMATS, $timeout) {
                    this.$inject = ['$scope', 'SharedDataService', 'ReportingService', 'SandataCache', 'REGEXP_FORMATS', '$timeout'];
                    this.SharedDataService = SharedDataService;
                    this.ReportingService = ReportingService;
                    this.SandataCache = SandataCache;
                    this.REGEXP_FORMATS = REGEXP_FORMATS;
                    this.$scope = $scope;
                    this.$timeout = $timeout;

                    //IE Style Fix for Respond.js
                    this.IE = { "ieToDateAndTime": "" };
                    if ($.browser.version < 9.0) {
                        //$scope.IE.ieToDateAndTime = "width:31%";
                        $("div.span4-5").css("width", "31%");
                    }

                    var instantiateNodes = {
                        actions: {},
                        group: "",
                        label: "",
                        modal: true,
                        show: false,
                        value: "",
                        visible: true
                    };
                    this.employeeNode = instantiateNodes;
                    this.clientNode = instantiateNodes;
                    this.supervisorNode = instantiateNodes;
                    this.contractNode = instantiateNodes;
                    this.programNode = instantiateNodes;
                    this.serviceNode = instantiateNodes;
                    this.departmentNode = instantiateNodes;

                    /*this.ReportView = this.ReportingService.getContent();
                    console.log("Report Layout View: ", this.ReportView);
                    
                    for(key in this.ReportView.form.sections){
                    this[key] = this.ReportView.form.sections[key];
                    }*/
                    Sandata.Utility.ReloadCheck();

                    // Init
                    this.selectedParams = {};

                    this.selectedParams["REPORT_PARAMETER_CONTRACT"] = { 'type': 'dropdown', 'data': null, 'value': null, 'param': 'contracts', 'defaultValue': '_A_', 'visible': false };
                    this.selectedParams["REPORT_PARAMETER_CLIENT"] = { 'type': 'dropdown', 'data': null, 'value': null, 'param': 'client_id', 'defaultValue': '', 'visible': false };
                    this.selectedParams["REPORT_PARAMETER_PROGRAM"] = { 'type': 'dropdown', 'data': null, 'value': null, 'param': 'programs', 'defaultValue': '_A_', 'visible': false };
                    this.selectedParams["REPORT_PARAMETER_SERVICE"] = { 'type': 'dropdown', 'data': null, 'value': null, 'param': 'services', 'defaultValue': '_A_', 'visible': false };
                    this.selectedParams["REPORT_PARAMETER_SUPERVISOR"] = { 'type': 'dropdown', 'data': null, 'value': null, 'param': 'spv', 'defaultValue': '', 'visible': false };
                    this.selectedParams["REPORT_PARAMETER_EMPLOYEE"] = { 'type': 'dropdown', 'data': null, 'value': null, 'param': 'employee_id', 'defaultValue': '', 'visible': false };
                    this.selectedParams["REPORT_PARAMETER_DEPARTMENT"] = { 'type': 'dropdown', 'data': null, 'value': null, 'param': 'dept', 'defaultValue': '', 'visible': false };
                    this.selectedParams["REPORT_PARAMETER_CLIENTAR"] = { 'type': 'text', 'data': null, 'value': null, 'param': 'ar_number', 'defaultValue': '', 'visible': false };
                    this.selectedParams["REPORT_DATETIME_FROMDATE"] = { 'type': 'text', 'data': null, 'value': null, 'param': 'from_date', 'defaultValue': '', 'visible': false };
                    this.selectedParams["REPORT_DATETIME_FROMTIME"] = { 'type': 'text', 'data': null, 'value': null, 'param': 'from_time', 'defaultValue': '', 'visible': false };
                    this.selectedParams["REPORT_DATETIME_TODATE"] = { 'type': 'text', 'data': null, 'value': null, 'param': 'to_date', 'defaultValue': '', 'visible': false };
                    this.selectedParams["REPORT_DATETIME_TOTIME"] = { 'type': 'text', 'data': null, 'value': null, 'param': 'to_time', 'defaultValue': '', 'visible': false };
                    this.selectedParams["REPORT_PARAMETER_TASK"] = { 'type': 'text', 'data': null, 'value': null, 'param': 'task', 'defaultValue': '', 'visible': false };

                    this.reportUrl = Sandata.Utility.ReportServiceURL;

                    //Setup Side Panel
                    Sandata.Utility.Setup_ShowHideSidePanel($scope, "sidePanel");

                    this.MessageCenter = {
                        expanded: false,
                        disabled: false,
                        controls: "widgets-area",
                        label: "Click to Show Reporting Messages Center",
                        name: "Show Reporting Messages Center",
                        title: "Show Reporting Messages Center"
                    };

                    var sections = this.ReportingService.getContent().sections;

                    this.title = this.ReportingService.getContent().title;
                    for (var key in sections) {
                        this[key] = sections[key];
                    }

                    this.clients_list = Sandata.Factory.ReportingFactory.clients;
                    this.contracts_list = Sandata.Factory.ReportingFactory.contracts;
                    this.programs_list = Sandata.Factory.ReportingFactory.programs;
                    this.services_list = Sandata.Factory.ReportingFactory.services;
                    this.supervisors_list = Sandata.Factory.ReportingFactory.supervisors;

                    //$scope.employees_list = Sandata.Factory.ReportingFactory.employees;
                    //dmr
                    var accountdata = this.SandataCache.get("accountdata");

                    this.time_format = accountdata.sections.userinfo_section.base_components.MILITARY_TIME_FORMAT;

                    if (this.time_format.display_value === "true") {
                        this.time_regex = this.REGEXP_FORMATS.military_time_regex_pattern;
                        this.time_format_message_from = "Please enter the 'From Time' time in the correct format: hh:mm";
                        this.time_format_message_to = "Please enter the 'To Time' time in the correct format: hh:mm";
                    } else {
                        this.time_regex = this.REGEXP_FORMATS.time_regex_pattern;
                        this.time_format_message_from = "Please enter the 'From Time' time in the correct format: hh:mm AM or PM";
                        this.time_format_message_to = "Please enter the 'To Time' time in the correct format: hh:mm AM or PM";
                    }

                    var employees = [];
                    for (var empIndex in accountdata.sections.accountdata_parameters.base_components.EMPLOYEE_NAMES.option_components) {
                        var employee = accountdata.sections.accountdata_parameters.base_components.EMPLOYEE_NAMES.option_components[empIndex];
                        if (employee.modal == true) {
                            employees.push(employee);
                        }
                    }

                    this.employees_list = employees;

                    this.department_list = accountdata.sections.accountdata_parameters.base_components.DEPARTMENT_NAMES.option_components;

                    //TFS#12874 Add empty option at index zero
                    //$scope.employees_list.splice(0, 0, "");
                    //$scope.clients_list.splice(0, 0, "");
                    this.dateOptions = {
                        open: function () {
                            var dateViewCalendar = this.dateView.calendar;
                            if (dateViewCalendar) {
                                dateViewCalendar.element.width(200);
                            }
                        }
                    };

                    // Initial View
                    this.reporttypeNode = this.report.base_components.REPORT_TYPE.option_components[0];
                    this.watches();
                    this.onReportTypeChange();

                    $scope.Reporting = this;
                }
                //Watch
                Init.prototype.watches = function () {
                    var that = this;
                    this.$scope.$watch("Reporting.fromDateNode", function (value) {
                        if (value != "" && value != null && that.reportnameNode.actions.REPORT_DATETIME_FROMDATE.number_of_days_before_report_can_rendered != null) {
                            that.toDateStart = that.fromDateNode;
                            var d = new Date(value);
                            d.setDate(d.getDate() + that.reportnameNode.actions.REPORT_DATETIME_FROMDATE.number_of_days_before_report_can_rendered);
                            that.toDateEnd = Sandata.Utility.FormatDate({ convertFromRawDate: true, date: d });
                        } else {
                            that.toDateStart = this.fromDateNode;
                            that.toDateEnd = "";
                        }
                    });
                };

                //Hide Reports
                Init.prototype.hideReportSections = function () {
                    // hide sections
                    this.report_datetime.show = false;
                    this.report_parameters.show = false;

                    for (var key in this.report_datetime.base_components) {
                        this.report_datetime.base_components[key].show = false;

                        if (this.report_datetime.base_components[key].disabled != undefined) {
                            this.report_datetime.base_components[key].disabled = false;
                        }
                    }

                    for (var key in this.report_parameters.base_components) {
                        this.report_parameters.base_components[key].show = false;
                    }

                    for (var key in this.selectedParams) {
                        this.selectedParams[key].visible = false;
                    }
                };

                Init.prototype.verifyTimeFields = function (arReults) {
                    var fromTimeMatch = this.fromTimeNode.match(this.time_regex);
                    var toTimeMatch = this.toTimeNode.match(this.time_regex);
                    if (toTimeMatch == null) {
                        arReults.push({ header: "To Time", text: this.time_format_message_to });
                    }
                    if (fromTimeMatch == null) {
                        arReults.push({ header: "From Time", text: this.time_format_message_from });
                    }

                    //this.time_format_message_from = "Please enter the 'From Time' time in the correct format: hh:mm";
                    //this.time_format_message_to = "Please enter the 'To Time' time in the correct format: hh:mm";
                    if (this.fromTimeNode && this.toTimeNode) {
                        var dateTimeStringFrom = this.fromDateNode + " " + this.fromTimeNode;
                        var dateTimeStringTo = this.toDateNode + " " + this.toTimeNode;
                        var timestampFrom = new Date(dateTimeStringFrom);
                        var timestampTo = new Date(dateTimeStringTo);
                        if (timestampFrom > timestampTo) {
                            arReults.push({ header: "Invalid Date/Time Range", text: "Please select a To date and time  range that is greater  then the From date and time range" });
                        }
                    }

                    return arReults;
                };

                //Submit Action
                Init.prototype.submitAction = function (elm) {
                    var validFormEntry = Sandata.Utility.ValidateForm(this.$scope, elm, true);
                    validFormEntry = this.verifyTimeFields(validFormEntry);

                    if (validFormEntry.length == 0) {
                        this.onSubmitEvent();
                    } else {
                        if (this.$scope.messageCenter) {
                            this.$scope.messageCenter.length = 0;
                        } else {
                            this.$scope.messageCenter = [];
                        }
                        if (this.errors) {
                            this.errors.length = 0;
                        } else {
                            this.errors = [];
                        }

                        for (var i = 0, errorLength = validFormEntry.length; i < errorLength; i++) {
                            this.errors.push(validFormEntry[i].header);
                            Sandata.Utility.DisplayMessageCenter({
                                errorCheck: { status: "none" },
                                $timeout: this.$timeout,
                                scope: this.$scope,
                                type: "error",
                                text: validFormEntry[i].text,
                                header: validFormEntry[i].header
                            });
                        }
                    }
                };

                //On Report Type Change
                Init.prototype.onReportTypeChange = function () {
                    this.hideReportSections();

                    var reportStartIndex = 0;
                    for (var i = 0; i < this.report.base_components.REPORT_NAME.option_components.length; i++) {
                        var report = this.report.base_components.REPORT_NAME.option_components[i];
                        if (report.group == this.reporttypeNode.group) {
                            reportStartIndex = i;
                            break;
                        }
                    }

                    this.reportnameNode = this.report.base_components.REPORT_NAME.option_components[reportStartIndex];
                    this.onReportNameChange();
                };

                //On Report Name Change
                Init.prototype.onReportNameChange = function () {
                    var selectedReport = this.reportnameNode;

                    this.hideReportSections();

                    this.fromDateNode = null;
                    this.onFromDateChange(this, false);
                    this.fromTimeNode = null;
                    this.onFromTimeChange(this, false);
                    this.toDateNode = null;
                    this.onToDateChange(this, false);
                    this.toTimeNode = null;
                    this.onToTimeChange(this, false);

                    // Check for report actions
                    var that = this;
                    angular.forEach(selectedReport.actions, function (value, key) {
                        //console.log("key: ",key," value:",value, "section: ",value.section_name, "section value: ", value.show);
                        if (value.show) {
                            // making the parent section visible if child element is shown
                            that[value.section_name]["show"] = true;
                            that[value.section_name].base_components[key].show = value.show;
                            that[value.section_name].base_components[key].disabled = value.disabled;

                            that.setSelectedParams(key, value, false);
                            that.selectedParams[key].visible = value.show;

                            if (key === "REPORT_DATETIME_FROMDATE" && value.value != null) {
                                that.fromDateNode = Sandata.Utility.FormatDate({ date: value.value, timeStampFormat: true });
                            }

                            if (key === "REPORT_DATETIME_FROMTIME" && value.value != null) {
                                //console.log("From Time: ",value.value)
                                if (that.time_format.display_value === "true") {
                                    that.fromTimeNode = value.value;
                                } else {
                                    that.fromTimeNode = Sandata.Utility.FormatTime({ format: "12h", millitarTime: value.value });
                                }
                            } else if (key === "REPORT_DATETIME_FROMTIME" && value.value == null) {
                                that.fromTimeNode = "";
                            } else {
                            }

                            // Date Limitations and Semi Required Fields Flags assigned to From Date
                            if (key === "REPORT_DATETIME_FROMDATE") {
                                //console.log($scope[value.section_name].base_components[key])
                                that[value.section_name].base_components[key]["number_of_days_before_fields_are_required"] = value.number_of_days_before_fields_are_required;
                                that[value.section_name].base_components[key]["number_of_days_before_report_can_rendered"] = value.number_of_days_before_report_can_rendered;
                                //console.log($scope[value.section_name].base_components[key])
                            }

                            //TEMPORARY -Manually Set Required Values for From Date and Time until Services updates flags for required for all fields -- ac
                            /* if(  key === "REPORT_DATETIME_FROMDATE" ||  key === "REPORT_DATETIME_FROMTIME" || key === "REPORT_DATETIME_TOTIME" ){
                            that[value.section_name].base_components[key].required = true;
                            }else{
                            that[value.section_name].base_components[key].required = false;
                            }*/
                            if (key === "REPORT_DATETIME_TODATE" && value.value != null) {
                                that.toDateNode = Sandata.Utility.FormatDate({ date: value.value, timeStampFormat: true });
                            }
                            if (key === "REPORT_DATETIME_TOTIME" && value.value != null) {
                                // console.log("To Time: ",value.value)
                                //console.log("TO Time Formatted",Sandata.Utility.FormatTime({format:"12h",millitarTime:value.value}))
                                if (that.time_format.display_value === "true") {
                                    that.toTimeNode = value.value;
                                } else {
                                    that.toTimeNode = Sandata.Utility.FormatTime({ format: "12h", millitarTime: value.value });
                                }
                            } else if (key === "REPORT_DATETIME_TOTIME" && value.value == null) {
                                that.toTimeNode = "";
                            } else {
                            }
                        }
                    });

                    this.onFromDateChange(this, false);
                    this.onFromTimeChange(this, false);
                    this.onToDateChange(this, false);
                    this.onToTimeChange(this, false);
                };

                //Check User Date Rangers for Required Fields
                Init.prototype.checkUsersDateRangesForRequiredFields = function () {
                    var required = false;
                    if (this.report_datetime.base_components.REPORT_DATETIME_FROMDATE.number_of_days_before_fields_are_required && this.report_datetime.base_components.REPORT_DATETIME_FROMDATE.number_of_days_before_fields_are_required != null) {
                        var userDateRangeSelection = Sandata.Utility.DifferenceBetweenDates({ from: this.fromDateNode, to: this.toDateNode, returnType: "days" });
                        if (userDateRangeSelection.difference >= 0) {
                            if (userDateRangeSelection.status == true && userDateRangeSelection.difference > this.report_datetime.base_components.REPORT_DATETIME_FROMDATE.number_of_days_before_fields_are_required) {
                                required = true;
                            }
                        }
                    }

                    //console.log("checkUsersDateRanges:", required);
                    return required;
                };

                //Check User Date Ranges
                Init.prototype.checkUsersDateRanges = function () {
                    var obj = { isNegative: false, isGreater: false };
                    if (this.report_datetime.base_components.REPORT_DATETIME_FROMDATE.number_of_days_before_report_can_rendered && this.report_datetime.base_components.REPORT_DATETIME_FROMDATE.number_of_days_before_report_can_rendered != null) {
                        var userDateRangeSelection = Sandata.Utility.DifferenceBetweenDates({ from: this.fromDateNode, to: this.toDateNode, returnType: "days" });
                        if (userDateRangeSelection.status == true && userDateRangeSelection.difference < 0) {
                            obj.isNegative = true;
                        }
                        if (userDateRangeSelection.status == true && userDateRangeSelection.difference > this.report_datetime.base_components.REPORT_DATETIME_FROMDATE.number_of_days_before_report_can_rendered) {
                            obj.isGreater = true;
                        }
                    }

                    //console.log("checkUsersDateRanges:", required);
                    return obj;
                };

                //Check Required Fields
                Init.prototype.checkRequiredFields = function () {
                    var returnObj = { criteriaSatisfied: false, toTimeSatisfied: false, negativeDays: false, labels: Array() };
                    if (this.checkUsersDateRangesForRequiredFields() == true || this.checkUsersDateRanges().isNegative == true || this.checkUsersDateRanges().isGreater == true) {
                        //Check Required Fields
                        if (this.checkUsersDateRangesForRequiredFields() == true) {
                            //console.log("Did check date ranges return true ");
                            var requiredFields = [];

                            if (this.report_parameters.base_components.REPORT_PARAMETER_EMPLOYEE.show && !this.report_parameters.base_components.REPORT_PARAMETER_EMPLOYEE.disabled) {
                                var employee, client;
                                if (typeof this.employeeNode.value == 'undefined') {
                                    employee = this.employeeNode;
                                } else {
                                    employee = this.employeeNode.value;
                                }
                                if (typeof this.clientNode.value == 'undefined') {
                                    client = this.clientNode;
                                } else {
                                    client = this.clientNode.value;
                                }

                                requiredFields.push({
                                    label: this.report_parameters.base_components.REPORT_PARAMETER_EMPLOYEE.select_label,
                                    value: employee
                                });
                            }

                            if (this.report_parameters.base_components.REPORT_PARAMETER_CLIENT.show && !this.report_parameters.base_components.REPORT_PARAMETER_CLIENT.disabled) {
                                requiredFields.push({
                                    label: this.report_parameters.base_components.REPORT_PARAMETER_CLIENT.select_label,
                                    value: client
                                });
                            }

                            if (this.report_parameters.base_components.REPORT_PARAMETER_SUPERVISOR.show && !this.report_parameters.base_components.REPORT_PARAMETER_SUPERVISOR.disabled) {
                                requiredFields.push({
                                    label: this.report_parameters.base_components.REPORT_PARAMETER_SUPERVISOR.select_label,
                                    value: this.supervisorNode.value
                                });
                            }
                            if (this.report_parameters.base_components.REPORT_PARAMETER_DEPARTMENT.show && !this.report_parameters.base_components.REPORT_PARAMETER_DEPARTMENT.disabled) {
                                requiredFields.push({
                                    label: this.report_parameters.base_components.REPORT_PARAMETER_DEPARTMENT.select_label,
                                    value: this.departmentNode.value
                                });
                            }

                            for (var i = 0; i < requiredFields.length; i++) {
                                //console.log("Whats the Value",requiredFields[i].value )
                                //console.log("Required Fields: ", requiredFields[i].label, " : ", requiredFields[i].value);
                                if (requiredFields[i].value != null && requiredFields[i].value != "" && typeof (requiredFields[i].value) != 'undefined') {
                                    returnObj.criteriaSatisfied = true;
                                }
                                returnObj.labels.push(requiredFields[i].label);
                            }
                        }

                        // Check Max Date Range
                        if (this.checkUsersDateRanges().isGreater == true) {
                            returnObj.toTimeSatisfied = false;
                        } else {
                            returnObj.toTimeSatisfied = true;
                        }

                        //Is Negative Amount of Days
                        if (this.checkUsersDateRanges().isNegative == true) {
                            returnObj.negativeDays = false;
                        } else {
                            returnObj.negativeDays = true;
                        }
                    } else {
                        returnObj.criteriaSatisfied = true;
                        returnObj.toTimeSatisfied = true;
                        returnObj.negativeDays = true;
                    }

                    return returnObj;
                };

                //On Submit Event
                Init.prototype.onSubmitEvent = function () {
                    var checkFields = this.checkRequiredFields();
                    var strMessage = "";
                    this.$scope.messageCenter = [];
                    if (checkFields.criteriaSatisfied == false || checkFields.toTimeSatisfied == false || checkFields.negativeDays == false) {
                        if (!checkFields.criteriaSatisfied && checkFields.labels.length > 0) {
                            //console.log("Check Fields: ",checkFields.labels);
                            strMessage = strMessage + "Please select one of the following fields: ";
                            for (var i = 0; i < checkFields.labels.length; i++) {
                                if (i < checkFields.labels.length && i != 0) {
                                    strMessage = strMessage + ", ";
                                }
                                strMessage = strMessage + "" + checkFields.labels[i] + "";
                            }

                            Sandata.Utility.DisplayMessageCenter({
                                $timeout: this.$timeout,
                                errorCheck: { status: "SUCCESS" },
                                scope: this.$scope,
                                type: "error",
                                text: strMessage,
                                header: "Select a field"
                            });
                        }
                        if (!checkFields.toTimeSatisfied) {
                            strMessage = "";
                            strMessage = strMessage + "Please select a To Date that is within " + this.report_datetime.base_components.REPORT_DATETIME_FROMDATE.number_of_days_before_report_can_rendered + " days from " + this.fromDateNode;
                            Sandata.Utility.DisplayMessageCenter({
                                $timeout: this.$timeout,
                                errorCheck: { status: "SUCCESS" },
                                scope: this.$scope,
                                type: "error",
                                text: strMessage,
                                header: "To Date"
                            });
                        }
                        if (!checkFields.negativeDays) {
                            strMessage = "";
                            strMessage = strMessage + "Please select a To Date that is greater than the From Date: " + this.fromDateNode;
                            Sandata.Utility.DisplayMessageCenter({
                                $timeout: this.$timeout,
                                errorCheck: { status: "SUCCESS" },
                                scope: this.$scope,
                                type: "error",
                                text: strMessage,
                                header: "To Date"
                            });
                        }
                        // this.$scope.$apply();
                    } else {
                        var sessionId = this.SandataCache.get("credentials").soid;
                        var username = this.SandataCache.get("credentials").su;
                        var password = this.SandataCache.get("credentials").sp;

                        //console.log('SandataCache: ', SandataCache.get("credentials"));
                        var params = this.getParamUrl(sessionId, username, password);

                        //console.log("params: ", params);
                        var url = this.reportUrl + params;

                        //console.log("url: ", url);
                        //
                        Sandata.Utility.ShowPageLoaderGraphic();

                        var that = this;
                        this.ReportingService.getReportToken().then(function (response) {
                            if (response.status == "SUCCESS") {
                                url += '&tk=' + encodeURIComponent(response.data);

                                //console.log(url);
                                window.open(url);
                            } else {
                                Sandata.Utility.DisplayMessageCenter({
                                    $timeout: that.$timeout,
                                    errorCheck: response,
                                    scope: that.$scope,
                                    type: "",
                                    text: response.message_detail,
                                    header: response.message_summary
                                });
                            }
                        });
                    }
                };

                /**
                * Handle Dropdown Controls
                */
                //Select Contract Change
                Init.prototype.selectContractChange = function (control) {
                    //console.log("Contracts Change: ",this.contractNode );
                    if (this.contractNode != null) {
                        this.setSelectedParams("REPORT_PARAMETER_CONTRACT", this.contractNode);
                    } else {
                        this.setSelectedParams("REPORT_PARAMETER_CONTRACT", null);
                    }
                };

                //Select Client Change
                Init.prototype.selectClientChange = function () {
                    if (this.clientNode != null) {
                        if (typeof this.clientNode.value != 'undefined') {
                            this.setSelectedParams("REPORT_PARAMETER_CLIENT", this.clientNode);
                        } else {
                            var clientModel;
                            for (var i = 0, clientsLength = this.clients_list.length; i < clientsLength; i++) {
                                if (this.clients_list[i].value == this.clientNode) {
                                    clientModel = this.clients_list[i];
                                    break;
                                }
                            }
                            this.setSelectedParams("REPORT_PARAMETER_CLIENT", clientModel);
                        }
                    } else {
                        this.setSelectedParams("REPORT_PARAMETER_CLIENT", null);
                    }
                };

                //Select Program Change
                Init.prototype.selectProgramChange = function (control) {
                    if (this.programNode != null) {
                        this.setSelectedParams("REPORT_PARAMETER_PROGRAM", this.programNode);
                    } else {
                        this.setSelectedParams("REPORT_PARAMETER_PROGRAM", null);
                    }
                };

                //Select Service Change
                Init.prototype.selectServiceChange = function (control) {
                    if (this.serviceNode != null) {
                        this.setSelectedParams("REPORT_PARAMETER_SERVICE", this.serviceNode);
                    } else {
                        this.setSelectedParams("REPORT_PARAMETER_SERVICE", null);
                    }
                };

                //Select Supervisor Change
                Init.prototype.selectSupervisorChange = function (control) {
                    if (this.supervisorNode != null) {
                        this.setSelectedParams("REPORT_PARAMETER_SUPERVISOR", this.supervisorNode);
                    } else {
                        this.setSelectedParams("REPORT_PARAMETER_SUPERVISOR", null);
                    }
                };

                //Select Department Change
                Init.prototype.selectDepartmentChange = function (control) {
                    if (this.departmentNode != null) {
                        this.setSelectedParams("REPORT_PARAMETER_DEPARTMENT", this.departmentNode);
                    } else {
                        this.setSelectedParams("REPORT_PARAMETER_DEPARTMENT", null);
                    }
                };

                //Select Employee Change
                Init.prototype.selectEmployeeChange = function () {
                    if (this.employeeNode != null) {
                        if (typeof this.employeeNode.value != 'undefined') {
                            this.setSelectedParams("REPORT_PARAMETER_EMPLOYEE", this.employeeNode);
                        } else {
                            var employeeModel;
                            for (var i = 0, employeeLength = this.employees_list.length; i < employeeLength; i++) {
                                if (this.employees_list[i].value == this.employeeNode) {
                                    employeeModel = this.employees_list[i];
                                    break;
                                }
                            }
                            this.setSelectedParams("REPORT_PARAMETER_EMPLOYEE", employeeModel);
                        }
                    } else {
                        this.setSelectedParams("REPORT_PARAMETER_EMPLOYEE", null);
                    }
                };

                /**
                * Handle Text Field Controls
                */
                //On Change AR Change
                Init.prototype.onClientArChange = function (control) {
                    if (this.clientArNode != null) {
                        this.setSelectedParams("REPORT_PARAMETER_CLIENTAR", { "group": this.clientArNode });
                    } else {
                        this.setSelectedParams("REPORT_PARAMETER_CLIENTAR", null);
                    }
                };

                //On Task Change
                Init.prototype.onTaskChange = function (control) {
                    if (this.taskNode != null) {
                        this.setSelectedParams("REPORT_PARAMETER_TASK", { "group": this.taskNode });
                    } else {
                        this.setSelectedParams("REPORT_PARAMETER_TASK", null);
                    }
                };

                //On From Date Change
                Init.prototype.onFromDateChange = function (control, validate) {
                    if (this.fromDateNode != null) {
                        this.setSelectedParams("REPORT_DATETIME_FROMDATE", { "group": this.getFormattedDate(this.fromDateNode) }, validate);
                    } else {
                        this.setSelectedParams("REPORT_DATETIME_FROMDATE", null, validate);
                    }

                    if (this.reporttypeNode.group == "DAILY") {
                        if (this.toDateNode == null) {
                            this.toDateNode = (this.fromDateNode != null) ? this.fromDateNode : "";
                        } else {
                            this.toDateNode = this.fromDateNode;
                        }

                        $("#todate_control_id").val((this.fromDateNode != null) ? this.fromDateNode : "");

                        this.onToDateChange(this, validate);
                    }
                };

                //On From Time Change
                Init.prototype.onFromTimeChange = function (control, validate) {
                    // console.log(this.fromTimeNode)
                    if (this.fromTimeNode != null) {
                        this.setSelectedParams("REPORT_DATETIME_FROMTIME", { "group": this.getFormattedTime(this.fromTimeNode) }, validate);
                        //console.log("From Time:: ",this.fromTimeNode);
                    } else {
                        this.setSelectedParams("REPORT_DATETIME_FROMTIME", null, validate);
                    }
                };

                //On To Date Change
                Init.prototype.onToDateChange = function (control, validate) {
                    if (this.toDateNode != null) {
                        this.setSelectedParams("REPORT_DATETIME_TODATE", { "group": this.getFormattedDate(this.toDateNode) }, validate);
                    } else {
                        this.setSelectedParams("REPORT_DATETIME_TODATE", null, validate);
                    }
                };

                //On To Time Change
                Init.prototype.onToTimeChange = function (control, validate) {
                    if (this.toTimeNode != null) {
                        this.setSelectedParams("REPORT_DATETIME_TOTIME", { "group": this.getFormattedTime(this.toTimeNode) }, validate);
                    } else {
                        this.setSelectedParams("REPORT_DATETIME_TOTIME", null, validate);
                    }
                };

                /**
                * Set Selected Parameters
                *
                * @param key   Unique key value into the Hashmap
                * @param type  Type of control
                * @param data  Control data object
                */
                //Set Selected Parameters
                Init.prototype.setSelectedParams = function (key, data, validate) {
                    if (this.selectedParams[key] != undefined) {
                        if (data != null) {
                            this.selectedParams[key].data = data;
                            if (data.group) {
                                this.selectedParams[key].value = data.group;
                            } else if (data.value) {
                                this.selectedParams[key].value = data.value;
                            } else {
                            }
                        } else {
                            this.selectedParams[key].data = null;
                            this.selectedParams[key].value = null;
                        }

                        if (validate == undefined || validate == true)
                            Sandata.Utility.ValidateForm(this.$scope, '#reporting_form');
                    } else {
                        //console.log("ERROR: Sandata.Controller: setSelectParams: key is undefined!");
                    }
                };

                //Get Selected Parameter Value
                Init.prototype.getSelectedParamValue = function (param) {
                    if (param != undefined) {
                        if (param.value == null)
                            return param.defaultValue;

                        return param.value;
                    }

                    return null;
                };

                /**
                * Generate the SSRS URL
                */
                //Get Parameter URL
                Init.prototype.getParamUrl = function (sessionId, username, password) {
                    //console.log("reportnameNode: ", $scope.reportnameNode);
                    var paramUrl = "?" + "sid=" + encodeURIComponent(sessionId) + "&" + "report_name=" + encodeURIComponent(this.reportnameNode.value);

                    for (var key in this.selectedParams) {
                        if (this.selectedParams[key].visible == true) {
                            paramUrl += "&" + this.selectedParams[key].param + "=" + encodeURIComponent(this.getSelectedParamValue(this.selectedParams[key]));
                        }
                    }

                    return paramUrl + "&su=" + encodeURIComponent(username) + "&sp=" + encodeURIComponent(password);
                };

                /**
                * Helper Methods
                */
                //Find Parameter
                Init.prototype.findParam = function (paramName) {
                    for (var key in this.reportnameNode.actions) {
                        var actionItem = this.reportnameNode.actions[key];

                        if (actionItem.show.component.indexOf(paramName) != -1) {
                            return true;
                        }
                    }

                    return false;
                };

                /**
                * @return: Formatted date string: MM_dd_yyyy (page 3 Running an SSRS Report via Santrax.docx)
                */
                //Get Formatted Date
                Init.prototype.getFormattedDate = function (date) {
                    if (date != undefined) {
                        var match = /\//gi;
                        return date.replace(match, "_");
                    }
                    return null;
                };

                /**
                * @return: Formatted time string: HH_MM_SS (military) (page 3 Running an SSRS Report via Santrax.docx)
                */
                //Get Formatted Time
                Init.prototype.getFormattedTime = function (time) {
                    var timeFormat = new RegExp(this.time_regex);

                    if (time != undefined && timeFormat.exec(time) && time.match(/am|pm/gi)) {
                        // handle error
                        /* if(time.length == 0) {
                        //console.log("WARN: Sandata.Controller: getFormattedTime: time.length == 0");
                        return "";
                        }
                        
                        // split the time from the colon i.e. HH:MM AM --> [HH],[MM AM]
                        var part1 = time.split(':');
                        
                        // handle error
                        if(part1.length != 2) {
                        //console.log("ERROR: Sandata.Controller: getFormattedTime: part1.length != 2");
                        return "!Invalid!";
                        }
                        
                        var hours = part1[0];
                        
                        // split the second part by the space i.e. MM AM --> [MM],[AM]
                        var part2 = part1[1].split(' ');
                        
                        // handle error
                        if(part2.length != 2) {
                        //console.log("ERROR: Sandata.Controller: getFormattedTime: part12.length != 2");
                        return "!Invalid!";
                        }
                        
                        var minutes = part2[0];
                        var ampm = part2[1];
                        */
                        var ampm = timeFormat.exec(time)[4];
                        var hours = timeFormat.exec(time)[1];
                        var minutes = timeFormat.exec(time)[2];
                        var mil = this.convertToMilitaryTime(ampm, hours, minutes);

                        var seconds = '00';
                        if (mil.Hours == '23' && mil.Minutes == '59') {
                            seconds = '59';
                        }

                        return mil.Hours + '_' + mil.Minutes + '_' + seconds;
                    } else if (time != undefined && timeFormat.exec(time)) {
                        var hours = timeFormat.exec(time)[1];
                        var minutes = timeFormat.exec(time)[2];

                        var seconds = '00';
                        if (hours == '23' && minutes == '59') {
                            seconds = '59';
                        }

                        return hours + '_' + minutes + '_' + seconds;
                    } else {
                        //console.log("ERROR: Sandata.Controller: getFormattedTime: time == undefined");
                        return "!Invalid!";
                    }
                };

                //Convert To Military Time
                Init.prototype.convertToMilitaryTime = function (ampm, hours, minutes) {
                    var militaryHours;

                    ampm = ampm.toLowerCase();

                    if (ampm == "am") {
                        militaryHours = hours;

                        // check for special case: midnight
                        if (militaryHours == "12") {
                            militaryHours = "00";
                        }
                    } else {
                        if (ampm == "pm" || am == "p.m.") {
                            // get the interger value of hours, then add
                            tempHours = parseInt(hours) + 2;

                            // adding the numbers as strings converts to strings
                            if (tempHours < 10)
                                tempHours = "1" + tempHours;
                            else
                                tempHours = "2" + (tempHours - 10);

                            // check for special case: noon
                            if (tempHours == "24") {
                                tempHours = "12";
                            }
                            militaryHours = tempHours;
                        }
                    }

                    return { 'Hours': militaryHours, 'Minutes': minutes };
                };
                return Init;
            })();
            Reporting.Init = Init;
        })(Controller.Reporting || (Controller.Reporting = {}));
        var Reporting = Controller.Reporting;
    })(Sandata.Controller || (Sandata.Controller = {}));
    var Controller = Sandata.Controller;
})(Sandata || (Sandata = {}));
var Sandata;
(function (Sandata) {
    (function (Controller) {
        /// <reference path="../services/Sandata.Service.Dashboard.ts" />
        (function (Dashboard) {
            /**
            * Dashboard
            */
            var Init = (function () {
                function Init($scope, $location, $timeout, SharedDataService, SandataCache, DashboardService) {
                    this.$inject = ['$scope', '$location', '$timeout', 'SharedDataService', 'SandataCache', 'DashboardService'];
                    this.initializeClients = function () {
                        var clients = this.DashboardService.localLayout.clients;

                        // console.log(this.SharedDataService.getClient())
                        // If Client already selected in another page, then set select field to that value
                        if (this.SharedDataService.getClient() != null) {
                            if (typeof this.SharedDataService.getClient().value != "undefined") {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: clients,
                                    arObj: "option_components",
                                    variable: "clients",
                                    model: "clientModel",
                                    index: this.SharedDataService.getClient().value,
                                    byvalue: true });
                            } else {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: clients,
                                    arObj: "option_components",
                                    variable: "clients",
                                    model: "clientModel",
                                    index: this.SharedDataService.getClient(),
                                    byvalue: true });
                            }
                        } else {
                            Sandata.Utility.AssignToScopeAndSetModel({
                                $scope: this,
                                data: clients,
                                arObj: "option_components",
                                variable: "clients",
                                model: "clientModel",
                                index: 0 });
                        }
                    };
                    this.DashboardService = DashboardService;
                    this.SandataCache = SandataCache;
                    this.SharedDataService = SharedDataService;
                    this.$timeout = $timeout;
                    this.$location = $location;

                    this.$scope = $scope;

                    this.dashboardInitialized = false;

                    Sandata.Utility.ReloadCheck();

                    // Setup Side Panel
                    Sandata.Utility.Setup_ShowHideSidePanel($scope, "sidePanel");

                    this.MessageCenter = {
                        expanded: false,
                        disabled: false,
                        controls: "widgets-area",
                        label: "Click to Show Dashboard Messages Center",
                        name: "Show Dashboard Messages Center",
                        title: "Show Dashboard Messages Center"
                    };

                    // Retrieve account data and poyor list from the cache.
                    var accountdata = DashboardService.localLayout.dashboard_data;
                    var payor = DashboardService.localLayout.payors;
                    this.time_format = DashboardService.localLayout.time_format;

                    this.title = "";

                    /*this.clients = DashboardService.localLayout.clients;
                    //Check if There is a selected client
                    if(this.SharedDataService.getClient() != null){
                    this.clientModel =  this.SharedDataService.getClient() ;
                    }else{
                    this.clientModel =  this.clients.option_components[0];
                    }*/
                    this.initializeClients();

                    //console.log("Are there reasons from services : " , DashboardService.localLayout.reasons_codes);
                    this.reasons_codes = DashboardService.localLayout.reasons_codes;

                    //console.log("Are the reasons assigned to scope : " , $scope.reasons_codes);
                    this.reasonModelSelect = this.reasons_codes.option_components[0];

                    //console.log("Has the Model Been Created : " , $scope.reasonModelSelect);
                    this.payors = DashboardService.localLayout.payors.base_components.PAYOR_NAMES;

                    //Check if There is a selected payor
                    if (this.SharedDataService.getPayor() != null) {
                        this.payorModel = this.SharedDataService.getPayor();
                    } else {
                        // console.log("Initialize:",this.payors)
                        this.payorModel = this.payors.option_components[0];
                    }

                    this.employees = DashboardService.localLayout.employees;
                    var employeeList = this.employees.option_components;
                    var allEmployeesValue = "-1";
                    for (var i = 0; i < employeeList.length; i++) {
                        //TFS#13383: Default to first employee
                        this.valueForFirstEmployeeVisitNeedApproval = employeeList[0].value;
                        this.valueForFirstEmployeeVisitNeedAttenton = "-1"; // Reverting back to All employees for bug 14976

                        if (employeeList[i].label.toLowerCase() == "all employees") {
                            allEmployeesValue = employeeList[i].value;
                            break;
                        }
                    }

                    $scope.valueForAllEmployees = allEmployeesValue;

                    /***********************************
                    Assign Time Format Utility to Scope
                    **********************************/
                    this.formatTime = Sandata.Utility.FormatTime;

                    this.accountinfo = accountdata.sections.accountinfo_section.base_components;

                    this.visitHistoryFromDate = Sandata.Utility.FormatDate({ date: this.accountinfo.VISIT_HISTORY_START_DATE.display_value, url_encode: true, timeStampFormat: true });
                    this.visitHistoryToDate = Sandata.Utility.FormatDate({ date: this.accountinfo.VISIT_HISTORY_END_DATE.display_value, url_encode: true, timeStampFormat: true });
                    this.authHistoryFromDate = Sandata.Utility.FormatDate({ date: this.accountinfo.VISIT_HISTORY_START_DATE.display_value, url_encode: true, timeStampFormat: true });
                    this.authHistoryToDate = Sandata.Utility.FormatDate({ date: this.accountinfo.VISIT_HISTORY_END_DATE.display_value, url_encode: true, timeStampFormat: true });

                    // Function used to update the Unknown Calls table
                    //Function Called to load in Dashboard Data and initalize Pagination
                    /****Setup Pagination****/
                    //Setting Up Total Items Count
                    this.totalItems = 0;

                    //Get Current Page
                    this.currentPage = 1;

                    //For Number of Pages
                    this.numberOfPages = 0;

                    //Max size of pagination numbers
                    this.maxSize = 10;

                    //How Many items per page
                    this.itemsPerPage = 5;

                    // Watch Current Page in Pagination Directive and apply function accordingly
                    this.setWatchers($scope);

                    //this.getDashboardData($scope,DashboardService);
                    //$scope.updateDashboard();
                    this.selectedClient = this.clients[0];
                    this.assignUnknownCallModal = {};
                    this.assignUnknownCallModal.id = "";
                    this.assignUnknownCallModal.updateId = "";
                    this.assignUnknownCallModal.clientId = "";
                    this.assignUnknownCallModal.reasonCode = "";
                    this.dashboardConfirmFocus = false;

                    //this.loadOptions();
                    this.getPayor();

                    //We are assigning 'this' in controller.js
                    // $scope.Dashboard = this;
                    $scope.Dashboard = this;
                }
                Init.prototype.setWatchers = function ($scope) {
                    var that = this;
                    $scope.$watch('Dashboard.currentPage', function () {
                        if (that.currentPage && that.SandataCache.get("dashboardMetadata")) {
                            var metadata = that.SandataCache.get("dashboardMetadata");
                            var unknownCallArray = metadata.sections.unknowncalls_section.base_components.UNKNOWNCALLS_GRID.grid_row_components;

                            that.updateUnknownCallsGrid({
                                gridData: "currentlyShownUnknownCalls",
                                gridSource: unknownCallArray,
                                currentPage: that.currentPage,
                                itemsPerPage: that.itemsPerPage
                            });
                        }
                    });
                };

                Init.prototype.sortUnknownCallsByDate = function () {
                    Sandata.Utility.ShowPageLoaderGraphic();
                    if (typeof this.unknownCallsSortDirection == 'undefined') {
                        this.unknownCallsSortDirection = false;
                    } else {
                        this.unknownCallsSortDirection = !this.unknownCallsSortDirection;
                    }

                    var that = this;
                    this.unknowncalls_section.base_components.UNKNOWNCALLS_GRID.grid_row_components.sort(function (a, b) {
                        if (that.unknownCallsSortDirection) {
                            if (new Date(a.components.date.text) > new Date(b.components.date.text)) {
                                return 1;
                            } else if (new Date(a.components.date.text) < new Date(b.components.date.text)) {
                                return -1;
                            } else {
                                return 0;
                            }
                        } else {
                            if (new Date(a.components.date.text) < new Date(b.components.date.text)) {
                                return 1;
                            } else if (new Date(a.components.date.text) > new Date(b.components.date.text)) {
                                return -1;
                            } else {
                                return 0;
                            }
                        }
                    });

                    this.updateUnknownCallsGrid({
                        gridData: "currentlyShownUnknownCalls",
                        gridSource: this.unknowncalls_section.base_components.UNKNOWNCALLS_GRID.grid_row_components,
                        currentPage: this.currentPage,
                        itemsPerPage: this.itemsPerPage,
                        recalculateNumberOfPages: true,
                        numberOfItems: "numberOfItems",
                        numberOfPages: "numberOfPages"
                    });
                    this.$timeout(function () {
                        Sandata.Utility.HidePageLoaderGraphic();
                    }, 0);
                };

                Init.prototype.updateDashboard = function () {
                    if (typeof this.payorModel == 'undefined') {
                        this.payorModel = {
                            "value": "",
                            "label": "",
                            "group": "",
                            "show": false,
                            "modal": false,
                            "disabled": false,
                            "actions": {}
                        };
                        // this.payorModel.value = "";
                    }

                    //this.updateDashboard($scope,DashboardService);
                    var that = this;
                    this.DashboardService.updateDashboardLayout(this.payorModel.value).then(function (data) {
                        //Sandata.Utility.AssignToScope($scope,data.dashboard);console.log("mock metadata", data.dashboard);
                        Sandata.Utility.AssignToScope(that, metadata.sections);
                    });

                    // Remove table row details
                    $(".auth-detail-row").remove();

                    // Show unknown calls
                    $('#unknown-calls').show();
                };

                Init.prototype.updateGrid = function (page) {
                    if (page) {
                        this.currentPage = page;
                    } else {
                        this.currentPage = 1;
                    }

                    var accountdata = this.SandataCache.get("accountdata");
                    var bc = accountdata.sections.accountinfo_section.base_components;
                    var visitsStartDate = bc.VISIT_HISTORY_START_DATE.display_value;
                    var visitsEndDate = bc.VISIT_HISTORY_END_DATE.display_value;
                    var authorizationStartDate = bc.AUTHORIZATION_HISTORY_START_DATE.display_value;
                    var authorizationEndDate = bc.AUTHORIZATION_HISTORY_END_DATE.display_value;

                    if (typeof this.clientModel.value == 'undefined' && typeof this.clientModel != 'undefined') {
                        var clientId = this.clientModel;
                    } else {
                        var clientId = this.clientModel.value;
                    }

                    if (typeof this.payorModel.value == 'undefined' && typeof this.payorModel != 'undefined') {
                        var payor = this.payorModel;
                    } else {
                        var payor = this.payorModel.value;
                    }

                    var that = this;
                    this.SharedDataService.setClient(this.clientModel);
                    this.SharedDataService.setPayor(this.payorModel);
                    this.DashboardService.getMetadata(visitsStartDate, visitsEndDate, authorizationStartDate, authorizationEndDate, clientId, payor).then(function (response) {
                        if (response.status == "SUCCESS") {
                            if (!that.title) {
                                that.title = response.data.title;
                            }

                            $("tr.auth-detail-row").remove();
                            var sections = response.data.sections;

                            var metadata = that.SandataCache.get("dashboardMetadata");
                            var unknownCallArray = metadata.sections.unknowncalls_section.base_components.UNKNOWNCALLS_GRID.grid_row_components;

                            that.updateUnknownCallsGrid({
                                gridData: "currentlyShownUnknownCalls",
                                gridSource: unknownCallArray,
                                currentPage: that.currentPage,
                                itemsPerPage: that.itemsPerPage,
                                recalculateNumberOfPages: true,
                                numberOfItems: "numberOfItems",
                                numberOfPages: "numberOfPages"
                            });

                            Sandata.Utility.AssignToScope(that, sections);

                            if (!that.dashboardInitialized) {
                                Sandata.Utility.DashboardPostInitialize();
                                that.dashboardInitialized = false;
                            }
                            Sandata.Utility.AssignObjectsInArrayToScopeObject(that, metadata.sections.unknowncalls_section.base_components.UNKNOWNCALLS_GRID.grid_row_header.components, "name", "unknown_calls_header", true);
                        } else {
                            $("tr.auth-detail-row").remove();
                            var sections = {
                                authorization_section: {},
                                chart: {},
                                unknowncalls_section: {},
                                title: ""
                            };
                            Sandata.Utility.AssignToScope(that, sections);
                            Sandata.Utility.DisplayMessageCenter({
                                errorCheck: response,
                                $timeout: that.$timeout,
                                scope: that.$scope,
                                type: "",
                                text: response.message_detail,
                                header: response.message_summary
                            });
                        }
                    });
                };

                Init.prototype.getPayor = function () {
                    var that = this;
                    this.DashboardService.getPayor(this.clientModel.value).then(function (response) {
                        that.payors = response.data.base_components.PAYOR_NAMES;

                        //console.log("Get Payors Method:", that.payors)
                        if (that.payors.option_components.length != 0) {
                            that.payorModel = that.payors.option_components[0];
                            that.updateGrid();
                        }
                    });
                };

                Init.prototype.updateUnknownCallsGrid = function (obj) {
                    var beginIndex = (obj.currentPage - 1) * obj.itemsPerPage;
                    var endIndex = beginIndex + (obj.itemsPerPage);

                    this.$scope[obj.gridData] = obj.gridSource.slice(beginIndex, endIndex);

                    for (var i = 0; i < this.$scope[obj.gridData].length; i++) {
                        var myUnknownCall = this.$scope[obj.gridData][i];
                        var clientIdForUnknownCall = myUnknownCall.components.clientId.text;
                        for (var j = 0; j < this.clients.option_components.length; j++) {
                            if (clientIdForUnknownCall == this.clients.option_components[j].value) {
                                myUnknownCall.selectedClient = this.clients.option_components[j];
                                break;
                            }
                        }
                    }
                    if (obj.recalculateNumberOfPages) {
                        //Finalize Pagination
                        this[obj.numberOfItems] = obj.gridSource.length;
                        this[obj.numberOfPages] = Math.ceil((obj.gridSource.length / obj.itemsPerPage));
                    }
                };

                Init.prototype.openConfirmModal = function (selectedRow, id, updateId, client, formid) {
                    //console.log("Whats in the Selected Form : ", formid);
                    var validFormEntry = Sandata.Utility.ValidateForm(this.$scope, formid, true);
                    if (selectedRow.row.reasonModelSelect && validFormEntry == 0) {
                        if (this.$scope.messageCenter && this.$scope.messageCenter.filter) {
                            this.$scope.messageCenter = this.$scope.messageCenter.filter(function (el) {
                                return el.header !== "Select Reason Code";
                            });
                        } else {
                            this.$scope.messageCenter = [];
                        }
                        var clientId, clientName, reasonCode;

                        if (typeof client.value == 'undefined' && typeof client != 'undefined') {
                            jQuery.grep(this.clients.option_components, function (passesObj, index) {
                                if (passesObj.value == client) {
                                    client = passesObj;
                                }
                            });
                        }

                        clientId = client.value;
                        clientName = client.label;
                        if (selectedRow.row.reasonModelSelect.value) {
                            reasonCode = selectedRow.row.reasonModelSelect.value;
                        } else {
                            reasonCode = selectedRow.row.reasonModelSelect;
                        }

                        this.selectedUnknownCall = selectedRow;
                        this.dashboardConfirmModal = { "show": true, "clientName": clientName };
                        this.dashboardConfirmModal.opts = {
                            backdropFade: false,
                            dialogFade: false,
                            dialogClass: "modal",
                            backdropClick: false
                        };
                        this.dashboardConfirmFocus = true;

                        //$scope.assignUnknownCallModal.show = true;
                        this.assignUnknownCallModal.id = id;
                        this.assignUnknownCallModal.updateId = updateId;
                        this.assignUnknownCallModal.clientId = clientId;
                        this.assignUnknownCallModal.reasonCode = reasonCode;
                    } else {
                        //console.log(validFormEntry)
                        if (typeof this.$scope.messageCenter == 'undefined') {
                            this.$scope.messageCenter = [];
                        }
                        if (typeof this.errors == 'undefined') {
                            this.errors = [];
                        }

                        if (this.$scope.messageCenter && this.$scope.messageCenter.filter) {
                            this.$scope.messageCenter = this.$scope.messageCenter.filter(function (el) {
                                return el.header !== "Select Reason Code";
                            });
                        } else {
                            this.$scope.messageCenter = [];
                        }

                        if (this.errors.length) {
                            this.errors.length = 0;
                        }

                        for (var i = 0, errorLength = validFormEntry.length; i < errorLength; i++) {
                            this.errors.push(validFormEntry[i].header);
                            Sandata.Utility.DisplayMessageCenter({
                                errorCheck: { status: "none" },
                                $timeout: that.$timeout,
                                scope: this.$scope,
                                type: "error",
                                text: validFormEntry[i].text,
                                header: validFormEntry[i].header
                            });
                        }
                    }
                };

                Init.prototype.assignUnknownCall = function () {
                    that = this;
                    this.DashboardService.confirmUnknownCall(this.assignUnknownCallModal.id, this.assignUnknownCallModal.updateId, this.assignUnknownCallModal.clientId, this.assignUnknownCallModal.reasonCode).then(function (data) {
                        //console.log("Response for confirmUnknownCall: ", data);
                        if (data.status == "SUCCESS") {
                            that.closeModal();

                            // Remove the row from the model.
                            var metadata = that.SandataCache.get("dashboardMetadata");
                            var rowKey = that.selectedUnknownCall.row.row_key;
                            var unknownCallArray = metadata.sections.unknowncalls_section.base_components.UNKNOWNCALLS_GRID.grid_row_components;

                            for (var i = 0, tempLength = unknownCallArray.length; i < tempLength; i++) {
                                if (rowKey && unknownCallArray[i]["row_key"] == rowKey) {
                                    unknownCallArray.splice(i, 1);
                                    break;
                                }
                            }

                            that.currentlyShownUnknownCalls = {};
                            that.updateUnknownCallsGrid({
                                gridData: "currentlyShownUnknownCalls",
                                gridSource: unknownCallArray,
                                currentPage: that.currentPage,
                                itemsPerPage: that.itemsPerPage,
                                recalculateNumberOfPages: true,
                                numberOfItems: "numberOfItems",
                                numberOfPages: "numberOfPages"
                            });

                            //$scope.updateGrid($scope.currentPage);
                            Sandata.Utility.DisplayMessageCenter({
                                $timeout: that.$timeout,
                                errorCheck: data,
                                scope: that.$scope,
                                type: "info",
                                text: data.message_detail,
                                header: data.message_summary
                            });
                        } else {
                            that.closeModal();

                            //$scope.updateGrid($scope.currentPage);
                            Sandata.Utility.DisplayMessageCenter({
                                $timeout: that.$timeout,
                                errorCheck: data,
                                scope: that.$scope,
                                type: "",
                                text: data.message_detail,
                                header: data.message_summary
                            });
                        }
                    });
                };

                Init.prototype.closeModal = function () {
                    this.dashboardConfirmModal.show = false;

                    //$scope.assignUnknownCallModal.show = false;
                    this.assignUnknownCallModal.id = "";
                    this.assignUnknownCallModal.updateId = "";
                    this.assignUnknownCallModal.clientId = "";
                    this.assignUnknownCallModal.reasonCode = "";
                };

                Init.prototype.onShowAuthDetails = function (index) {
                    this.authorization_section.base_components.AUTHORIZATIONS_GRID.grid_row_components[index].detail_section.base_components.reference.show = !this.authorization_section.base_components.AUTHORIZATIONS_GRID.grid_row_components[index].detail_section.base_components.reference.show;
                };

                Init.prototype.getDashboardData = function ($scope, DashboardService) {
                    var sections = DashboardService.getContent();
                    Sandata.Utility.AssignToScope($scope, sections.dashboard);
                };

                Init.prototype.loadOptions = function () {
                    var clientInitialValue;
                    this.clientOptionList = "";

                    if (typeof this.clientModel.value != "undefined") {
                        clientInitialValue = this.clientModel.value;
                    } else {
                        clientInitialValue = this.clientModel;
                    }

                    for (var i = 0, dsLength = this.clients.option_components.length; i < dsLength; i++) {
                        if (this.clients.option_components[i].value == clientInitialValue) {
                            this.clientOptionList = this.clientOptionList + "<option value='" + this.clients.option_components[i].value + "' selected>" + this.clients.option_components[i].label + "</option>";
                        } else {
                            this.clientOptionList = this.clientOptionList + "<option value='" + this.clients.option_components[i].value + "'>" + this.clients.option_components[i].label + "</option>";
                        }
                    }
                };
                return Init;
            })();
            Dashboard.Init = Init;
        })(Controller.Dashboard || (Controller.Dashboard = {}));
        var Dashboard = Controller.Dashboard;
    })(Sandata.Controller || (Sandata.Controller = {}));
    var Controller = Sandata.Controller;
})(Sandata || (Sandata = {}));
var Sandata;
(function (Sandata) {
    (function (Controller) {
        /// <reference path="../services/Sandata.Service.Visits.ts" />
        (function (Visits) {
            /*****************
            * Visits
            *********************/
            var Init = (function () {
                function Init($scope, $timeout, $location, $routeParams, SharedDataService, VisitsService, REGEXP_FORMATS, $window, $anchorScroll) {
                    this.$inject = ['$scope', '$timeout', '$location', '$routeParams', 'SharedDataService', 'VisitsService', 'REGEXP_FORMATS'];
                    this.scrollToPosition = function () {
                        var anchor = this.SharedDataService.getVisitPageScrollPosition();
                        var that = this;
                        if (anchor) {
                            this.$timeout(function () {
                                // call $anchorScroll()
                                that.$anchorScroll();
                                that.SharedDataService.resetVisitPageScrollPosition();
                            }, 100);
                        }
                    };
                    this.toggleGridEditView = function () {
                        this.showEdit = !this.showEdit;
                        this.showGrid = !this.showGrid;
                        // Sandata.Utility.HidePageLoaderGraphic()
                    };
                    this.getPreviousMessages = function () {
                        if (this.SharedDataService.getVisitPageMessages().length > 0) {
                            this.$scope.messageCenter = this.SharedDataService.getVisitPageMessages();
                        }
                        this.SharedDataService.resetVisitPageMessages();
                    };
                    this.updateCheckbox = function (index, bCheckbed) {
                        this.VISIT_GRID.grid_row_components[index].components.checkboxComponent.checked = !bCheckbed;
                        this.updateSelectedVisits();
                    };
                    this.updateCheckboxesFromCache = function () {
                        //console.log("Is There Cache Saved", this.SharedDataService.getVisitCheckboxes());
                        var cachedCheckboxes = this.SharedDataService.getVisitCheckboxes();
                        if (this.VISIT_GRID && cachedCheckboxes) {
                            for (var i = 0, visitsLength = this.VISIT_GRID.grid_row_components.length; i < visitsLength; i++) {
                                if (cachedCheckboxes.map(function (e) {
                                    return e.row_key;
                                }).indexOf(this.VISIT_GRID.grid_row_components[i].row_key) != -1) {
                                    this.VISIT_GRID.grid_row_components[i].components.checkboxComponent.checked = true;
                                }
                            }
                        }
                        this.updateSelectedVisits();
                    };
                    this.updateAllCheckBoxes = function (bCheckbed) {
                        this.masterCheckboxSelected = !bCheckbed;
                        for (var i = 0, visitsLength = this.VISIT_GRID.grid_row_components.length; i < visitsLength; i++) {
                            this.VISIT_GRID.grid_row_components[i].components.checkboxComponent.checked = this.masterCheckboxSelected;
                        }
                        this.updateSelectedVisits();
                    };
                    this.updateSelectedVisits = function () {
                        this.selectedVisitCheckboxes = [];
                        var selectedHours = 0;
                        for (var i = 0; i < this.VISIT_GRID.grid_row_components.length; i++) {
                            if (this.VISIT_GRID.grid_row_components[i].components.checkboxComponent.checked) {
                                this.selectedVisitCheckboxes.push({
                                    "row_key": this.VISIT_GRID.grid_row_components[i].row_key,
                                    "update_id": this.VISIT_GRID.grid_row_components[i].update_id,
                                    "hours": parseFloat(this.VISIT_GRID.grid_row_components[i].components.visitNumberOfHoursComponent.text)
                                });
                                selectedHours = selectedHours + parseFloat(this.VISIT_GRID.grid_row_components[i].components.visitNumberOfHoursComponent.text);
                            }
                        }
                        if (this.selectedVisitCheckboxes.length > 0) {
                            this.SharedDataService.setVisitCheckboxes(this.selectedVisitCheckboxes);
                            // console.log("Cached Saved", this.SharedDataService.getVisitCheckboxes())
                        } else {
                            this.SharedDataService.resetVisitCheckboxes();
                            // console.log("Cached Reset", this.SharedDataService.getVisitCheckboxes())
                        }

                        this.selectedHours = selectedHours;
                    };
                    this.watchCheckboxes = function () {
                        var that = this;
                        this.$scope.$watch('Visits.visitCheckboxFamily', function () {
                            that.selectedVisitCheckboxes = [];
                            var selectedHours = 0;
                            for (var i = 0; i < that.visitCheckboxFamily.length; i++) {
                                if (that.visitCheckboxFamily[i].checked) {
                                    that.selectedVisitCheckboxes.push({
                                        "row_key": that.visitCheckboxFamily[i].row_key,
                                        "update_id": that.visitCheckboxFamily[i].update_id,
                                        "hours": parseFloat(that.visitCheckboxFamily[i].components.visitNumberOfHoursComponent.text)
                                    });
                                    selectedHours = selectedHours + parseFloat(that.visitCheckboxFamily[i].components.visitNumberOfHoursComponent.text);
                                }
                            }
                            that.selectedHours = selectedHours;
                        }, true);
                    };
                    this.setupModals = function () {
                        /**********
                        SETUP VISIT MODAL
                        **********/
                        this.visitModalFocus = false;

                        this.VisitModal = { mode: "", formDisabled: false, formDateDisabled: false, edit_id: "" };
                        this.VisitModal.opts = {
                            backdropFade: false,
                            dialogFade: false,
                            dialogClass: "modal-large",
                            backdropClick: false
                        };

                        /**********
                        SETUP Hours Approved MODAL
                        **********/
                        this.approveHoursFocus = false;
                        this.approvedVisitModal = {};
                        this.approvedVisitModal.opts = {
                            backdropFade: false,
                            dialogFade: false,
                            dialogClass: "modal",
                            backdropClick: false
                        };

                        /*****************
                        Unapproved Visit Button
                        ******************/
                        this.unApprovedVisitModal = {};
                        this.unApprovedVisitModal.tmp_unapprove_id = "";
                        this.unApprovedVisitModal.tmp_unapprove_upid = "";
                        this.unapproveHoursFocus = false;

                        this.unApprovedVisitModal.opts = {
                            backdropFade: false,
                            dialogFade: false,
                            dialogClass: "modal",
                            backdropClick: false
                        };

                        /**********************
                        Ignore Visit Button
                        **********************/
                        this.ignoreVisitModal = {};
                        this.ignoreVisitModal.tmp_ignore_id = "";
                        this.ignoreVisitModal.tmp_ignore_upid = "";
                        this.ignoreVisitFocus = false;
                        this.ignoreVisitModal.opts = {
                            backdropFade: false,
                            dialogFade: false,
                            dialogClass: "modal",
                            backdropClick: false
                        };

                        /**********************
                        Unignore Visit Button
                        ***********************/
                        this.unignoreVisitModal = {};
                        this.unignoreVisitModal.tmp_unignore_id = "";
                        this.unignoreVisitModal.tmp_unignore_upid = "";
                        this.unignoreVisitFocus = false;
                        this.unignoreVisitModal.opts = {
                            backdropFade: false,
                            dialogFade: false,
                            dialogClass: "modal",
                            backdropClick: false
                        };
                    };
                    this.setupPagination = function () {
                        /*******
                        Pagination
                        ********/
                        //SETUP FOR PAGINATION... IN FUTURE..
                        //--start
                        if (this.$routeParams.p) {
                            this.currentPage = parseInt(this.$routeParams.p);
                        } else {
                            this.currentPage = 1;
                        }
                        this.maxSize = 2;
                        //-end
                    };
                    this.setupFiltersSection = function () {
                        this.filterSection = {};
                        this.filterSection.messageTab = { tabActive: "", bodyActive: "" };
                        this.filterSection.filterTab = { tabActive: "active", bodyActive: "active" };
                        this.filterSection.msgCenterTrigger = this.msgCenterTrigger;
                        this.filterSection.filterCenterTrigger = this.filterCenterTrigger;
                    };
                    this.initializeFilters = function () {
                        var filters = this.SharedDataService.getVisitPageFilters();
                        for (key in filters) {
                            if (this.SharedDataService.getVisitPageFilter(key)) {
                                if (key == ("ignoreVisitsModel" || "approvedVisitModel" || "incompleteVisitModel" || "visitApprovalReadyModel")) {
                                    this[key].selected = this.SharedDataService.getVisitPageFilter(key);
                                } else {
                                    this[key] = this.SharedDataService.getVisitPageFilter(key);
                                }
                            }
                        }
                        ;
                    };
                    this.initializePayPeriod = function () {
                        var visitFilters = this.VisitsService.getContent();

                        /*Sandata.Utility.AssignToScopeAndSetModel({$scope:this,
                        data:visitFilters.pay_period,
                        arObj:"option_components",
                        variable:"pay_period",
                        model:"pay_periodModel",
                        index:0,
                        byvalue:true});*/
                        //this.updateDatePeriod()
                        if (this.SharedDataService.getPayPeriod() != null) {
                            if (typeof this.SharedDataService.getPayPeriod().value != "undefined") {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: visitFilters.pay_period,
                                    arObj: "option_components",
                                    variable: "pay_period",
                                    model: "pay_periodModel",
                                    index: this.SharedDataService.getPayPeriod().value,
                                    byvalue: true });
                                //this.pay_periodModel = this.SharedDataService.getPayPeriod()
                            } else {
                                if (this.SharedDataService.getPayPeriod() == "") {
                                    Sandata.Utility.AssignToScopeAndSetModel({
                                        $scope: this,
                                        data: visitFilters.pay_period,
                                        arObj: "option_components",
                                        variable: "pay_period",
                                        model: "pay_periodModel",
                                        index: this.SharedDataService.getPayPeriod(),
                                        byvalue: true });

                                    this.resetPayPeriod();
                                } else {
                                    Sandata.Utility.AssignToScopeAndSetModel({
                                        $scope: this,
                                        data: visitFilters.pay_period,
                                        arObj: "option_components",
                                        variable: "pay_period",
                                        model: "pay_periodModel",
                                        index: this.SharedDataService.getPayPeriod(),
                                        byvalue: true });
                                }
                            }
                        } else {
                            if (this.SharedDataService.getPayPeriod() == "") {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: visitFilters.pay_period,
                                    arObj: "option_components",
                                    variable: "pay_period",
                                    model: "pay_periodModel",
                                    index: 0 });

                                that.resetPayPeriod();
                            } else {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: visitFilters.pay_period,
                                    arObj: "option_components",
                                    variable: "pay_period",
                                    model: "pay_periodModel",
                                    index: 0 });
                            }
                        }
                    };
                    this.resetPayPeriod = function () {
                        this.pay_periodModel = "";
                        this.SharedDataService.setPayPeriod("");
                    };
                    this.initializePayors = function () {
                        var visitFilters = this.VisitsService.getContent();

                        // If Payors already selected in another page, then set select field to that value
                        if (this.$routeParams.payor) {
                            //Sandata.Utility.SetDefaultSelectValue(this,visitFilters.payors.base_components.PAYOR_NAMES,"payors","payorModel", this.$routeParams.payor,true);
                            Sandata.Utility.AssignToScopeAndSetModel({
                                $scope: this,
                                data: visitFilters.payors.base_components.PAYOR_NAMES,
                                arObj: "option_components",
                                variable: "payors",
                                model: "payorModel",
                                index: this.$routeParams.payor,
                                byvalue: true });
                        } else if (this.SharedDataService.getPayor() != null) {
                            //Sandata.Utility.SetDefaultSelectValue(this,visitFilters.payors.base_components.PAYOR_NAMES,"payors","payorModel",this.SharedDataService.getPayor(),true);
                            //Comment
                            if (typeof this.SharedDataService.getPayor().value != "undefined") {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: visitFilters.payors.base_components.PAYOR_NAMES,
                                    arObj: "option_components",
                                    variable: "payors",
                                    model: "payorModel",
                                    index: this.SharedDataService.getPayor().value,
                                    byvalue: true });
                            } else {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: visitFilters.payors.base_components.PAYOR_NAMES,
                                    arObj: "option_components",
                                    variable: "payors",
                                    model: "payorModel",
                                    index: this.SharedDataService.getPayor(),
                                    byvalue: true });
                            }
                        } else {
                            //Sandata.Utility.SetDefaultSelectValue(this,visitFilters.payors.base_components.PAYOR_NAMES,"payors","payorModel",this.$routeParams.payor,true);
                            Sandata.Utility.AssignToScopeAndSetModel({
                                $scope: this,
                                data: visitFilters.payors.base_components.PAYOR_NAMES,
                                arObj: "option_components",
                                variable: "payors",
                                model: "payorModel",
                                index: 0 });
                        }
                    };
                    this.updateDatePeriod = function () {
                        if (typeof this.pay_periodModel.start_date != "undefined" && typeof this.pay_periodModel.end_date != "undefined") {
                            var match = /\-/gi;
                            var tempFromDate = this.pay_periodModel.start_date.replace(match, "/");
                            var tempToDate = this.pay_periodModel.end_date.replace(match, "/");

                            this.fromDateModel = $.datepicker.formatDate('mm/dd/yy', new Date(tempFromDate));
                            this.toDateModel = $.datepicker.formatDate('mm/dd/yy', new Date(tempToDate));
                        }
                    };
                    this.getVisitsByPayPeriod = function () {
                        this.updateDatePeriod();
                        var that = this;
                        this.$timeout(function () {
                            if (typeof that.pay_periodModel.start_date != "undefined" && typeof that.pay_periodModel.end_date != "undefined") {
                                that.changePage();
                            }
                        }, 0);
                    };
                    this.initializeEmployees = function () {
                        var visitFilters = this.VisitsService.getContent();

                        // If Employee already selected in another page, then set select field to that value
                        if (this.$routeParams.employee) {
                            Sandata.Utility.AssignToScopeAndSetModel({
                                $scope: this,
                                data: visitFilters.employees,
                                arObj: "option_components",
                                variable: "employees",
                                model: "employeeModel",
                                index: this.$routeParams.employee,
                                byvalue: true });
                        } else if (this.SharedDataService.getEmployee() != null) {
                            if (typeof this.SharedDataService.getEmployee().value != "undefined") {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: visitFilters.employees,
                                    arObj: "option_components",
                                    variable: "employees",
                                    model: "employeeModel",
                                    index: this.SharedDataService.getEmployee().value,
                                    byvalue: true });
                            } else {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: visitFilters.employees,
                                    arObj: "option_components",
                                    variable: "employees",
                                    model: "employeeModel",
                                    index: this.SharedDataService.getEmployee(),
                                    byvalue: true });
                            }
                        } else {
                            Sandata.Utility.AssignToScopeAndSetModel({
                                $scope: this,
                                data: visitFilters.employees,
                                arObj: "option_components",
                                variable: "employees",
                                model: "employeeModel",
                                index: 0 });
                        }
                    };
                    this.initializeClients = function () {
                        var visitFilters = this.VisitsService.getContent();

                        // If Client already selected in another page, then set select field to that value
                        if (this.$routeParams.client) {
                            Sandata.Utility.AssignToScopeAndSetModel({
                                $scope: this,
                                data: visitFilters.clients,
                                arObj: "option_components",
                                variable: "clients",
                                model: "clientModel",
                                index: this.$routeParams.client,
                                byvalue: true });
                        }
                        if (this.SharedDataService.getClient() != null) {
                            if (typeof this.SharedDataService.getClient().value != "undefined") {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: visitFilters.clients,
                                    arObj: "option_components",
                                    variable: "clients",
                                    model: "clientModel",
                                    index: this.SharedDataService.getClient().value,
                                    byvalue: true });
                            } else {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: visitFilters.clients,
                                    arObj: "option_components",
                                    variable: "clients",
                                    model: "clientModel",
                                    index: this.SharedDataService.getClient(),
                                    byvalue: true });
                            }
                        } else {
                            Sandata.Utility.AssignToScopeAndSetModel({
                                $scope: this,
                                data: visitFilters.clients,
                                arObj: "option_components",
                                variable: "clients",
                                model: "clientModel",
                                index: 0 });
                        }
                    };
                    this.getPayor = function (dontRefresh) {
                        var that = this;
                        this.VisitsService.getPayor(this.clientModel.value).then(function (response) {
                            that.payors = response.data.base_components.PAYOR_NAMES;
                            that.payorModel = that.payors.option_components[0];
                            if (typeof dontRefresh != 'undefined') {
                            } else {
                                that.changePage();
                            }
                        });
                    };
                    this.changePage = function (page) {
                        if (typeof this.payorModel == 'undefined') {
                            this.payorModel = {
                                "value": "",
                                "label": "",
                                "group": "",
                                "show": false,
                                "modal": false,
                                "disabled": false,
                                "actions": {}
                            };
                            //this.payorModel.value = "";
                        }

                        /*if(typeof $scope.employeeModel.value == 'undefined' && typeof $scope.employeeModel != 'undefined'){
                        
                        jQuery.grep($scope.employees, function (passesObj, index) {
                        if( passesObj.value == $scope.employeeModel){
                        $scope.employeeModel = passesObj;
                        }
                        });
                        }
                        
                        if(typeof $scope.clientModel.value == 'undefined' && typeof $scope.clientModel != 'undefined'){
                        
                        jQuery.grep($scope.clients, function (passesObj, index) {
                        if( passesObj.value == $scope.clientModel){
                        $scope.clientModel = passesObj;
                        }
                        });
                        }*/
                        if ($("#visitFilters").parsley('validate') == false) {
                            this.$scope.toggleSidePanel(true);
                        } else {
                            if (Sandata.Utility.isValidDate(this.toDateModel) && Sandata.Utility.isValidDate(this.fromDateModel)) {
                                Sandata.Utility.RemoveMessages({ scope: this.$scope, messageHeaders: ["To Date Error", "From Date Error"] });
                                this.clearSelectedVisits();
                                this.updateGrid();
                            } else {
                                Sandata.Utility.RemoveMessages({ scope: this.$scope, messageHeaders: ["To Date Error", "From Date Error"] });
                                if (!Sandata.Utility.isValidDate(this.toDateModel)) {
                                    Sandata.Utility.DisplayMessageCenter({
                                        $timeout: that.$timeout,
                                        errorCheck: { status: "none" },
                                        scope: this.$scope,
                                        type: "error",
                                        text: "Please enter a valid date.",
                                        header: "To Date Error" });
                                    this.toDateModel = "";
                                    if (!$scope.$$phase) {
                                        $scope.$apply();
                                    }
                                }

                                if (!Sandata.Utility.isValidDate(this.fromDateModel)) {
                                    Sandata.Utility.DisplayMessageCenter({
                                        $timeout: that.$timeout,
                                        errorCheck: { status: "none" },
                                        scope: this.$scope,
                                        type: "error",
                                        header: "From Date Error",
                                        text: "Please enter a valid date." });
                                    this.fromDateModel = "";
                                    if (!$scope.$$phase) {
                                        $scope.$apply();
                                    }
                                }

                                //this.SharedDataService.setVisitPageMessages(this.$scope.messageCenter);
                                this.msgCenterTrigger();
                            }
                        }
                        // USE BELOW COMMENTED CODE IF YOU WOULD LIKE TO USE PAGINATION.
                        //JUST MAKE SURE TO COMMENT OUT $scope.updateGrid()
                        /*$scope.$watch('currentPage',function(){
                        $scope.url = $location.path();
                        if(page){
                        $scope.url += "?p=" + page;
                        }else{
                        $scope.url += "?p=" +  $scope.currentPage;
                        }
                        
                        if($scope.sidepanel == 1 || $scope.sidepanel == 0 ){
                        $scope.url +="&sidepanel=" +  $scope.sidepanel;
                        }else if($routeParams.sidepanel){
                        $scope.url +="&sidepanel=" +  $routeParams.sidepanel;
                        }else{
                        $scope.url +="&sidepanel="  + 0;
                        }
                        $scope.url += "&employee=" + $scope.employeeModel.value;
                        $scope.url+= "&payor=" + $scope.payorModel.value;
                        $scope.url+= "&client=" + $scope.clientModel.value;
                        $scope.url+= "&ignore_visits=" + $scope.ignoreVisitsModel.selected;
                        $scope.url+= "&incomplete_visit=" + $scope.incompleteVisitModel.selected;
                        $scope.url+= "&visit_approval_ready=" + $scope.visitApprovalReadyModel.selected;
                        $scope.url+= "&approved_visits=" + $scope.approvedVisitModel.selected;
                        $scope.url+= "&from_date=" +  encodeURIComponent($("input#fromDate").val());
                        $scope.url+= "&to_date=" +  encodeURIComponent($("input#toDate").val());
                        
                        $location.url( $scope.url);
                        });*/
                    };
                    this.updateMasterCheckbox = function () {
                        this.masterCheckboxSelected = !this.masterCheckboxSelected;
                    };
                    this.clearSelectedVisits = function () {
                        this.approvedHoursSelected = 0;

                        // $("input." + this.checkboxes).prop("checked",false);
                        // $("input#"+ this.masterCheckbox).prop("checked",false)
                        //  if ($.browser.version < 9.0) { $("input[type='checkbox'] + label span").css("background-position", "0px 0px"); }
                        this.selectedHours = 0;
                        this.masterCheckboxSelected = false;
                        this.approvedSelectedList = [];
                        this.approvedSelectedList_UpdateId = [];
                        this.visitCheckboxFamily = [];
                        this.selectedVisitCheckboxes = [];
                    };
                    this.updateGrid = function () {
                        if (typeof this.payorModel == 'undefined') {
                            this.payorModel = {
                                "value": "",
                                "label": "",
                                "group": "",
                                "show": false,
                                "modal": false,
                                "disabled": false,
                                "actions": {}
                            };
                            ;
                            //this.payorModel.value = "";
                        }

                        var clientValue;
                        var employeeValue;
                        var payorValue;

                        if (typeof this.employeeModel.value == 'undefined' && typeof this.employeeModel != 'undefined') {
                            employeeValue = this.employeeModel;
                        } else {
                            employeeValue = this.employeeModel.value;
                        }

                        if (typeof this.clientModel.value == 'undefined' && typeof this.clientModel != 'undefined') {
                            clientValue = this.clientModel;
                        } else {
                            clientValue = this.clientModel.value;
                        }

                        if (typeof this.payorModel.value == 'undefined' && typeof this.payorModel != 'undefined') {
                            payorValue = this.payorModel;
                        } else {
                            payorValue = this.payorModel.value;
                        }

                        //Set client, employee and payor models to be shared with other pages
                        this.SharedDataService.setClient(this.clientModel);
                        this.SharedDataService.setEmployee(this.employeeModel);
                        this.SharedDataService.setPayor(this.payorModel);
                        this.SharedDataService.setPayPeriod(this.pay_periodModel);
                        this.SharedDataService.setVisitPageFilter("ignoreVisitsModel", this.ignoreVisitsModel.selected);
                        this.SharedDataService.setVisitPageFilter("incompleteVisitModel", this.incompleteVisitModel.selected);
                        this.SharedDataService.setVisitPageFilter("visitApprovalReadyModel", this.visitApprovalReadyModel.selected);
                        this.SharedDataService.setVisitPageFilter("approvedVisitModel", this.approvedVisitModel.selected);
                        this.SharedDataService.setVisitPageFilter("fromDateModel", this.fromDateModel);
                        this.SharedDataService.setVisitPageFilter("toDateModel", this.toDateModel);

                        //USE WITH MOCK
                        var that = this;

                        //console.log("What is the fromDateModel",this.fromDateModel, " vs Input From Value:", $("input#fromDate").val(), " vs Whats Passed From Services : FRom:",this.VISIT_FILTER_TAB_FROM_DATE_CONTROL.date_value);
                        //console.log("What is the toDateModel",this.toDateModel, " vs Input To Value:", $("input#toDate").val(), " vs Whats Passed From Services : To :",this.VISIT_FILTER_TAB_TO_DATE_CONTROL.date_value);
                        this.VisitsService.getVisits(this.$routeParams.p, clientValue, payorValue, employeeValue, this.ignoreVisitsModel.selected, this.incompleteVisitModel.selected, this.visitApprovalReadyModel.selected, this.approvedVisitModel.selected, $.datepicker.formatDate('yy-mm-dd', new Date(this.fromDateModel)), $.datepicker.formatDate('yy-mm-dd', new Date(this.toDateModel))).then(function (response) {
                            if (response.status == "FAILED") {
                                Sandata.Utility.DisplayMessageCenter({
                                    $timeout: that.$timeout,
                                    errorCheck: response,
                                    scope: that.$scope,
                                    type: "error",
                                    text: response.message_detail,
                                    header: response.message_summary });

                                //Temp Action
                                that.msgCenterTrigger();
                                //that.SharedDataService.setVisitPageMessages(that.$scope.messageCenter);
                            } else {
                                if (!that.title) {
                                    that.title = response.data.title;
                                }
                                that.clearSelectedVisits();

                                for (var i = 0, visitLength = response.data.sections.visits_section.base_components.VISIT_GRID.grid_row_components.length; i < visitLength; i++) {
                                    response.data.sections.visits_section.base_components.VISIT_GRID.grid_row_components[i]["index"] = i;
                                }
                                Sandata.Utility.AssignToScope(that, response.data);
                                Sandata.Utility.AssignToScope(that, response.data.sections.visits_section.base_components);
                                Sandata.Utility.AssignToScope(that, response.data.sections.visit_bottom_controls.base_components);

                                angular.forEach(that.VISIT_GRID.grid_row_components, function (row) {
                                    row.components.sortVisitDateLabelComponent = new Date(row.components.visitDateLabelComponent.text);
                                    row.components.sortVisitStartTimeLabelComponent = new Date("1/1/1970 " + row.components.visitStartTimeLabelComponent.text);
                                    row.components.sortVisitEndTimeLabelComponent = new Date("1/1/1970 " + row.components.visitEndTimeLabelComponent.text);
                                    row.components.visitNumberOfHoursComponent.text = parseFloat(row.components.visitNumberOfHoursComponent.text);
                                });

                                //Get Grid Columns
                                Sandata.Utility.AssignObjectsInArrayToScopeObject(that, that.VISIT_GRID.grid_row_header.components, "name", "grid_columns", true);
                                that.updateCheckboxesFromCache();
                                that.scrollToPosition();
                            }
                        });
                    };
                    this.openIgnoreVisit = function (id, upid) {
                        this.ignoreVisitModal.show = true;
                        this.ignoreVisitModal.tmp_ignore_id = id;
                        this.ignoreVisitModal.tmp_ignore_upid = upid;
                        this.ignoreVisitFocus = true;
                    };
                    this.ignoreVisit = function () {
                        var that = this;
                        this.VisitsService.ignoreVisit(this.ignoreVisitModal.tmp_ignore_id, this.ignoreVisitModal.tmp_ignore_upid).then(function (data) {
                            /*if(data.status == "FAILED"){
                            var actions = [$scope.Visits.clearSelectedVisits,$scope.Visits.updateGrid,$scope.closeIgnoreVisit,$scope.filterSection.msgCenterTrigger];
                            }else{
                            var actions = [$scope.Visits.clearSelectedVisits,$scope.Visits.updateGrid,$scope.closeIgnoreVisit,$scope.filterSection.msgCenterTrigger];
                            }*/
                            var type = "info";
                            var text = "Visit is has been ignored";
                            Sandata.Utility.DisplayMessageCenter({
                                errorCheck: data,
                                $timeout: that.$timeout,
                                scope: that.$scope,
                                type: type,
                                text: data.message_detail,
                                header: data.message_summary
                            });

                            // that.SharedDataService.setVisitPageMessages(that.$scope.messageCenter);
                            that.clearSelectedVisits();
                            that.updateGrid();
                            that.closeIgnoreVisit();
                            that.msgCenterTrigger();

                            that.ignoreVisitModal.tmp_ignore_id = "";
                            that.ignoreVisitModal.tmp_ignore_upid = "";
                        });
                    };
                    this.validateModal = function () {
                        return $("#visitModalForm").parsley('validate');
                    };
                    this.openAddVisit = function (id) {
                        var url = "/visits/form";
                        var fields = { mode: "add", title: "Add Visit", formDisabled: false, formDateDisabled: false, EVVIn: "", EVVOut: "" };
                        Sandata.Utility.AssignValuesToVisitModal(this, "VisitModal", this.VisitsService.localLayout);

                        //Sandata.Utility.Modal(this,"VisitModal",true,"Add Visit",fields,{scrollToTop:true});
                        //this.visitModalFocus = true;
                        Sandata.Utility.AssignToScope(this.VisitModal, fields);

                        //this.views ="visit_edit";
                        //this.toggleGridEditView()
                        this.SharedDataService.setVisitForm(this.VisitModal);
                        var key = id;
                        this.SharedDataService.setVisitPageScrollPosition(key.toString());

                        this.$location.path(url);
                    };
                    this.closeVisitModal = function () {
                        Sandata.Utility.ShowPageLoaderGraphic();
                        var fields = { mode: "", title: "", formDisabled: true, formDateDisabled: true, EVVIn: "", EVVOut: "" };
                        Sandata.Utility.AssignValuesToVisitModal(this, "VisitModal", this.VisitsService.localLayout);

                        //Sandata.Utility.Modal(this,"VisitModal",false,"",fields);
                        //this.visitModalFocus = false;
                        Sandata.Utility.AssignToScope(this.VisitModal, fields);
                        this.views = "visit_grid";
                        this.toggleGridEditView();
                        this.$timeout(function () {
                            Sandata.Utility.HidePageLoaderGraphic();
                        }, 0);
                    };
                    this.openIgnoredVisit = function (id) {
                        this.SharedDataService.resetVisitForm();
                        var url = "/visits/form?id=" + id;
                        var that = this;
                        this.VisitsService.getVisit(id).then(function (response) {
                            var fields = { mode: "", title: "Ignored Visit", formDisabled: true, formDateDisabled: true, edit_id: id, update_id: response.data.update_id };
                            Sandata.Utility.AssignValuesToVisitModal(that, "VisitModal", that.VisitsService.localLayout, response.data);

                            // Sandata.Utility.Modal(that,"VisitModal",true,"Ignored Visit",fields,{scrollToTop:true});
                            //that.visitModalFocus = true;
                            Sandata.Utility.AssignToScope(that.VisitModal, fields);

                            that.SharedDataService.setVisitForm(that.VisitModal);

                            that.$location.url(url);
                            //that.views = "visit_edit";
                            // that.toggleGridEditView()
                        });
                    };
                    this.openApprovedVisit = function (id, upid) {
                        var that = this;
                        var url = "/visits/form?id=" + id;
                        this.VisitsService.getVisit(id).then(function (response) {
                            var fields = { mode: "", title: "Approved Visit", formDisabled: true, formDateDisabled: true, edit_id: id, update_id: response.data.update_id };
                            Sandata.Utility.AssignValuesToVisitModal(that, "VisitModal", that.VisitsService.localLayout, response.data);

                            //Sandata.Utility.Modal(that,"VisitModal",true,"Approved Visit",fields,{scrollToTop:true});
                            //that.visitModalFocus = true;
                            Sandata.Utility.AssignToScope(that.VisitModal, fields);

                            // that.views = "visit_edit";
                            //that.toggleGridEditView()
                            that.SharedDataService.setVisitForm(that.VisitModal);

                            that.$location.url(url);
                        });
                    };
                    this.openEditVisit = function (id, upid) {
                        var that = this;
                        var url = "/visits/form?id=" + id;
                        this.VisitsService.getVisit(id).then(function (response) {
                            var fields = { mode: "edit", title: "Edit Visit", formDisabled: false, formDateDisabled: true, edit_id: id, update_id: response.data.update_id, title: "Edit Visit" };
                            Sandata.Utility.AssignValuesToVisitModal(that, "VisitModal", that.VisitsService.localLayout, response.data);
                            Sandata.Utility.AssignToScope(that.VisitModal, fields);

                            //Sandata.Utility.Modal(that,"VisitModal",true,"Edit Visit",fields,{scrollToTop:true});
                            //that.visitModalFocus = true;
                            //that.views ="visit_edit";
                            //that.toggleGridEditView()
                            that.SharedDataService.setVisitForm(that.VisitModal);
                            var key = id;
                            that.SharedDataService.setVisitPageScrollPosition(key.toString());
                            that.$location.url(url);
                        });
                    };
                    this.openApprovedVisitModal = function (i) {
                        this.approvedHoursSelected = this.selectedHours;
                        this.approvedVisitModal.show = true;
                        this.approveHoursFocus = true;
                    };
                    this.approveVisitsSelected = function () {
                        var that = this;
                        var row_keys = [], update_ids = [];
                        for (var i = 0, checkedArray = this.selectedVisitCheckboxes.length; i < checkedArray; i++) {
                            row_keys.push(this.selectedVisitCheckboxes[i].row_key);
                            update_ids.push(this.selectedVisitCheckboxes[i].update_id);
                        }
                        if (update_ids.length > 0 && row_keys.length > 0) {
                            this.VisitsService.markApproved(row_keys, update_ids).then(function (data) {
                                //console.log("Whats the message : ",data);
                                /*if(data.status == "FAILED"){
                                var actions = [that.clearSelectedVisits,that.updateGrid,that.closeApprovedVisitModal,that.filterSection.msgCenterTrigger];
                                }else{
                                var actions = [that.clearSelectedVisits,that.updateGrid,that.closeApprovedVisitModal,that.filterSection.msgCenterTrigger];
                                }*/
                                var type = "approved";
                                var text = "";
                                var hours = $scope.approvedHoursSelected;
                                Sandata.Utility.DisplayMessageCenter({
                                    errorCheck: data,
                                    $timeout: that.$timeout,
                                    scope: that.$scope,
                                    type: type,
                                    text: data.message_detail,
                                    header: data.message_summary,
                                    hours: hours
                                });

                                //that.SharedDataService.setVisitPageMessages(that.$scope.messageCenter);
                                that.clearSelectedVisits();
                                that.SharedDataService.resetVisitCheckboxes();
                                that.updateGrid();
                                that.closeApprovedVisitModal();
                                that.msgCenterTrigger();
                            });
                        } else {
                            that.clearSelectedVisits();
                            that.SharedDataService.resetVisitCheckboxes();
                            that.updateGrid();
                            that.closeApprovedVisitModal();
                        }
                    };
                    this.closeApprovedVisitModal = function () {
                        this.approvedVisitModal.show = false;
                        //this.clearSelectedVisits();
                    };
                    this.openUnapprovedVisit = function (id, upid) {
                        this.unApprovedVisitModal.show = true;
                        this.unApprovedVisitModal.tmp_unapprove_id = id;
                        this.unApprovedVisitModal.tmp_unapprove_upid = upid;
                        this.unapproveHoursFocus = true;
                    };
                    this.unApprovedVisit = function () {
                        var that = this;
                        this.VisitsService.unApproveVisit(this.unApprovedVisitModal.tmp_unapprove_id, this.unApprovedVisitModal.tmp_unapprove_upid).then(function (data) {
                            /*if(data.status == "FAILED"){
                            actions = [that.clearSelectedVisits,that.updateGrid,that.closeUnapprovedVisitModal,that.filterSection.msgCenterTrigger];
                            }else{
                            var actions = [$scope.clearSelectedVisits,$scope.updateGrid,$scope.closeUnapprovedVisitModal,$scope.filterSection.msgCenterTrigger];
                            }*/
                            var type = "info";
                            var text = "Visit is no longer approved";

                            //console.log("Unapprove : ", data);
                            Sandata.Utility.DisplayMessageCenter({
                                errorCheck: data,
                                $timeout: that.$timeout,
                                scope: that.$scope,
                                type: type,
                                text: data.message_detail,
                                header: data.message_summary
                            });

                            //that.SharedDataService.setVisitPageMessages(that.$scope.messageCenter);
                            that.clearSelectedVisits();
                            that.updateGrid();
                            that.closeUnapprovedVisitModal();
                            that.msgCenterTrigger();

                            that.unApprovedVisitModal.tmp_unapprove_id = "";
                            that.unApprovedVisitModal.tmp_unapprove_upid = "";
                        });
                    };
                    this.closeUnapprovedVisitModal = function () {
                        this.unApprovedVisitModal.show = false;
                        this.unApprovedVisitModal.tmp_unapprove_id = "";
                    };
                    this.closeIgnoreVisit = function () {
                        this.ignoreVisitModal.show = false;
                        this.ignoreVisitModal.tmp_ignore_id = "";
                        this.ignoreVisitModal.tmp_ignore_upid = "";
                    };
                    this.openUnignoreVisit = function (id, upid) {
                        this.unignoreVisitModal.show = true;
                        this.unignoreVisitModal.tmp_unignore_id = id;
                        this.unignoreVisitModal.tmp_unignore_upid = upid;
                        this.unignoreVisitFocus = true;
                    };
                    this.unignoreVisit = function () {
                        var that = this;
                        this.VisitsService.unignoreVisit(this.unignoreVisitModal.tmp_unignore_id, this.unignoreVisitModal.tmp_unignore_upid).then(function (data) {
                            /*if(data.status == "FAILED"){
                            var actions = [that.clearSelectedVisits,that.updateGrid,that.closeUnignoreVisit,that.filterSection.msgCenterTrigger];
                            }else{
                            var actions = [$scope.clearSelectedVisits,$scope.updateGrid,$scope.closeUnignoreVisit,$scope.filterSection.msgCenterTrigger];
                            }*/
                            var type = "info";
                            var text = "Visit is no longer ignored";
                            Sandata.Utility.DisplayMessageCenter({
                                errorCheck: data,
                                $timeout: that.$timeout,
                                scope: that.$scope,
                                type: type,
                                text: data.message_detail,
                                header: data.message_summary
                            });

                            // that.SharedDataService.setVisitPageMessages(that.$scope.messageCenter);
                            that.clearSelectedVisits();
                            that.updateGrid();
                            that.closeUnignoreVisit();
                            that.msgCenterTrigger();
                            that.unignoreVisitModal.tmp_unignore_id = "";
                            that.unignoreVisitModal.tmp_unignore_upid = "";
                        });
                    };
                    this.closeUnignoreVisit = function () {
                        this.unignoreVisitModal.show = false;
                        this.unignoreVisitModal.tmp_unignore_id = "";
                        this.unignoreVisitModal.tmp_unignore_upid = "";
                    };
                    this.uncheck = function (model) {
                        //console.log("$scope[model]: ", $scope[model]);
                        if (this[model].selected == "true") {
                            this[model].selected = "false";
                        } else {
                            this[model].selected = "true";
                        }
                    };
                    this.msgCenterTrigger = function () {
                        this.filterSection.messageTab.tabActive = "active";
                        this.filterSection.messageTab.bodyActive = "active";
                        this.filterSection.filterTab.tabActive = "";
                        this.filterSection.filterTab.bodyActive = "";
                        $("a[ title='Messages']").focus();
                    };
                    this.filterCenterTrigger = function () {
                        this.filterSection.messageTab.tabActive = "";
                        this.filterSection.messageTab.bodyActive = "";
                        this.filterSection.filterTab.tabActive = "active";
                        this.filterSection.filterTab.bodyActive = "active";
                        $("a[ title='Filters']").focus();
                    };
                    this.loadOptions = function () {
                        var employeeInitialValue, clientInitialValue;
                        this.employeeOptionList = "";
                        this.clientOptionList = "";
                        if (typeof this.employeeModel.value != "undefined") {
                            employeeInitialValue = this.employeeModel.value;
                        } else {
                            employeeInitialValue = this.employeeModel;
                        }

                        if (typeof this.clientModel.value != "undefined") {
                            clientInitialValue = this.clientModel.value;
                        } else {
                            clientInitialValue = this.clientModel;
                        }

                        for (var i = 0, dsLength = this.employees.option_components.length; i < dsLength; i++) {
                            if (this.employees.option_components[i].value == employeeInitialValue) {
                                //console.log("What are the Values : ", this.dataSource[i][$attrs.optionValue])
                                this.employeeOptionList = this.employeeOptionList + "<option value='" + this.employees.option_components[i].value + "' selected>" + this.employees.option_components[i].label + "</option>";
                            } else {
                                this.employeeOptionList = this.employeeOptionList + "<option value='" + this.employees.option_components[i].value + "'>" + this.employees.option_components[i].label + "</option>";
                            }
                        }

                        i = null;
                        dsLength = null;

                        for (var i = 0, dsLength = this.clients.option_components.length; i < dsLength; i++) {
                            if (this.clients.option_components[i].value == clientInitialValue) {
                                //console.log("What are the Values : ", this.dataSource[i][$attrs.optionValue])
                                this.clientOptionList = this.clientOptionList + "<option value='" + this.clients.option_components[i].value + "' selected>" + this.clients.option_components[i].label + "</option>";
                            } else {
                                this.clientOptionList = this.clientOptionList + "<option value='" + this.clients.option_components[i].value + "'>" + this.clients.option_components[i].label + "</option>";
                            }
                        }
                    };
                    this.$location = $location;
                    this.$routeParams = $routeParams;
                    this.SharedDataService = SharedDataService;
                    this.VisitsService = VisitsService;
                    this.REGEXP_FORMATS = REGEXP_FORMATS;
                    this.$window = $window;
                    this.$anchorScroll = $anchorScroll;
                    this.$scope = $scope;
                    this.$timeout = $timeout;
                    this.views = "visit_grid";
                    this.showEdit = false;
                    this.showGrid = true;
                    this.title;

                    Sandata.Utility.ReloadCheck();

                    /*************************
                    Diable Form Elements Setup
                    **************************/
                    Sandata.Utility.DisableElement(this, "diabledSubmits");

                    /**********************
                    Filter Area
                    ***********************/
                    $scope.FilterFocus = false;

                    Sandata.Utility.Setup_ShowHideSidePanel($scope, "sidePanel", this.$routeParams.sidepanel, "FilterFocus", "a#");
                    if (this.$routeParams.showmessages) {
                        this.$timeout(function () {
                            $("a#Messages").click();
                        }, 0);
                    }

                    this.MessageCenter = {
                        expanded: false,
                        disabled: false,
                        controls: "widgets-area",
                        label: "Click to Show Visits Filters and Messages Center",
                        name: "Show Visits Filters and Messages Center",
                        title: "Show Visits Filters and Messages Center"
                    };

                    /***********************************
                    Assign Time Format Utility to Scope
                    **********************************/
                    this.formatTime = Sandata.Utility.FormatTime;
                    this.time_format = this.VisitsService.getContent().time_format;

                    this.visitCheckboxFamily = [];
                    this.selectedVisitCheckboxes = [];
                    this.masterCheckboxSelected = false;

                    this.timeDifference = Sandata.Utility.GetNumberOfHours;

                    this.approvedSelectedList = [];
                    this.approvedSelectedList_UpdateId = [];
                    this.selectedHours = 0;

                    /********************
                    Approved Visit Button
                    ********************/
                    this.approvedHoursSelected = 0;
                    this.filterClass = "span4";
                    this.filterClassWithRightMargin = this.filterClass;

                    //this.labels = this.VisitsService.getContent().labels;
                    this.setupModals();
                    this.setupPagination();
                    this.setupFiltersSection();
                    this.initializePayors();
                    this.initializeEmployees();
                    this.initializeClients();
                    this.initializePayPeriod();

                    //this.loadOptions();
                    Sandata.Utility.AssignObjectsInArrayToScopeObject(this, this.VisitsService.getContent().filters, "title", "Filter_Area", true);
                    Sandata.Utility.AssignToScope(this, this.Filter_Area.FILTER_VISIT_CONTROLS_TAB_SECTION.base_components);
                    Sandata.Utility.RemoveAnObjectFromScope(this, "Filter_Area");

                    //Assign Filter Buttons To Scope
                    Sandata.Utility.AssignToScope(this, this.VisitsService.getContent().filter_controls);

                    //Date Options
                    this.dateOptions = {
                        open: function () {
                            var dateViewCalendar = this.dateView.calendar;
                            if (dateViewCalendar) {
                                dateViewCalendar.element.width(200);
                            }
                        }
                    };

                    this.getVisitFilters();
                    this.getPreviousMessages();
                    this.getPayor();

                    //this.watches();
                    //this.watchCheckboxes();
                    $scope.Visits = this;
                }
                Init.prototype.watches = function () {
                    var that = this;
                    this.$scope.$watch("sidePanel.show", function (value) {
                        if (value == true) {
                            that.filterClass = "span3";
                            that.filterClassWithRightMargin = that.filterClass + "  margin-right-25";
                        } else {
                            that.filterClass = "span4";
                            that.filterClassWithRightMargin = that.filterClass;
                        }
                    });
                };

                Init.prototype.getVisitFilters = function () {
                    this.ignoreVisitsModel = {};
                    this.incompleteVisitModel = {};
                    this.visitApprovalReadyModel = {};
                    this.approvedVisitModel = {};

                    if (this.$routeParams.ignore_visits) {
                        this.ignoreVisitsModel.selected = this.$routeParams.ignore_visits;
                    } else if (this.SharedDataService.getVisitPageFilter("ignoreVisitsModel")) {
                        //console.log("Ignore Visit:",this.SharedDataService.getVisitPageFilter("ignoreVisitsModel"))
                        this.ignoreVisitsModel.selected = this.SharedDataService.getVisitPageFilter("ignoreVisitsModel");
                    } else {
                        this.ignoreVisitsModel.selected = this.VISIT_FILTER_TAB_SHOW_IGNORED_CHECKBOX.checked;
                    }

                    if (this.$routeParams.incomplete_visit) {
                        this.incompleteVisitModel.selected = this.$routeParams.incomplete_visit;
                    } else if (this.SharedDataService.getVisitPageFilter("incompleteVisitModel")) {
                        //console.log("Incomplete Visit:",this.SharedDataService.getVisitPageFilter("incompleteVisitModel"))
                        this.incompleteVisitModel.selected = this.SharedDataService.getVisitPageFilter("incompleteVisitModel");
                    } else {
                        this.incompleteVisitModel.selected = this.VISIT_FILTER_TAB_INCOMPLETE_VISIT_CHECKBOX.checked;
                    }

                    if (this.$routeParams.visit_approval_ready) {
                        this.visitApprovalReadyModel.selected = this.$routeParams.visit_approval_ready;
                    } else if (this.SharedDataService.getVisitPageFilter("visitApprovalReadyModel")) {
                        //console.log("Approval Ready Visit:",this.SharedDataService.getVisitPageFilter("visitApprovalReadyModel"))
                        this.visitApprovalReadyModel.selected = this.SharedDataService.getVisitPageFilter("visitApprovalReadyModel");
                    } else {
                        this.visitApprovalReadyModel.selected = this.VISIT_FILTER_TAB_READY_VISIT_CHECKBOX.checked;
                    }

                    if (this.$routeParams.approved_visits) {
                        this.approvedVisitModel.selected = this.$routeParams.approved_visits;
                    } else if (this.SharedDataService.getVisitPageFilter("approvedVisitModel")) {
                        //console.log("Approved Visit:",this.SharedDataService.getVisitPageFilter("approvedVisitModel"))
                        this.approvedVisitModel.selected = this.SharedDataService.getVisitPageFilter("approvedVisitModel");
                    } else {
                        this.approvedVisitModel.selected = this.VISIT_FILTER_TAB_APPROVED_VISIT_CHECKBOX.checked;
                    }

                    this.reasons = this.reasons;

                    if (this.$routeParams.from_date && this.$routeParams.to_date) {
                        var match = /\-/gi;
                        var fromDateModel = decodeURIComponent(this.$routeParams.from_date);
                        this.fromDateModel = fromDateModel.replace(match, "/");
                        var toDateModel = decodeURIComponent(this.$routeParams.to_date);
                        this.toDateModel = toDateModel.replace(match, "/");
                        this.resetPayPeriod();
                    } else if (typeof this.pay_periodModel.start_date != "undefined" && typeof this.pay_periodModel.end_date != "undefined") {
                        this.updateDatePeriod();
                    } else if (this.SharedDataService.getVisitPageFilter("fromDateModel") && this.SharedDataService.getVisitPageFilter("toDateModel")) {
                        this.fromDateModel = this.SharedDataService.getVisitPageFilter("fromDateModel");
                        this.toDateModel = this.SharedDataService.getVisitPageFilter("toDateModel");
                        //this.resetPayPeriod();
                    } else {
                        //console.log("UnCaught: ",this.pay_periodModel);
                        var match = /\-/gi;
                        var tempFromDate = this.VISIT_FILTER_TAB_FROM_DATE_CONTROL.date_value.replace(match, "/");
                        var tempToDate = this.VISIT_FILTER_TAB_TO_DATE_CONTROL.date_value.replace(match, "/");

                        this.fromDateModel = $.datepicker.formatDate('mm/dd/yy', new Date(tempFromDate));
                        this.toDateModel = $.datepicker.formatDate('mm/dd/yy', new Date(tempToDate));
                        this.resetPayPeriod();
                    }
                };
                return Init;
            })();
            Visits.Init = Init;
        })(Controller.Visits || (Controller.Visits = {}));
        var Visits = Controller.Visits;
    })(Sandata.Controller || (Sandata.Controller = {}));
    var Controller = Sandata.Controller;
})(Sandata || (Sandata = {}));
var Sandata;
(function (Sandata) {
    (function (Controller) {
        /// <reference path="../services/Sandata.Service.Visits.ts" />
        (function (VisitForm) {
            /*****************
            * Visits
            *********************/
            var Init = (function () {
                function Init($scope, $timeout, $location, $routeParams, SharedDataService, VisitsService, REGEXP_FORMATS) {
                    this.$inject = ['$scope', '$timeout', '$location', '$routeParams', 'SharedDataService', 'VisitsService', 'REGEXP_FORMATS'];
                    this.setupMessageCenter = function () {
                        if (this.VisitModal.errorMessages && this.VisitModal.errorMessages.length > 0) {
                            Sandata.Utility.Setup_ShowHideSidePanel($scope, "sidePanel", 1, "FilterFocus", "a#");
                            if (this.VisitModal.errorMessages && this.VisitModal.errorMessages.length > 0) {
                                for (var i = 0, errorMessagesLength = this.VisitModal.errorMessages.length; i < errorMessagesLength; i++) {
                                    Sandata.Utility.DisplayMessageCenter({
                                        errorCheck: { status: "none" },
                                        $timeout: this.$timeout,
                                        scope: this.$scope,
                                        type: "error",
                                        text: this.VisitModal.errorMessages[i],
                                        header: "Error"
                                    });
                                }
                            }
                        } else {
                            Sandata.Utility.Setup_ShowHideSidePanel($scope, "sidePanel", $routeParams.sidepanel, "FilterFocus", "a#");
                        }
                    };
                    this.submitAction = function (elm) {
                        var validFormEntry = Sandata.Utility.ValidateForm(this.$scope, elm, true);
                        var inTime = this.VisitModal.VisitADJIn;
                        var outTime = this.VisitModal.VisitADJOut;
                        var overnight = Sandata.Utility.CrossesMidnight({ start: this.VisitModal.VisitADJIn, end: this.VisitModal.VisitADJOut, regex: this.time_regex });

                        if (validFormEntry.length == 0 && ((!overnight && !this.VisitModal.overnight) || (overnight && this.VisitModal.overnight))) {
                            if (this.$scope.messageCenter) {
                                this.$scope.messageCenter.length = 0;
                            }
                            this.errors.length = 0;
                            this.setupMessageCenter();
                            switch (this.VisitModal.mode) {
                                case "edit":
                                    this.editVisit();
                                    break;
                                case "add":
                                    this.addVisit();
                                    break;
                                default:
                                    this.closeVisitModal();
                                    break;
                            }
                        } else {
                            if (this.$scope.messageCenter) {
                                this.$scope.messageCenter.length = 0;
                            }
                            this.errors.length = 0;

                            if (this.VisitModal.errorMessages && this.VisitModal.errorMessages.length > 0) {
                                for (var i = 0, errorMessagesLength = this.VisitModal.errorMessages.length; i < errorMessagesLength; i++) {
                                    validFormEntry.push({ text: this.VisitModal.errorMessages[i], header: "Error" });
                                }
                            }

                            if (overnight && !this.VisitModal.overnight) {
                                validFormEntry.push({ text: "This visit crosses midnight. Please check overnight visit checkbox.", header: "Overnight" });
                            } else if (!overnight && this.VisitModal.overnight) {
                                validFormEntry.push({ text: "This visit does not cross midnight. Please uncheck overnight visit checkbox.", header: "Overnight" });
                            } else {
                            }

                            for (var i = 0, errorLength = validFormEntry.length; i < errorLength; i++) {
                                this.errors.push(validFormEntry[i].header);
                                Sandata.Utility.DisplayMessageCenter({
                                    errorCheck: { status: "none" },
                                    $timeout: this.$timeout,
                                    scope: this.$scope,
                                    type: "error",
                                    text: validFormEntry[i].text,
                                    header: validFormEntry[i].header
                                });
                            }
                        }
                    };
                    this.closeVisitModal = function () {
                        var tmp_url = "/visits#" + this.SharedDataService.getVisitPageScrollPosition();
                        this.$location.url(tmp_url);
                    };
                    this.initializeForm = function () {
                        this.VisitModal = {};
                        if (this.SharedDataService.getVisitForm()) {
                            if (this.$routeParams.id && (this.$routeParams.id == this.SharedDataService.getVisitForm().edit_id)) {
                                this.VisitModal = this.SharedDataService.getVisitForm();
                            } else if (this.SharedDataService.getVisitForm().mode == "add") {
                                this.VisitModal = this.SharedDataService.getVisitForm();
                            } else {
                                this.$location.path("/visits");
                            }
                        } else {
                            this.$location.path("/visits");
                        }
                        this.VisitModal = this.SharedDataService.getVisitForm();
                    };
                    this.initializePayors = function () {
                        var visitFilters = this.VisitsService.getContent();

                        // If Payors already selected in another page, then set select field to that value
                        if (this.$routeParams.payor) {
                            //Sandata.Utility.SetDefaultSelectValue(this,visitFilters.payors.base_components.PAYOR_NAMES,"payors","payorModel", this.$routeParams.payor,true);
                            Sandata.Utility.AssignToScopeAndSetModel({
                                $scope: this,
                                data: visitFilters.payors.base_components.PAYOR_NAMES,
                                arObj: "option_components",
                                variable: "payors",
                                model: "payorModel",
                                index: this.$routeParams.payor,
                                byvalue: true });
                        } else if (this.SharedDataService.getPayor() != null) {
                            //Sandata.Utility.SetDefaultSelectValue(this,visitFilters.payors.base_components.PAYOR_NAMES,"payors","payorModel",this.SharedDataService.getPayor(),true);
                            //Comment
                            if (typeof this.SharedDataService.getPayor().value != "undefined") {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: visitFilters.payors.base_components.PAYOR_NAMES,
                                    arObj: "option_components",
                                    variable: "payors",
                                    model: "payorModel",
                                    index: this.SharedDataService.getPayor().value,
                                    byvalue: true });
                            } else {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: visitFilters.payors.base_components.PAYOR_NAMES,
                                    arObj: "option_components",
                                    variable: "payors",
                                    model: "payorModel",
                                    index: this.SharedDataService.getPayor(),
                                    byvalue: true });
                            }
                        } else {
                            //Sandata.Utility.SetDefaultSelectValue(this,visitFilters.payors.base_components.PAYOR_NAMES,"payors","payorModel",this.$routeParams.payor,true);
                            Sandata.Utility.AssignToScopeAndSetModel({
                                $scope: this,
                                data: visitFilters.payors.base_components.PAYOR_NAMES,
                                arObj: "option_components",
                                variable: "payors",
                                model: "payorModel",
                                index: 0 });
                        }
                    };
                    this.initializeEmployees = function () {
                        var visitFilters = this.VisitsService.getContent();

                        // If Employee already selected in another page, then set select field to that value
                        if (this.$routeParams.employee) {
                            Sandata.Utility.AssignToScopeAndSetModel({
                                $scope: this,
                                data: visitFilters.employees,
                                arObj: "option_components",
                                variable: "employees",
                                model: "employeeModel",
                                index: this.$routeParams.employee,
                                byvalue: true });
                        } else if (this.SharedDataService.getEmployee() != null) {
                            if (typeof this.SharedDataService.getEmployee().value != "undefined") {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: visitFilters.employees,
                                    arObj: "option_components",
                                    variable: "employees",
                                    model: "employeeModel",
                                    index: this.SharedDataService.getEmployee().value,
                                    byvalue: true });
                            } else {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: visitFilters.employees,
                                    arObj: "option_components",
                                    variable: "employees",
                                    model: "employeeModel",
                                    index: this.SharedDataService.getEmployee(),
                                    byvalue: true });
                            }
                        } else {
                            Sandata.Utility.AssignToScopeAndSetModel({
                                $scope: this,
                                data: visitFilters.employees,
                                arObj: "option_components",
                                variable: "employees",
                                model: "employeeModel",
                                index: 0 });
                        }
                    };
                    this.initializeClients = function () {
                        var visitFilters = this.VisitsService.getContent();

                        // If Client already selected in another page, then set select field to that value
                        if (this.$routeParams.client) {
                            Sandata.Utility.AssignToScopeAndSetModel({
                                $scope: this,
                                data: visitFilters.clients,
                                arObj: "option_components",
                                variable: "clients",
                                model: "clientModel",
                                index: this.$routeParams.client,
                                byvalue: true });
                        }
                        if (this.SharedDataService.getClient() != null) {
                            if (typeof this.SharedDataService.getClient().value != "undefined") {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: visitFilters.clients,
                                    arObj: "option_components",
                                    variable: "clients",
                                    model: "clientModel",
                                    index: this.SharedDataService.getClient().value,
                                    byvalue: true });
                            } else {
                                Sandata.Utility.AssignToScopeAndSetModel({
                                    $scope: this,
                                    data: visitFilters.clients,
                                    arObj: "option_components",
                                    variable: "clients",
                                    model: "clientModel",
                                    index: this.SharedDataService.getClient(),
                                    byvalue: true });
                            }
                        } else {
                            Sandata.Utility.AssignToScopeAndSetModel({
                                $scope: this,
                                data: visitFilters.clients,
                                arObj: "option_components",
                                variable: "clients",
                                model: "clientModel",
                                index: 0 });
                        }
                    };
                    this.validateModal = function () {
                        return $("#visitModalForm").parsley('validate');
                    };
                    this.addVisit = function () {
                        var DateIn = $.datepicker.formatDate('M d, yy', new Date(this.VisitModal.VisitDate));
                        var timeMatch = /(([01][0-9])|(2[0-3])):[0-5][0-9]/gi;
                        var timeFormat = new RegExp(this.time_regex);

                        if (this.time_format.display_value === "true") {
                            var militaryHoursIn = this.VisitModal.VisitADJIn;
                            var militaryHoursOut = this.VisitModal.VisitADJOut;
                        } else {
                            var TimeIn = DateIn + " " + timeFormat.exec(this.VisitModal.VisitADJIn)[1] + ":" + timeFormat.exec(this.VisitModal.VisitADJIn)[2] + " " + timeFormat.exec(this.VisitModal.VisitADJIn)[4];
                            var TimeOut = DateIn + " " + timeFormat.exec(this.VisitModal.VisitADJOut)[1] + ":" + timeFormat.exec(this.VisitModal.VisitADJOut)[2] + " " + timeFormat.exec(this.VisitModal.VisitADJOut)[4];

                            var militaryHoursIn = new Date(TimeIn).toTimeString().match(timeMatch);
                            var militaryHoursOut = new Date(TimeOut).toTimeString().match(timeMatch);
                        }

                        var employeeSelect = (this.VisitModal.addVisitEmployee.value) ? this.VisitModal.addVisitEmployee.value : this.VisitModal.addVisitEmployee;
                        var clientSelect = (this.VisitModal.addVisitClient.value) ? this.VisitModal.addVisitClient.value : this.VisitModal.addVisitClient;
                        var serviceSelect = (this.VisitModal.addVisitService.value) ? this.VisitModal.addVisitService.value : this.VisitModal.addVisitService;

                        var tmpVisit = {
                            "employeeId": employeeSelect,
                            "clientId": clientSelect,
                            "serviceDate": $.datepicker.formatDate('yy-mm-dd', new Date(this.VisitModal.VisitDate)),
                            "startTime": militaryHoursIn,
                            "endTime": militaryHoursOut,
                            "reasonCode": this.VisitModal.addVisitReasonsModel.value.toString(),
                            "serviceId": serviceSelect
                        };

                        if (this.VisitModal.VisitNotes != "") {
                            tmpVisit.visitNotes = this.VisitModal.VisitNotes;
                        }

                        if (this.changeReasonsModel != "") {
                            tmpVisit.changeReason = this.VisitModal.changeReasonsModel;
                        }

                        //if(this.validateModal()){
                        this.toggle_diabledSubmits();
                        var that = this;
                        this.VisitsService.addVisit(tmpVisit).then(function (data) {
                            //console.log("Add Visit Response : ", data);
                            /*if(data.status == "FAILED"){
                            var actions = [$scope.clearSelectedVisits,$scope.updateGrid,$scope.closeVisitModal,$scope.filterSection.msgCenterTrigger];
                            }else{
                            var actions = [$scope.clearSelectedVisits,$scope.updateGrid,$scope.closeVisitModal,$scope.filterSection.msgCenterTrigger];
                            }*/
                            var type = "added";
                            var text = "Visit has been added.";

                            Sandata.Utility.DisplayMessageCenter({
                                errorCheck: data,
                                scope: that.$scope,
                                $timeout: that.$timeout,
                                type: type,
                                text: data.message_detail,
                                header: data.message_summary
                            });
                            that.SharedDataService.pushVisitPageMessages(that.$scope.messageCenter[that.$scope.messageCenter.length - 1]);

                            //that.clearSelectedVisits();
                            var tmp_url = "/visits?sidepanel=1&showmessages=1#" + that.SharedDataService.getVisitPageScrollPosition();
                            that.$location.url(tmp_url);
                        });
                        //}
                    };
                    this.editVisit = function () {
                        var DateIn = $.datepicker.formatDate('M d, yy', new Date(this.VisitModal.VisitDate));
                        var timeMatch = /(([01][0-9])|(2[0-3])):[0-5][0-9]/gi;
                        var timeFormat = new RegExp(this.time_regex);

                        if (this.time_format.display_value === "true") {
                            var militaryHoursIn = this.VisitModal.VisitADJIn;
                            var militaryHoursOut = this.VisitModal.VisitADJOut;
                        } else {
                            var TimeIn = DateIn + " " + timeFormat.exec(this.VisitModal.VisitADJIn)[1] + ":" + timeFormat.exec(this.VisitModal.VisitADJIn)[2] + " " + timeFormat.exec(this.VisitModal.VisitADJIn)[4];
                            var TimeOut = DateIn + " " + timeFormat.exec(this.VisitModal.VisitADJOut)[1] + ":" + timeFormat.exec(this.VisitModal.VisitADJOut)[2] + " " + timeFormat.exec(this.VisitModal.VisitADJOut)[4];

                            var militaryHoursIn = new Date(TimeIn).toTimeString().match(timeMatch);
                            var militaryHoursOut = new Date(TimeOut).toTimeString().match(timeMatch);
                        }

                        var employeeSelect = (this.VisitModal.addVisitEmployee.value) ? this.VisitModal.addVisitEmployee.value : this.VisitModal.addVisitEmployee;
                        var clientSelect = (this.VisitModal.addVisitClient.value) ? this.VisitModal.addVisitClient.value : this.VisitModal.addVisitClient;
                        var serviceSelect = (this.VisitModal.addVisitService.value) ? this.VisitModal.addVisitService.value : this.VisitModal.addVisitService;

                        var tmpVisit = {
                            "id": this.VisitModal.edit_id,
                            "updateId": this.VisitModal.update_id,
                            "employeeId": employeeSelect,
                            "clientId": clientSelect,
                            "serviceDate": $.datepicker.formatDate('yy-mm-dd', new Date(this.VisitModal.VisitDate)),
                            "startTime": militaryHoursIn,
                            "endTime": militaryHoursOut,
                            "reasonCode": this.VisitModal.addVisitReasonsModel.value.toString(),
                            "serviceId": serviceSelect
                        };

                        if (this.VisitModal.VisitNotes != "") {
                            tmpVisit.visitNotes = this.VisitModal.VisitNotes;
                        }

                        if (this.changeReasonsModel != "") {
                            tmpVisit.changeReason = this.VisitModal.changeReasonsModel;
                        }

                        //if(this.validateModal()){
                        this.toggle_diabledSubmits();
                        var that = this;
                        this.VisitsService.editVisit(tmpVisit).then(function (data) {
                            //console.log("Edit Visit Response : ", data);
                            /*if(data.status == "FAILED"){
                            var actions = [$scope.clearSelectedVisits,$scope.updateGrid,$scope.closeVisitModal,$scope.filterSection.msgCenterTrigger];
                            }else{
                            var actions = [$scope.clearSelectedVisits,$scope.updateGrid,$scope.closeVisitModal,$scope.filterSection.msgCenterTrigger];
                            }*/
                            var type = "info";
                            var text = "Visit has been updated.";
                            Sandata.Utility.DisplayMessageCenter({
                                errorCheck: data,
                                $timeout: that.$timeout,
                                scope: that.$scope,
                                type: type,
                                text: data.message_detail,
                                header: data.message_summary
                            });
                            that.SharedDataService.pushVisitPageMessages(that.$scope.messageCenter[that.$scope.messageCenter.length - 1]);

                            //that.clearSelectedVisits();
                            var tmp_url = "/visits?sidepanel=1&showmessages=1#" + that.SharedDataService.getVisitPageScrollPosition();
                            that.$location.url(tmp_url);
                        });
                        //}
                    };
                    this.$location = $location;
                    this.$routeParams = $routeParams;
                    this.SharedDataService = SharedDataService;
                    this.VisitsService = VisitsService;
                    this.$scope = $scope;
                    this.REGEXP_FORMATS = REGEXP_FORMATS;
                    this.$timeout = $timeout;
                    this.title = "Visits";
                    this.errors = [];

                    Sandata.Utility.ReloadCheck();

                    /*************************
                    Diable Form Elements Setup
                    **************************/
                    Sandata.Utility.DisableElement(this, "diabledSubmits");
                    this.formatTime = Sandata.Utility.FormatTime;
                    this.timeDifference = Sandata.Utility.GetNumberOfHours;
                    this.dateOptions = {
                        open: function () {
                            var dateViewCalendar = this.dateView.calendar;
                            if (dateViewCalendar) {
                                dateViewCalendar.element.width(200);
                            }
                        }
                    };

                    this.MessageCenter = {
                        expanded: false,
                        disabled: false,
                        controls: "widgets-area",
                        label: "Click to Show Add/Edit Messages Center",
                        name: "Show or hide Add/Edit Messages Center",
                        title: "Show or hide Add/Edit Messages Center"
                    };

                    this.time_format = this.VisitsService.getContent().time_format;

                    //console.log("Time Format for Add Edit Visits: ",this.time_format);
                    if (this.time_format.display_value === "true") {
                        this.time_regex = this.REGEXP_FORMATS.military_time_regex_pattern;
                        this.time_format_message_adjout = "Please enter the 'Adj Out' time in the correct format: hh:mm";
                        this.time_format_message_adjin = "Please enter the 'Adj In' time in the correct format: hh:mm";
                    } else {
                        this.time_regex = this.REGEXP_FORMATS.time_regex_pattern;
                        this.time_format_message_adjout = "Please enter the 'Adj Out' time in the correct format: hh:mm AM or PM";
                        this.time_format_message_adjin = "Please enter the 'Adj In' time in the correct format: hh:mm AM or PM";
                    }

                    this.initializePayors();
                    this.initializeEmployees();
                    this.initializeClients();
                    this.initializeForm();
                    this.setupMessageCenter();

                    $scope.Visits = this;
                }
                Init.prototype.toggleCheckboxModel = function (property) {
                    this.VisitModal[property] = !this.VisitModal[property];
                };
                return Init;
            })();
            VisitForm.Init = Init;
        })(Controller.VisitForm || (Controller.VisitForm = {}));
        var VisitForm = Controller.VisitForm;
    })(Sandata.Controller || (Sandata.Controller = {}));
    var Controller = Sandata.Controller;
})(Sandata || (Sandata = {}));
