/**
 * This module is used to simulate backend server for this demo application.
 */
angular.module('content-mocks', ['ngMockE2E'])
  .run(function ($httpBackend) {

      var authorized = false;

      //Locally Stored JSON...for Mocking DB Queries
      var visits = visitsDB();
      var realVISTS = realVisitsDB();


      $httpBackend.whenPOST('auth/login').respond(function (method, url, data) {
          authorized = true;
          return [
                    200,
                    {
                        menu: []
                    }
                  ];
      });
      $httpBackend.whenPOST('auth/logout').respond(function (method, url, data) {
          authorized = false;
          return [200];
      });

      $httpBackend.whenPOST('data/protected').respond(function (method, url, data) {
          //return authorized ? [200,'This is confidential [' + data + '].'] : [401];
          //return authorized ? [200, authorized] : [401];
          return authorized ? [200, { authorized: authorized,
              token: 'sdsa4d56sa4dsa4d564sadsa4d56sa46d5sa4d6'
          }] : [401, { authorized: authorized}];
      });

      $httpBackend.whenPOST('data/initalize').respond(function (method, url, data) {
          return [200, {
              init: {
                  menus: [
                                        {
                                            type: "main-menu",
                                            name: "main-menu",
                                            template: "includes/main-menu.html",
                                            links: [
                                                    {
                                                        "class": "main-menu-link home-link",
                                                        url: "#/",
                                                        title: "Sandata",
                                                        alt: "Sandata Home Page",
                                                        type: "image"
                                                    },
                                                    {
                                                        "class": "main-menu-link",
                                                        url: "#",
                                                        title: "Dashboard",
                                                        alt: "Dashboard",
                                                        type: "link"
                                                    },
                                                    {
                                                        "class": "main-menu-link",
                                                        url: "#",
                                                        title: "Scheduling",
                                                        alt: "Scheduling",
                                                        type: "link"
                                                    },
                                                    {
                                                        "class": "main-menu-link",
                                                        url: "#",
                                                        title: "Visit Maintenance",
                                                        alt: "Visit Maintenance",
                                                        type: "link"
                                                    },
                                                    {
                                                        "class": "main-menu-link",
                                                        url: "#/reporting",
                                                        title: "Reporting",
                                                        alt: "Reporting",
                                                        type: "link"
                                                    },
                                                    {
                                                        "class": "main-menu-link",
                                                        url: "#/reportingsecond",
                                                        title: "Clinic",
                                                        alt: "Clinic",
                                                        type: "link"
                                                    }
                                            ]

                                        }
                  ]
              }
          }];
      });

      /** 
      * Returns Employee, Client and Payor info       
      */
      $httpBackend.whenPOST('service/account/accountdata').respond(function (method, url, data) {
          var response = [
                200,
                {
                    "code": 200,
                    "token": "sdsa4d56sa4dsa4d564sadsa4d56sa46d5sa4d6",
                    "error_message": null,
                    "data": {
                        "status_code": "200",
                        "rolling_token": "d1sa5da156da1d3sa1d32sa1d23sa1dsa13d1sa3d1a",
                        "name": "Mike Hansen",
                        "sections": {
                            "accountdata_parameters": {
                                "show": true,
                                "title": "Account Data",
                                "base_components": {
                                    "client_names": {
                                        "option_components": [
                                   {
                                       "value": "H3444586",
                                       "label": "Edward Jones",
                                       "group": "H3444586",
                                       "show": true,
                                       "actions": []
                                   },
                                   {
                                       "value": "H3444586",
                                       "label": "John Appleseed",
                                       "group": "H3444586",
                                       "show": true,
                                       "actions": []
                                   }
                                ],
                                        "select_label": "Choose a Client",
                                        "required": false,
                                        "disabled": false,
                                        "error_message": "Select a Client",
                                        "content_type": "SelectComponent",
                                        "name": "clientNames",
                                        "show": true
                                    },
                                    "payor_names": {
                                        "option_components": [
                                   {
                                       "value": 1,
                                       "label": "Illinois DHS",
                                       "group": "H3444586",
                                       "show": true,
                                       "actions": []
                                   },
                                   {
                                       "value": 2,
                                       "label": "Aetna",
                                       "group": "H3444585",
                                       "show": true,
                                       "actions": []
                                   }
                                ],
                                        "select_label": "Choose a Payor",
                                        "required": false,
                                        "disabled": false,
                                        "error_message": "Select a Payor",
                                        "content_type": "SelectComponent",
                                        "name": "payorNames",
                                        "show": true
                                    },
                                    "employee_names": {
                                        "option_components": [
                                   {
                                       "value": "986647846",
                                       "label": "QUI, EMMA",
                                       "group": "986647846",
                                       "show": false,
                                       "actions": [

                                      ]
                                   }
                                ],
                                        "select_label": "Choose an Employee",
                                        "required": false,
                                        "disabled": false,
                                        "error_message": "Select an Employee",
                                        "content_type": "SelectComponent",
                                        "name": "employeeNames",
                                        "show": false
                                    }
                                }
                            }
                        }
                    }
                }
          ];

          return response;
      });

      $httpBackend.whenGET('cxf/up/service/account/accountdata').respond(function (method, url, data) {
          var data = accountData();
          var response = [
                200, accountData()];

          return response;
      });
      /* service/account/accountdata */
      $httpBackend.whenGET('cxf/up/service/payor').respond(function (method, url, data) {
          var data = accountData();
          var response = [
                200, {
                      "status": "SUCCESS",
                      "token": "sdsa4d56sa4dsa4d564sadsa4d56sa46d5sa4d6",
                      "error_message": null,
                      "data": [
                        {
                          "key": "Illinois DHS",
                          "value": "Illinois DHS"
                        }
                      ]
                    }];

          return response;
      });

      /**************************************/
      $httpBackend.whenPOST('data/reportinglayout').respond(function (method, url, data) {
          return [200, { authorized: authorized,
              token: 'sdsa4d56sa4dsa4d564sadsa4d56sa46d5sa4d6',
              layout: {
                  type: "form",
                  include: "includes/forms/reporting.html",
                  "class": "form-container",
                  templates: [{ url: "includes/sections/fieldsets.html",
                      "class": "form-inline first",
                      title: "Select Report",
                      rows: [
                                                                                {
                                                                                    "class": "row-fluid",
                                                                                    content: [
                                                                                                { "content_type": "label", "for": "reporttype", "class": "span4", "data": "Report Type" },
                                                                                                { "content_type": "label", "for": "reportname", "class": "span4", "data": "Report Name" }
                                                                                            ]
                                                                                },
                                                                                {
                                                                                    "class": "row-fluid",
                                                                                    content: [
                                                                                                { "content_type": "select", "name": "reporttype", "class": "span4", "data": [
                                                                                                                                                                                    { value: "dailyreports", label: "Daily Reports" }
                                                                                                                                                                                  ]
                                                                                                },
                                                                                                { "content_type": "select", "name": "reportname", "class": "span4", "data": [
                                                                                                                                                                                    { value: "clients", label: "Clients" }
                                                                                                                                                                                  ]
                                                                                                }
                                                                                            ]
                                                                                }
                                                                            ]
                  },
                                                                { url: "includes/sections/fieldsets.html",
                                                                    "class": "form-inline",
                                                                    title: "Select Parameters",
                                                                    rows: [
                                                                                {
                                                                                    "class": "row-fluid",
                                                                                    content: [
                                                                                                { "content_type": "label", "for": "contact", "class": "span4", "data": "Contact" },
                                                                                                { "content_type": "label", "for": "program", "class": "span4", "data": "Program" },
                                                                                                { "content_type": "label", "for": "service", "class": "span4", "data": "Service" }
                                                                                            ]
                                                                                },
                                                                                {
                                                                                    "class": "row-fluid",
                                                                                    content: [
                                                                                                { "content_type": "select", "name": "contact", "class": "span4", "data": [
                                                                                                                                                                                { value: "all", label: "All" }
                                                                                                                                                                              ]
                                                                                                },
                                                                                                { "content_type": "select", "name": "program", "class": "span4", "data": [
                                                                                                                                                                                { value: "mulitple", label: "Multiple" }
                                                                                                                                                                              ]
                                                                                                },
                                                                                                { "content_type": "select", "name": "service", "class": "span4", "data": [
                                                                                                                                                                                { value: "all", label: "All" }
                                                                                                                                                                              ]
                                                                                                }
                                                                                            ]
                                                                                },
                                                                                {
                                                                                    "class": "row-fluid",
                                                                                    content: [
                                                                                                { "content_type": "label", "for": "client", "class": "span4", "data": "Client" },
                                                                                                { "content_type": "label", "for": "clientar", "class": "span4", "data": "Client AR#" },
                                                                                                { "content_type": "label", "for": "supervisor", "class": "span4", "data": "Supervisor" }
                                                                                            ]
                                                                                },
                                                                                {
                                                                                    "class": "row-fluid",
                                                                                    content: [
                                                                                                { "content_type": "select", "name": "client", "class": "span4", "data": [
                                                                                                                                                                                { value: "all", label: "All" }
                                                                                                                                                                              ]
                                                                                                },
                                                                                                { "content_type": "select", "name": "clientar", "class": "span4", "data": [
                                                                                                                                                                                { value: "all", label: "All" }
                                                                                                                                                                              ]
                                                                                                },
                                                                                                { "content_type": "select", "name": "supervisor", "class": "span4", "data": [
                                                                                                                                                                                { value: "all", label: "All" }
                                                                                                                                                                              ]
                                                                                                }
                                                                                            ]
                                                                                }
                                                                            ]
                                                                },
                                                                { url: "includes/sections/fieldsets.html",
                                                                    "class": "form-inline last",
                                                                    title: "date / time",
                                                                    rows: [
                                                                                {
                                                                                    "class": "row-fluid",
                                                                                    content: [
                                                                                                { "content_type": "label", "for": "fromdate", "class": "span6", "data": "From" },
                                                                                                { "content_type": "label", "for": "todate", "class": "span6", "data": "To" }
                                                                                            ]
                                                                                },
                                                                                {
                                                                                    "class": "row-fluid",
                                                                                    content: [
                                                                                                { "content_type": "datepicker", "name": "fromdate", "class": "datepicker span3", "data": "" },
                                                                                                { "content_type": "timepicker", "name": "fromtime", "class": "timepicker span3", "data": "" },
                                                                                                { "content_type": "datepicker", "name": "todate", "class": "datepicker span3", "data": "" },
                                                                                                { "content_type": "timepicker", "name": "totime", "class": "timepicker span3", "data": "" }
                                                                                            ]
                                                                                }
                                                                            ]
                                                                }]

              }
          }
                           ];
      });

      $httpBackend.whenPOST('data/reportinglayoutdynamicfields').respond(function (method, url, data) {
          return [200, { authorized: authorized,
              token: 'sdsa4d56sa4dsa4d564sadsa4d56sa46d5sa4d6',
              layout: {
                  type: "form",
                  include: "includes/forms/reporting.html",
                  "class": "form-container",
                  templates: [{ url: "includes/sections/fieldsets.html",
                      "class": "form-inline first",
                      title: "Select Report",
                      rows: [
                                                    {
                                                        "class": "row-fluid",
                                                        content: [
                                                                    { "content_type": "label", "for": "reporttype", "class": "span4", "data": "Type" },
                                                                    { "content_type": "label", "for": "reportname", "class": "span4", "data": "Name" }
                                                                ]
                                                    },
                                                    {
                                                        "class": "row-fluid",
                                                        content: [
                                                                    { "content_type": "select", "name": "reporttype", "class": "span4", show: true, "required": true, "trigger": "change", "notblank": true, "valuetype": "alphanum", "errormsg": "Select A Type", "bindingfrom": "reporttype", "oldBindingTo": [{ type: "select", name: "reportname" }, { type: 'sandata-datepicker', name: 'todate' }, { type: 'sandata-timepicker', name: 'timepicker'}], "data": [
                                                                                                                                                    { value: "daily", label: "Daily", reportname: [
                                                                                                                                                                                                        { label: "Second Option A", value: "secondOption1a",
                                                                                                                                                                                                            contact: {
                                                                                                                                                                                                                show: true,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Contact Option A 1", value: "1" },
                                                                                                                                                                                                                                { label: "Contact Option A 2", value: "2" },
                                                                                                                                                                                                                                { label: "Contact Option A 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            program: {
                                                                                                                                                                                                                show: true,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Program Option A 1", value: "1" },
                                                                                                                                                                                                                                { label: "Program Option A 2", value: "2" },
                                                                                                                                                                                                                                { label: "Program Option A 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            service: {
                                                                                                                                                                                                                show: true,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Service Option A 1", value: "1" },
                                                                                                                                                                                                                                { label: "Service Option A 2", value: "2" },
                                                                                                                                                                                                                                { label: "Service Option A 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            client: {
                                                                                                                                                                                                                show: false,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Client Option A 1", value: "1" },
                                                                                                                                                                                                                                { label: "Client Option A 2", value: "2" },
                                                                                                                                                                                                                                { label: "Client Option A 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            clientar: {
                                                                                                                                                                                                                show: false,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Client AR Option A 1", value: "1" },
                                                                                                                                                                                                                                { label: "Client AR Option A 2", value: "2" },
                                                                                                                                                                                                                                { label: "Client AR Option A 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            supervisor: {
                                                                                                                                                                                                                show: false,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Supervisor Option A 1", value: "1" },
                                                                                                                                                                                                                                { label: "Supervisor Option A 2", value: "2" },
                                                                                                                                                                                                                                { label: "Supervisor Option A 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            }
                                                                                                                                                                                                        },
                                                                                                                                                                                                        { label: "Second Option B", value: "secondOption1b",
                                                                                                                                                                                                            contact: {
                                                                                                                                                                                                                show: true,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Contact Option B 1", value: "4" },
                                                                                                                                                                                                                                { label: "Second Option B 2", value: "5" },
                                                                                                                                                                                                                                { label: "Contact Option B 3", value: "6" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            program: {
                                                                                                                                                                                                                show: true,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Program Option B 1", value: "1" },
                                                                                                                                                                                                                                { label: "Program Option B 2", value: "2" },
                                                                                                                                                                                                                                { label: "Program Option B 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            service: {
                                                                                                                                                                                                                show: true,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Service Option B 1", value: "1" },
                                                                                                                                                                                                                                { label: "Service Option B 2", value: "2" },
                                                                                                                                                                                                                                { label: "Service Option B 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            client: {
                                                                                                                                                                                                                show: false,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Client Option B 1", value: "1" },
                                                                                                                                                                                                                                { label: "Client Option B 2", value: "2" },
                                                                                                                                                                                                                                { label: "Client Option B 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            clientar: {
                                                                                                                                                                                                                show: false,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Client AR Option B 1", value: "1" },
                                                                                                                                                                                                                                { label: "Client AR Option B 2", value: "2" },
                                                                                                                                                                                                                                { label: "Client AR Option B 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            supervisor: {
                                                                                                                                                                                                                show: false,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Supervisor Option B 1", value: "1" },
                                                                                                                                                                                                                                { label: "Supervisor Option B 2", value: "2" },
                                                                                                                                                                                                                                { label: "Supervisor Option B 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            }

                                                                                                                                                                                                        }
                                                                                                                                                                                                    ],
                                                                                                                                                        todate: { show: false, disabled: true },
                                                                                                                                                        totime: { show: false, disabled: true }
                                                                                                                                                    },
                                                                                                                                                    { value: "monthly", label: "Monthly", reportname: [
                                                                                                                                                                                                            { label: "Second Option C", value: "secondOption1c",
                                                                                                                                                                                                                contact: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                    { label: "Contact Option C 1", value: "7" },
                                                                                                                                                                                                                                    { label: "Contact Option C 2", value: "8" },
                                                                                                                                                                                                                                    { label: "Contact Option C 3", value: "9" }
                                                                                                                                                                                                                                ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                program: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                    { label: "Program Option C 1", value: "1" },
                                                                                                                                                                                                                                    { label: "Program Option C 2", value: "2" },
                                                                                                                                                                                                                                    { label: "Program Option C 3", value: "3" }
                                                                                                                                                                                                                                ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                service: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                    { label: "Service Option C 1", value: "1" },
                                                                                                                                                                                                                                    { label: "Service Option C 2", value: "2" },
                                                                                                                                                                                                                                    { label: "Service Option C 3", value: "3" }
                                                                                                                                                                                                                                ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                client: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                { label: "Client Option C 1", value: "1" },
                                                                                                                                                                                                                                { label: "Client Option C 2", value: "2" },
                                                                                                                                                                                                                                { label: "Client Option C 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                clientar: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                { label: "Client AR Option C 1", value: "1" },
                                                                                                                                                                                                                                { label: "Client AR Option C 2", value: "2" },
                                                                                                                                                                                                                                { label: "Client AR Option C 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                supervisor: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                { label: "Supervisor Option C 1", value: "1" },
                                                                                                                                                                                                                                { label: "Supervisor Option C 2", value: "2" },
                                                                                                                                                                                                                                { label: "Supervisor Option C 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                                }

                                                                                                                                                                                                            },
                                                                                                                                                                                                            { label: "Second Option D", value: "secondOption1d",
                                                                                                                                                                                                                contact: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                    { label: "Contact Option D 1", value: "10" },
                                                                                                                                                                                                                                    { label: "Contact Option D 2", value: "11" },
                                                                                                                                                                                                                                    { label: "Contact Option D 3", value: "12" }
                                                                                                                                                                                                                                ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                program: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                    { label: "Program Option D 1", value: "10" },
                                                                                                                                                                                                                                    { label: "Program Option D 2", value: "11" },
                                                                                                                                                                                                                                    { label: "Program Option D 3", value: "12" }
                                                                                                                                                                                                                                ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                service: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                    { label: "Service Option D 1", value: "10" },
                                                                                                                                                                                                                                    { label: "Service Option D 2", value: "11" },
                                                                                                                                                                                                                                    { label: "Service Option D 3", value: "12" }
                                                                                                                                                                                                                                ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                client: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                { label: "Client Option D 1", value: "10" },
                                                                                                                                                                                                                                { label: "Client Option D 2", value: "11" },
                                                                                                                                                                                                                                { label: "Client Option D 3", value: "12" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                clientar: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                { label: "Client AR Option D 1", value: "10" },
                                                                                                                                                                                                                                { label: "Client AR Option D 2", value: "11" },
                                                                                                                                                                                                                                { label: "Client AR Option D 3", value: "12" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                supervisor: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                { label: "Supervisor Option D 1", value: "1" },
                                                                                                                                                                                                                                { label: "Supervisor Option D 2", value: "2" },
                                                                                                                                                                                                                                { label: "Supervisor Option D 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                                }

                                                                                                                                                                                                            }
                                                                                                                                                                                                        ],
                                                                                                                                                        todate: { show: true, disabled: false },
                                                                                                                                                        totime: { show: true, disabled: false }
                                                                                                                                                    }
                                                                                                                                                  ]
                                                                    },
                                                                    { "content_type": "select", "name": "reportname", "class": "span4", show: true, "required": true, "trigger": "change", "notblank": true, "valuetype": "alphanum", "errormsg": "Select A Name", "bindingfrom": "reportname", "bindingto": "reporttype", "bindingevent": "insertOptions", "data": "" }

                                                                ]
                                                    }
                                                ]
                  },
                                            { url: "includes/sections/fieldsets.html",
                                                "class": "form-inline",
                                                title: "Select Parameters",
                                                rows: [
                                                            {
                                                                "class": "row-fluid",
                                                                content: [
                                                                            { "content_type": "label", "for": "contact", "class": "span4", show: true, "required": false, "trigger": "", "notblank": false, "valuetype": "alphanum", "errormsg": "Select A Contact", "data": "Contact" },
                                                                            { "content_type": "label", "for": "program", "class": "span4", show: true, "required": false, "trigger": "", "notblank": false, "valuetype": "alphanum", "errormsg": "Select A Program", "data": "Program" },
                                                                            { "content_type": "label", "for": "service", "class": "span4", show: true, "required": false, "trigger": "", "notblank": false, "valuetype": "alphanum", "errormsg": "Select A Service", "data": "Service" }
                                                                        ]
                                                            },
                                                            {
                                                                "class": "row-fluid",
                                                                content: [
                                                                            { "content_type": "select", "name": "contact", "class": "span4", show: true, "bindingto": "reportname", "bindingevent": "insertOptions", "data": ""
                                                                            },
                                                                            { "content_type": "select", "name": "program", "class": "span4", show: true, "bindingto": "reportname", "bindingevent": "insertOptions", "data": ""
                                                                            },
                                                                            { "content_type": "select", "name": "service", "class": "span4", show: true, "bindingto": "reportname", "bindingevent": "insertOptions", "data": ""
                                                                            }
                                                                        ]
                                                            },
                                                            {
                                                                "class": "row-fluid",
                                                                content: [
                                                                            { "content_type": "label", "for": "client", "class": "span4 dontshow", show: false, "required": false, "trigger": "", "notblank": false, "valuetype": "alphanum", "errormsg": "Select A Client", "bindingto": "reportname", "bindingevent": "showHide", "data": "Client" },
                                                                            { "content_type": "label", "for": "clientar", "class": "span4 dontshow", show: false, "required": false, "trigger": "", "notblank": false, "valuetype": "alphanum", "errormsg": "Select A Client AR", "bindingto": "reportname", "bindingevent": "showHide", "data": "Client AR#" },
                                                                            { "content_type": "label", "for": "supervisor", "class": "span4 dontshow", show: false, "required": false, "trigger": "", "notblank": false, "valuetype": "alphanum", "errormsg": "Select A Supervisor", "bindingto": "reportname", "bindingevent": "showHide", "data": "Supervisor" }
                                                                        ]
                                                            },
                                                            {
                                                                "class": "row-fluid",
                                                                content: [
                                                                            { "content_type": "select", "name": "client", "class": "span4", show: false, "bindingto": "reportname", "bindingevent": "insertOptions,showHide", "data": ""
                                                                            },
                                                                            { "content_type": "select", "name": "clientar", "class": "span4", show: false, "bindingto": "reportname", "bindingevent": "insertOptions,showHide", "data": ""
                                                                            },
                                                                            { "content_type": "select", "name": "supervisor", "class": "span4", show: false, "bindingto": "reportname", "bindingevent": "insertOptions,showHide", "data": ""
                                                                            }
                                                                        ]
                                                            }
                                                        ]
                                            },
                                            { url: "includes/sections/fieldsets.html",
                                                "class": "form-inline last",
                                                title: "date / time",
                                                rows: [
                                                            {
                                                                "class": "row-fluid",
                                                                content: [
                                                                            { "content_type": "label", "for": "fromdate", "class": "span6", "data": "From" },
                                                                            { "content_type": "label", "for": "todate", "class": "span6 dontshow", "bindingto": "reporttype", "bindingevent": "showHide", show: false, "data": "To" }
                                                                        ]
                                                            },
                                                            {
                                                                "class": "row-fluid",
                                                                content: [
                                                                            { "content_type": "datepicker", "name": "fromdate", "class": "datepicker span3", show: true, disabled: false, "data": "" },
                                                                            { "content_type": "timepicker", "name": "fromtime", "class": "timepicker span3", show: true, disabled: false, "data": "" },
                                                                            { "content_type": "datepicker", "name": "todate", "class": "datepicker span3 dontshow", "bindingto": "reporttype", "bindingevent": "showHide", show: false, disabled: true, "data": "" },
                                                                            { "content_type": "timepicker", "name": "totime", "class": "timepicker span3 dontshow", "bindingto": "reporttype", "bindingevent": "showHide", show: false, disabled: true, "data": "" }
                                                                        ]
                                                            },
                                                            {
                                                                "class": "row-fluid",
                                                                content: [
                                                                            { "content_type": "submit", "name": "submit", "class": "btn btn-medium reportsubmit", "bindingevent": "submit", "data": "Run Report" }
                                                                         ]
                                                            }
                                                        ]
                                            }]

              } //End Of Layout JSON                                                                                      
          }
                           ];
      });

      $httpBackend.whenPOST('data/reportinglayoutsemidynamicfields').respond(function (method, url, data) {
          return [200, { authorized: authorized,
              token: 'sdsa4d56sa4dsa4d564sadsa4d56sa46d5sa4d6',
              layout: {
                  type: "form",
                  include: "includes/forms/reporting2.html",
                  "class": "form-container",
                  templates: [{ url: "includes/sections/select_report.html",
                      "class": "form-inline first",
                      title: "Select Report",
                      rows: [
                                                    {
                                                        "class": "row-fluid",
                                                        content: [
                                                                    { "content_type": "label", "for": "reporttype", "class": "span4", "data": "Type" },
                                                                    { "content_type": "label", "for": "reportname", "class": "span4", "data": "Name" }
                                                                ]
                                                    },
                                                    {
                                                        "class": "row-fluid",
                                                        content: [
                                                                    { "content_type": "select", "name": "reporttype", "class": "span4", show: true, "required": true, "trigger": "change", "notblank": true, "valuetype": "alphanum", "errormsg": "Select A Type", "publish": "reporttype", "oldsubscribe": [{ type: "select", name: "reportname" }, { type: 'sandata-datepicker', name: 'todate' }, { type: 'sandata-timepicker', name: 'timepicker'}], "data": [
                                                                                                                                                    { value: "daily", label: "Daily", reportname: [
                                                                                                                                                                                                        { label: "Second Option A", value: "secondOption1a",
                                                                                                                                                                                                            contact: {
                                                                                                                                                                                                                show: true,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Contact Option A 1", value: "1" },
                                                                                                                                                                                                                                { label: "Contact Option A 2", value: "2" },
                                                                                                                                                                                                                                { label: "Contact Option A 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            program: {
                                                                                                                                                                                                                show: true,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Program Option A 1", value: "1" },
                                                                                                                                                                                                                                { label: "Program Option A 2", value: "2" },
                                                                                                                                                                                                                                { label: "Program Option A 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            service: {
                                                                                                                                                                                                                show: true,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Service Option A 1", value: "1" },
                                                                                                                                                                                                                                { label: "Service Option A 2", value: "2" },
                                                                                                                                                                                                                                { label: "Service Option A 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            client: {
                                                                                                                                                                                                                show: false,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Client Option A 1", value: "1" },
                                                                                                                                                                                                                                { label: "Client Option A 2", value: "2" },
                                                                                                                                                                                                                                { label: "Client Option A 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            clientar: {
                                                                                                                                                                                                                show: false,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Client AR Option A 1", value: "1" },
                                                                                                                                                                                                                                { label: "Client AR Option A 2", value: "2" },
                                                                                                                                                                                                                                { label: "Client AR Option A 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            supervisor: {
                                                                                                                                                                                                                show: false,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Supervisor Option A 1", value: "1" },
                                                                                                                                                                                                                                { label: "Supervisor Option A 2", value: "2" },
                                                                                                                                                                                                                                { label: "Supervisor Option A 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            }
                                                                                                                                                                                                        },
                                                                                                                                                                                                        { label: "Second Option B", value: "secondOption1b",
                                                                                                                                                                                                            contact: {
                                                                                                                                                                                                                show: true,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Contact Option B 1", value: "4" },
                                                                                                                                                                                                                                { label: "Second Option B 2", value: "5" },
                                                                                                                                                                                                                                { label: "Contact Option B 3", value: "6" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            program: {
                                                                                                                                                                                                                show: true,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Program Option B 1", value: "1" },
                                                                                                                                                                                                                                { label: "Program Option B 2", value: "2" },
                                                                                                                                                                                                                                { label: "Program Option B 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            service: {
                                                                                                                                                                                                                show: true,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Service Option B 1", value: "1" },
                                                                                                                                                                                                                                { label: "Service Option B 2", value: "2" },
                                                                                                                                                                                                                                { label: "Service Option B 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            client: {
                                                                                                                                                                                                                show: false,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Client Option B 1", value: "1" },
                                                                                                                                                                                                                                { label: "Client Option B 2", value: "2" },
                                                                                                                                                                                                                                { label: "Client Option B 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            clientar: {
                                                                                                                                                                                                                show: false,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Client AR Option B 1", value: "1" },
                                                                                                                                                                                                                                { label: "Client AR Option B 2", value: "2" },
                                                                                                                                                                                                                                { label: "Client AR Option B 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            },
                                                                                                                                                                                                            supervisor: {
                                                                                                                                                                                                                show: false,
                                                                                                                                                                                                                data: [
                                                                                                                                                                                                                                { label: "Supervisor Option B 1", value: "1" },
                                                                                                                                                                                                                                { label: "Supervisor Option B 2", value: "2" },
                                                                                                                                                                                                                                { label: "Supervisor Option B 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                            }

                                                                                                                                                                                                        }
                                                                                                                                                                                                    ],
                                                                                                                                                        todate: { show: false, disabled: true },
                                                                                                                                                        totime: { show: false, disabled: true }
                                                                                                                                                    },
                                                                                                                                                    { value: "monthly", label: "Monthly", reportname: [
                                                                                                                                                                                                            { label: "Second Option C", value: "secondOption1c",
                                                                                                                                                                                                                contact: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                    { label: "Contact Option C 1", value: "7" },
                                                                                                                                                                                                                                    { label: "Contact Option C 2", value: "8" },
                                                                                                                                                                                                                                    { label: "Contact Option C 3", value: "9" }
                                                                                                                                                                                                                                ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                program: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                    { label: "Program Option C 1", value: "1" },
                                                                                                                                                                                                                                    { label: "Program Option C 2", value: "2" },
                                                                                                                                                                                                                                    { label: "Program Option C 3", value: "3" }
                                                                                                                                                                                                                                ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                service: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                    { label: "Service Option C 1", value: "1" },
                                                                                                                                                                                                                                    { label: "Service Option C 2", value: "2" },
                                                                                                                                                                                                                                    { label: "Service Option C 3", value: "3" }
                                                                                                                                                                                                                                ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                client: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                { label: "Client Option C 1", value: "1" },
                                                                                                                                                                                                                                { label: "Client Option C 2", value: "2" },
                                                                                                                                                                                                                                { label: "Client Option C 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                clientar: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                { label: "Client AR Option C 1", value: "1" },
                                                                                                                                                                                                                                { label: "Client AR Option C 2", value: "2" },
                                                                                                                                                                                                                                { label: "Client AR Option C 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                supervisor: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                { label: "Supervisor Option C 1", value: "1" },
                                                                                                                                                                                                                                { label: "Supervisor Option C 2", value: "2" },
                                                                                                                                                                                                                                { label: "Supervisor Option C 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                                }

                                                                                                                                                                                                            },
                                                                                                                                                                                                            { label: "Second Option D", value: "secondOption1d",
                                                                                                                                                                                                                contact: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                    { label: "Contact Option D 1", value: "10" },
                                                                                                                                                                                                                                    { label: "Contact Option D 2", value: "11" },
                                                                                                                                                                                                                                    { label: "Contact Option D 3", value: "12" }
                                                                                                                                                                                                                                ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                program: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                    { label: "Program Option D 1", value: "10" },
                                                                                                                                                                                                                                    { label: "Program Option D 2", value: "11" },
                                                                                                                                                                                                                                    { label: "Program Option D 3", value: "12" }
                                                                                                                                                                                                                                ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                service: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                    { label: "Service Option D 1", value: "10" },
                                                                                                                                                                                                                                    { label: "Service Option D 2", value: "11" },
                                                                                                                                                                                                                                    { label: "Service Option D 3", value: "12" }
                                                                                                                                                                                                                                ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                client: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                { label: "Client Option D 1", value: "10" },
                                                                                                                                                                                                                                { label: "Client Option D 2", value: "11" },
                                                                                                                                                                                                                                { label: "Client Option D 3", value: "12" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                clientar: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                { label: "Client AR Option D 1", value: "10" },
                                                                                                                                                                                                                                { label: "Client AR Option D 2", value: "11" },
                                                                                                                                                                                                                                { label: "Client AR Option D 3", value: "12" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                                },
                                                                                                                                                                                                                supervisor: {
                                                                                                                                                                                                                    show: true,
                                                                                                                                                                                                                    data: [
                                                                                                                                                                                                                                { label: "Supervisor Option D 1", value: "1" },
                                                                                                                                                                                                                                { label: "Supervisor Option D 2", value: "2" },
                                                                                                                                                                                                                                { label: "Supervisor Option D 3", value: "3" }
                                                                                                                                                                                                                            ]
                                                                                                                                                                                                                }

                                                                                                                                                                                                            }
                                                                                                                                                                                                        ],
                                                                                                                                                        todate: { show: true, disabled: false },
                                                                                                                                                        totime: { show: true, disabled: false }
                                                                                                                                                    }
                                                                                                                                                  ]
                                                                    },
                                                                    { "content_type": "select", "name": "reportname", "class": "span4", show: true, "required": true, "trigger": "change", "notblank": true, "valuetype": "alphanum", "errormsg": "Select A Name", "publish": "reportname", "subscribe": "reporttype", "events": "insertOptions", "data": "" }

                                                                ]
                                                    }
                                                ]
                  },
                                            { url: "includes/sections/select_parameters.html",
                                                "class": "form-inline",
                                                title: "Select Parameters",
                                                rows: [
                                                            {
                                                                "class": "row-fluid",
                                                                content: [
                                                                            { "content_type": "label", "for": "contact", "class": "span4", show: true, "required": false, "trigger": "", "notblank": false, "valuetype": "alphanum", "errormsg": "Select A Contact", "data": "Contact" },
                                                                            { "content_type": "label", "for": "program", "class": "span4", show: true, "required": false, "trigger": "", "notblank": false, "valuetype": "alphanum", "errormsg": "Select A Program", "data": "Program" },
                                                                            { "content_type": "label", "for": "service", "class": "span4", show: true, "required": false, "trigger": "", "notblank": false, "valuetype": "alphanum", "errormsg": "Select A Service", "data": "Service" }
                                                                        ]
                                                            },
                                                            {
                                                                "class": "row-fluid",
                                                                content: [
                                                                            { "content_type": "select", "name": "contact", "class": "span4", show: true, "subscribe": "reportname", "events": "insertOptions", "data": ""
                                                                            },
                                                                            { "content_type": "select", "name": "program", "class": "span4", show: true, "subscribe": "reportname", "events": "insertOptions", "data": ""
                                                                            },
                                                                            { "content_type": "select", "name": "service", "class": "span4", show: true, "subscribe": "reportname", "events": "insertOptions", "data": ""
                                                                            }
                                                                        ]
                                                            },
                                                            {
                                                                "class": "row-fluid",
                                                                content: [
                                                                            { "content_type": "label", "for": "client", "class": "span4 dontshow", show: false, "required": false, "trigger": "", "notblank": false, "valuetype": "alphanum", "errormsg": "Select A Client", "subscribe": "reportname", "events": "showHide", "data": "Client" },
                                                                            { "content_type": "label", "for": "clientar", "class": "span4 dontshow", show: false, "required": false, "trigger": "", "notblank": false, "valuetype": "alphanum", "errormsg": "Select A Client AR", "subscribe": "reportname", "events": "showHide", "data": "Client AR#" },
                                                                            { "content_type": "label", "for": "supervisor", "class": "span4 dontshow", show: false, "required": false, "trigger": "", "notblank": false, "valuetype": "alphanum", "errormsg": "Select A Supervisor", "subscribe": "reportname", "events": "showHide", "data": "Supervisor" }
                                                                        ]
                                                            },
                                                            {
                                                                "class": "row-fluid",
                                                                content: [
                                                                            { "content_type": "select", "name": "client", "class": "span4", show: false, "subscribe": "reportname", "events": "insertOptions,showHide", "data": ""
                                                                            },
                                                                            { "content_type": "select", "name": "clientar", "class": "span4", show: false, "subscribe": "reportname", "events": "insertOptions,showHide", "data": ""
                                                                            },
                                                                            { "content_type": "select", "name": "supervisor", "class": "span4", show: false, "subscribe": "reportname", "events": "insertOptions,showHide", "data": ""
                                                                            }
                                                                        ]
                                                            }
                                                        ]
                                            },
                                            { url: "includes/sections/date_time.html",
                                                "class": "form-inline last",
                                                title: "date / time",
                                                rows: [
                                                            {
                                                                "class": "row-fluid",
                                                                content: [
                                                                            { "content_type": "label", "for": "fromdate", "class": "span6", "data": "From" },
                                                                            { "content_type": "label", "for": "todate", "class": "span6 dontshow", "subscribe": "reporttype", "events": "showHide", show: false, "data": "To" }
                                                                        ]
                                                            },
                                                            {
                                                                "class": "row-fluid",
                                                                content: [
                                                                            { "content_type": "datepicker", "name": "fromdate", "class": "datepicker span3", show: true, disabled: false, "data": "" },
                                                                            { "content_type": "timepicker", "name": "fromtime", "class": "timepicker span3", show: true, disabled: false, "data": "" },
                                                                            { "content_type": "datepicker", "name": "todate", "class": "datepicker span3 dontshow", "subscribe": "reporttype", "events": "showHide", show: false, disabled: true, "data": "" },
                                                                            { "content_type": "timepicker", "name": "totime", "class": "timepicker span3 dontshow", "subscribe": "reporttype", "events": "showHide", show: false, disabled: true, "data": "" }
                                                                        ]
                                                            },
                                                            {
                                                                "class": "row-fluid",
                                                                content: [
                                                                            { "content_type": "submit", "name": "submit", "class": "btn btn-medium reportsubmit", "events": "submit", "data": "Run Report" }
                                                                         ]
                                                            }
                                                        ]
                                            }]

              } //End Of Layout JSON                                                                                       
          }
                           ];
      });

      $httpBackend.whenPOST('data/reportingform').respond(function (method, url, data) {
          return [200, {
              authorized: authorized,
              token: 'sdsa4d56sa4dsa4d564sadsa4d56sa46d5sa4d6',
              form: {
                  "sections": {
                      "report": { "class": "form-inline", "show": true, "title": "SELECT REPORT" },
                      "parameters": { "class": "form-inline", "show": false, "title": "SELECT PARAMETERS" },
                      "datetime": { "class": "form-inline", "show": true, "title": "date / time" },
                      "submit": { "class": "form-inline last", "show": true, "title": "" }
                  },
                  "fields": {
                      "reporttype": {
                          "content_type": "select", "name": "reporttype", "class": "span4", "show": true, "required": true, "trigger": "change", "notblank": true, "valuetype": "alphanum", "errormsg": "Select A Type", "publish": "reporttype", "data": [
                                                                                                                                           {
                                                                                                                                               "value": "daily", "label": "Daily", "reportname": [
                                                                                                                                                                                                 {
                                                                                                                                                                                                     "label": "Second Option A", "value": "secondOption1a",
                                                                                                                                                                                                     "contact": {
                                                                                                                                                                                                         "show": true,
                                                                                                                                                                                                         "data": [
                                                                                                                                                                                                                 { "label": "Contact Option A 1", "value": "1" },
                                                                                                                                                                                                                 { "label": "Contact Option A 2", "value": "2" },
                                                                                                                                                                                                                 { "label": "Contact Option A 3", "value": "3" }
                                                                                                                                                                                                         ]
                                                                                                                                                                                                     },
                                                                                                                                                                                                     "program": {
                                                                                                                                                                                                         "show": true,
                                                                                                                                                                                                         "data": [
                                                                                                                                                                                                                 { "label": "Program Option A 1", "value": "1" },
                                                                                                                                                                                                                 { "label": "Program Option A 2", "value": "2" },
                                                                                                                                                                                                                 { "label": "Program Option A 3", "value": "3" }
                                                                                                                                                                                                         ]
                                                                                                                                                                                                     },
                                                                                                                                                                                                     "service": {
                                                                                                                                                                                                         "show": true,
                                                                                                                                                                                                         "data": [
                                                                                                                                                                                                                 { "label": "Service Option A 1", "value": "1" },
                                                                                                                                                                                                                 { "label": "Service Option A 2", "value": "2" },
                                                                                                                                                                                                                 { "label": "Service Option A 3", "value": "3" }
                                                                                                                                                                                                         ]
                                                                                                                                                                                                     },
                                                                                                                                                                                                     "client": {
                                                                                                                                                                                                         "show": false,
                                                                                                                                                                                                         "data": [
                                                                                                                                                                                                                 { "label": "Client Option A 1", "value": "1" },
                                                                                                                                                                                                                 { "label": "Client Option A 2", "value": "2" },
                                                                                                                                                                                                                 { "label": "Client Option A 3", "value": "3" }
                                                                                                                                                                                                         ]
                                                                                                                                                                                                     },
                                                                                                                                                                                                     "clientar": {
                                                                                                                                                                                                         "show": false,
                                                                                                                                                                                                         "data": [
                                                                                                                                                                                                                 { "label": "Client AR Option A 1", "value": "1" },
                                                                                                                                                                                                                 { "label": "Client AR Option A 2", "value": "2" },
                                                                                                                                                                                                                 { "label": "Client AR Option A 3", "value": "3" }
                                                                                                                                                                                                         ]
                                                                                                                                                                                                     },
                                                                                                                                                                                                     "supervisor": {
                                                                                                                                                                                                         "show": false,
                                                                                                                                                                                                         "data": [
                                                                                                                                                                                                                 { "label": "Supervisor Option A 1", "value": "1" },
                                                                                                                                                                                                                 { "label": "Supervisor Option A 2", "value": "2" },
                                                                                                                                                                                                                 { "label": "Supervisor Option A 3", "value": "3" }
                                                                                                                                                                                                         ]
                                                                                                                                                                                                     }
                                                                                                                                                                                                 },
                                                                                                                                                                                                 {
                                                                                                                                                                                                     "label": "Second Option B", "value": "secondOption1b",
                                                                                                                                                                                                     "contact": {
                                                                                                                                                                                                         "show": true,
                                                                                                                                                                                                         "data": [
                                                                                                                                                                                                                 { "label": "Contact Option B 1", "value": "4" },
                                                                                                                                                                                                                 { "label": "Second Option B 2", "value": "5" },
                                                                                                                                                                                                                 { "label": "Contact Option B 3", "value": "6" }
                                                                                                                                                                                                         ]
                                                                                                                                                                                                     },
                                                                                                                                                                                                     "program": {
                                                                                                                                                                                                         "show": true,
                                                                                                                                                                                                         "data": [
                                                                                                                                                                                                                 { "label": "Program Option B 1", "value": "1" },
                                                                                                                                                                                                                 { "label": "Program Option B 2", "value": "2" },
                                                                                                                                                                                                                 { "label": "Program Option B 3", "value": "3" }
                                                                                                                                                                                                         ]
                                                                                                                                                                                                     },
                                                                                                                                                                                                     "service": {
                                                                                                                                                                                                         "show": true,
                                                                                                                                                                                                         "data": [
                                                                                                                                                                                                                 { "label": "Service Option B 1", "value": "1" },
                                                                                                                                                                                                                 { "label": "Service Option B 2", "value": "2" },
                                                                                                                                                                                                                 { "label": "Service Option B 3", "value": "3" }
                                                                                                                                                                                                         ]
                                                                                                                                                                                                     },
                                                                                                                                                                                                     "client": {
                                                                                                                                                                                                         "show": false,
                                                                                                                                                                                                         "data": [
                                                                                                                                                                                                                 { "label": "Client Option B 1", "value": "1" },
                                                                                                                                                                                                                 { "label": "Client Option B 2", "value": "2" },
                                                                                                                                                                                                                 { "label": "Client Option B 3", "value": "3" }
                                                                                                                                                                                                         ]
                                                                                                                                                                                                     },
                                                                                                                                                                                                     "clientar": {
                                                                                                                                                                                                         "show": false,
                                                                                                                                                                                                         "data": [
                                                                                                                                                                                                                 { "label": "Client AR Option B 1", "value": "1" },
                                                                                                                                                                                                                 { "label": "Client AR Option B 2", "value": "2" },
                                                                                                                                                                                                                 { "label": "Client AR Option B 3", "value": "3" }
                                                                                                                                                                                                         ]
                                                                                                                                                                                                     },
                                                                                                                                                                                                     "supervisor": {
                                                                                                                                                                                                         "show": false,
                                                                                                                                                                                                         "data": [
                                                                                                                                                                                                                 { "label": "Supervisor Option B 1", "value": "1" },
                                                                                                                                                                                                                 { "label": "Supervisor Option B 2", "value": "2" },
                                                                                                                                                                                                                 { "label": "Supervisor Option B 3", "value": "3" }
                                                                                                                                                                                                         ]
                                                                                                                                                                                                     }

                                                                                                                                                                                                 }
                                                                                                                                               ],
                                                                                                                                               "todate": { "show": false, "disabled": true },
                                                                                                                                               "totime": { "show": false, "disabled": true }
                                                                                                                                           },
                                                                                                                                           {
                                                                                                                                               "value": "monthly", "label": "Monthly", "reportname": [
                                                                                                                                                                                                     {
                                                                                                                                                                                                         "label": "Second Option C", "value": "secondOption1c",
                                                                                                                                                                                                         "contact": {
                                                                                                                                                                                                             "show": true,
                                                                                                                                                                                                             "data": [
                                                                                                                                                                                                                     { "label": "Contact Option C 1", "value": "7" },
                                                                                                                                                                                                                     { "label": "Contact Option C 2", "value": "8" },
                                                                                                                                                                                                                     { "label": "Contact Option C 3", "value": "9" }
                                                                                                                                                                                                             ]
                                                                                                                                                                                                         },
                                                                                                                                                                                                         "program": {
                                                                                                                                                                                                             "show": true,
                                                                                                                                                                                                             "data": [
                                                                                                                                                                                                                     { "label": "Program Option C 1", "value": "1" },
                                                                                                                                                                                                                     { "label": "Program Option C 2", "value": "2" },
                                                                                                                                                                                                                     { "label": "Program Option C 3", "value": "3" }
                                                                                                                                                                                                             ]
                                                                                                                                                                                                         },
                                                                                                                                                                                                         "service": {
                                                                                                                                                                                                             "show": true,
                                                                                                                                                                                                             "data": [
                                                                                                                                                                                                                     { "label": "Service Option C 1", "value": "1" },
                                                                                                                                                                                                                     { "label": "Service Option C 2", "value": "2" },
                                                                                                                                                                                                                     { "label": "Service Option C 3", "value": "3" }
                                                                                                                                                                                                             ]
                                                                                                                                                                                                         },
                                                                                                                                                                                                         "client": {
                                                                                                                                                                                                             "show": true,
                                                                                                                                                                                                             "data": [
                                                                                                                                                                                                                     { "label": "Client Option C 1", "value": "1" },
                                                                                                                                                                                                                     { "label": "Client Option C 2", "value": "2" },
                                                                                                                                                                                                                     { "label": "Client Option C 3", "value": "3" }
                                                                                                                                                                                                             ]
                                                                                                                                                                                                         },
                                                                                                                                                                                                         "clientar": {
                                                                                                                                                                                                             "show": true,
                                                                                                                                                                                                             "data": [
                                                                                                                                                                                                                     { "label": "Client AR Option C 1", "value": "1" },
                                                                                                                                                                                                                     { "label": "Client AR Option C 2", "value": "2" },
                                                                                                                                                                                                                     { "label": "Client AR Option C 3", "value": "3" }
                                                                                                                                                                                                             ]
                                                                                                                                                                                                         },
                                                                                                                                                                                                         "supervisor": {
                                                                                                                                                                                                             "show": true,
                                                                                                                                                                                                             "data": [
                                                                                                                                                                                                                     { "label": "Supervisor Option C 1", "value": "1" },
                                                                                                                                                                                                                     { "label": "Supervisor Option C 2", "value": "2" },
                                                                                                                                                                                                                     { "label": "Supervisor Option C 3", "value": "3" }
                                                                                                                                                                                                             ]
                                                                                                                                                                                                         }

                                                                                                                                                                                                     },
                                                                                                                                                                                                     {
                                                                                                                                                                                                         "label": "Second Option D", "value": "secondOption1d",
                                                                                                                                                                                                         "contact": {
                                                                                                                                                                                                             "show": true,
                                                                                                                                                                                                             "data": [
                                                                                                                                                                                                                     { "label": "Contact Option D 1", "value": "10" },
                                                                                                                                                                                                                     { "label": "Contact Option D 2", "value": "11" },
                                                                                                                                                                                                                     { "label": "Contact Option D 3", "value": "12" }
                                                                                                                                                                                                             ]
                                                                                                                                                                                                         },
                                                                                                                                                                                                         "program": {
                                                                                                                                                                                                             "show": true,
                                                                                                                                                                                                             "data": [
                                                                                                                                                                                                                     { "label": "Program Option D 1", "value": "10" },
                                                                                                                                                                                                                     { "label": "Program Option D 2", "value": "11" },
                                                                                                                                                                                                                     { "label": "Program Option D 3", "value": "12" }
                                                                                                                                                                                                             ]
                                                                                                                                                                                                         },
                                                                                                                                                                                                         "service": {
                                                                                                                                                                                                             "show": true,
                                                                                                                                                                                                             "data": [
                                                                                                                                                                                                                     { "label": "Service Option D 1", "value": "10" },
                                                                                                                                                                                                                     { "label": "Service Option D 2", "value": "11" },
                                                                                                                                                                                                                     { "label": "Service Option D 3", "value": "12" }
                                                                                                                                                                                                             ]
                                                                                                                                                                                                         },
                                                                                                                                                                                                         "client": {
                                                                                                                                                                                                             "show": true,
                                                                                                                                                                                                             "data": [
                                                                                                                                                                                                                     { "label": "Client Option D 1", "value": "10" },
                                                                                                                                                                                                                     { "label": "Client Option D 2", "value": "11" },
                                                                                                                                                                                                                     { "label": "Client Option D 3", "value": "12" }
                                                                                                                                                                                                             ]
                                                                                                                                                                                                         },
                                                                                                                                                                                                         "clientar": {
                                                                                                                                                                                                             "show": true,
                                                                                                                                                                                                             "data": [
                                                                                                                                                                                                                     { "label": "Client AR Option D 1", "value": "10" },
                                                                                                                                                                                                                     { "label": "Client AR Option D 2", "value": "11" },
                                                                                                                                                                                                                     { "label": "Client AR Option D 3", "value": "12" }
                                                                                                                                                                                                             ]
                                                                                                                                                                                                         },
                                                                                                                                                                                                         "supervisor": {
                                                                                                                                                                                                             "show": true,
                                                                                                                                                                                                             "data": [
                                                                                                                                                                                                                     { "label": "Supervisor Option D 1", "value": "1" },
                                                                                                                                                                                                                     { "label": "Supervisor Option D 2", "value": "2" },
                                                                                                                                                                                                                     { "label": "Supervisor Option D 3", "value": "3" }
                                                                                                                                                                                                             ]
                                                                                                                                                                                                         }

                                                                                                                                                                                                     }
                                                                                                                                               ],
                                                                                                                                               "todate": { "show": true, "disabled": false },
                                                                                                                                               "totime": { "show": true, "disabled": false }
                                                                                                                                           }
                          ]
                      },
                      "reportname": { "content_type": "select", "name": "reportname", "class": "span4", "show": true, "required": true, "trigger": "change", "notblank": true, "valuetype": "alphanum", "errormsg": "Select A Name", "publish": "reportname", "subscribe": "reporttype", "events": "insertOptions", "data": "" },
                      "contact": { "content_type": "select", "name": "contact", "class": "span4", "show": true, "subscribe": "reportname", "events": "insertOptions", "data": "" },
                      "program": { "content_type": "select", "name": "program", "class": "span4", "show": true, "subscribe": "reportname", "events": "insertOptions", "data": "" },
                      "service": { "content_type": "select", "name": "service", "class": "span4", "show": true, "subscribe": "reportname", "events": "insertOptions", "data": "" },
                      "client": { "content_type": "select", "name": "client", "class": "span4", "show": false, "subscribe": "reportname", "events": "insertOptions,showHide", "data": "" },
                      "clientar": { "content_type": "select", "name": "clientar", "class": "span4", "show": false, "subscribe": "reportname", "events": "insertOptions,showHide", "data": "" },
                      "supervisor": { "content_type": "select", "name": "supervisor", "class": "span4", "show": false, "subscribe": "reportname", "events": "insertOptions,showHide", "data": "" },
                      "fromdate": { "content_type": "datepicker", "name": "fromdate", "class": "datepicker span3", "show": true, "disabled": false, "data": "", "required": "required", "errormsg": "Enter A From Date" },
                      "fromtime": { "content_type": "timepicker", "name": "fromtime", "class": "timepicker span3", "show": true, "disabled": false, "data": "", "required": "required", "errormsg": "Enter A From Time" },
                      "todate": { "content_type": "datepicker", "name": "todate", "class": "datepicker span3 dontshow", "subscribe": "reporttype", "events": "showHide", "show": false, "disabled": true, "data": "" },
                      "totime": { "content_type": "timepicker", "name": "totime", "class": "timepicker span3 dontshow", "subscribe": "reporttype", "events": "showHide", "show": false, "disabled": true, "data": "" },
                      "submit": { "content_type": "submit", "name": "submit", "class": "btn btn-medium reportsubmit", "events": "submit", "data": "Run Report" }

                  },
                  "labels": {
                      "reporttype": { "content_type": "label", "for": "reporttype", "class": "span4", "data": "Type" },
                      "reportname": { "content_type": "label", "for": "reportname", "class": "span4", "data": "Name" },
                      "contact": { "content_type": "label", "for": "contact", "class": "span4", "show": true, "required": false, "data": "Contact" },
                      "program": { "content_type": "label", "for": "program", "class": "span4", "show": true, "required": false, "data": "Program" },
                      "service": { "content_type": "label", "for": "service", "class": "span4", "show": true, "required": false, "data": "Service" },
                      "client": { "content_type": "label", "for": "client", "class": "span4 dontshow", "show": false, "required": false, "data": "Client" },
                      "clientar": { "content_type": "label", "for": "clientar", "class": "span4 dontshow", "show": false, "required": false, "data": "Client AR#" },
                      "supervisor": { "content_type": "label", "for": "supervisor", "class": "span4 dontshow", "show": false, "required": false, "data": "Supervisor" },
                      "fromdate": { "content_type": "label", "for": "fromdate", "class": "span6", "data": "From" },
                      "todate": { "content_type": "label", "for": "todate", "class": "span6 dontshow", "subscribe": "reporttype", "events": "showHide", "show": false, "data": "To" }

                  }

              }//End Of Layout JSON                                                                                       
          }
          ];
      });

      // Contracts
      $httpBackend.whenGET('service/account/contracts').respond(function (method, url, data) {

          return [200, {

              "contracts": [
                    {
                        "key": 0,
                        "value": "Contract1"
                    },
                    {
                        "key": 1,
                        "value": "Contract2"
                    },
                    {
                        "key": 2,
                        "value": "Contract3"
                    }
                ]
          }
            ];
      });

      // Programs
      $httpBackend.whenGET('service/account/programs').respond(function (method, url, data) {

          return [200, {

              "programs": [
                  {
                      "label": "Program1"
                  },
                  {
                      "label": "Program2"
                  },
                  {
                      "label": "Program3"
                  }
              ]
          }
          ];
      });

      $httpBackend.whenPOST('data/reportingformfilter').respond(function (method, url, data) {
          return [200, {

              "status_code": "200",
              "rolling_token": "d1sa5da156da1d3sa1d32sa1d23sa1dsa13d1sa3d1a",
              "name": null,
              "form": {
                  "sections": {
                      "report": {
                          "show": true,
                          "title": "SELECT REPORT",
                          "base_components": {
                              "report_type": {
                                  "select_label": "Type",
                                  "required": false,
                                  "disabled": false,
                                  "error_message": "Select A Type",
                                  "content_type": "SelectComponent",
                                  "name": "reportTypes",
                                  "show": true,
                                  "option_components": [
                                  {
                                      "value": "Security",
                                      "label": "Security",
                                      "group": "SEC",
                                      "show": false,
                                      "actions": []
                                  },
                                  {
                                      "value": "Date Range",
                                      "label": "Date Range",
                                      "group": "ORA_OTHER",
                                      "show": false,
                                      "actions": []
                                  },
                                  {
                                      "value": "Daily Reports",
                                      "label": "Daily Reports",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": []
                                  },
                                  {
                                      "value": "Date Range Reports",
                                      "label": "Date Range Reports",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": []
                                  }
                              ]
                              },
                              "report_name": {
                                  "select_label": "Name",
                                  "required": false,
                                  "disabled": false,
                                  "error_message": "Select A Name",
                                  "content_type": "SelectComponent",
                                  "name": "reportNames",
                                  "show": true,
                                  "option_components": [
                                  {
                                      "value": "2989",
                                      "label": "Weekly Conflict Report",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "410",
                                      "label": "DADS Non-Contracted Use",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2988",
                                      "label": "Monthly Conflict Report",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [

                                      ]
                                  },
                                  {
                                      "value": "1",
                                      "label": "Daily Call Listing",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "411",
                                      "label": "DADS Client Validation Exceptions",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2",
                                      "label": "No Show",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "408",
                                      "label": "Agency Level Errors Report",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "3",
                                      "label": "Unknown Client",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "4",
                                      "label": "Unknown (Attn/Emp)",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "141",
                                      "label": "Employee Weekly Master Schedule",
                                      "group": "ORA_OTHER",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "414",
                                      "label": "DADS Invoice Client Issues",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "5",
                                      "label": "Unscheduled Visit",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "415",
                                      "label": "DADS Invoice CDS Employee Issues",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "6",
                                      "label": "Master Schedule",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "412",
                                      "label": "DADS Employee Validation Exceptions",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "413",
                                      "label": "DADS Schedule Validation Exceptions",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2314",
                                      "label": "Weekly Call Summary",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2315",
                                      "label": "Payroll Summary (Emp)",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "14",
                                      "label": "Clients",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "15",
                                      "label": "Attendant/Worker",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "152",
                                      "label": "Client Weekly Master Schedule",
                                      "group": "ORA_OTHER",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "16",
                                      "label": "Daily Call Summary",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "394",
                                      "label": "FVV Assignment Report",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2016",
                                      "label": "Daily Call Summary",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "393",
                                      "label": "Invalid FVV Entries Report",
                                      "group": "ORA_OTHER",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "398",
                                      "label": "FVV Registrations Report",
                                      "group": "ORA_OTHER",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "397",
                                      "label": "Session Activity Report",
                                      "group": "SEC",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "391",
                                      "label": "FVV Call Listing Report",
                                      "group": "ORA_OTHER",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2994",
                                      "label": "Client Visit Verification",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2995",
                                      "label": "Speaker Verification Client Enrollment",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2003",
                                      "label": "Unknown Client",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2001",
                                      "label": "Daily Call Listing",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "315",
                                      "label": "Payroll Summary (Emp)",
                                      "group": "ORA_OTHER",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "314",
                                      "label": "Weekly Call Summary",
                                      "group": "ORA_OTHER",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2014",
                                      "label": "Clients",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "316",
                                      "label": "Payroll Summary (Cli)",
                                      "group": "ORA_OTHER",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2015",
                                      "label": "Employees (Attendant/Worker)",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "425",
                                      "label": "DADS Visit Compliance Ad Hoc",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "424",
                                      "label": "DADS Attendant Compliance",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "427",
                                      "label": "DADS FVV Order Form Report",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2217",
                                      "label": "Individual Client Activity",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "426",
                                      "label": "DADS FVV Order Review",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "417",
                                      "label": "Visit Maintenance Activity Summary Report",
                                      "group": "ORA_OTHER",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "416",
                                      "label": "DADS Invoice Provider Schedule Issues",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "419",
                                      "label": "DADS VM Compliance Summary Ad Hoc",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "418",
                                      "label": "DADS VM Compliance Summary Snapshot",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "421",
                                      "label": "DADS VM Compliance Daily Ad Hoc",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "420",
                                      "label": "DADS VM Compliance Daily Snapshot",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "326",
                                      "label": "Visit Maint Exceptions",
                                      "group": "ORA_OTHER",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "327",
                                      "label": "Visit Maintenance Activity Report",
                                      "group": "ORA_OTHER",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2389",
                                      "label": "Speaker Verification Employee Enrollment",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "325",
                                      "label": "Visit Maintenance Report",
                                      "group": "ORA_OTHER",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2404",
                                      "label": "Client Visit Log",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2407",
                                      "label": "Personal Care Attendant Timesheet",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "371",
                                      "label": "Security Activity Report",
                                      "group": "SEC",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2286",
                                      "label": "Expanded Visit Sum (Cli)",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "2417",
                                      "label": "Visit Maintenance Activity Summary Report",
                                      "group": "DATERANGE",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_clientar",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_service",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_program",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "115",
                                      "label": "Unmatched Client Phone/ID",
                                      "group": "DAILY",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "354",
                                      "label": "Visits Summary by Emp",
                                      "group": "ORA_OTHER",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  },
                                  {
                                      "value": "355",
                                      "label": "Visits Summary by Client",
                                      "group": "ORA_OTHER",
                                      "show": false,
                                      "actions": [
                                          {
                                              "show": {
                                                  "component": "report_parameter_supervisor",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_client",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_parameter_contract",
                                                  "section": "report_parameters"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_totime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_todate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromtime",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          },
                                          {
                                              "show": {
                                                  "component": "report_datetime_fromdate",
                                                  "section": "report_datetime"
                                              },
                                              "disabled": null
                                          }
                                      ]
                                  }

                              ]
                              }
                          }

                      },
                      "report_datetime": {
                          "show": false,
                          "title": "DATE / TIME",
                          "base_components": {
                              "report_datetime_totime": {
                                  "time_label": null,
                                  "required": false,
                                  "disabled": false,
                                  "error_message": null,
                                  "content_type": "TimeComponent",
                                  "name": "report_datetime_totime",
                                  "show": false
                              },
                              "report_datetime_todate": {
                                  "date_label": "To",
                                  "required": false,
                                  "disabled": false,
                                  "error_message": null,
                                  "content_type": "DateComponent",
                                  "name": "report_datetime_todate",
                                  "show": false
                              },
                              "report_datetime_fromtime": {
                                  "time_label": null,
                                  "required": false,
                                  "disabled": false,
                                  "error_message": "Enter A From Time",
                                  "content_type": "TimeComponent",
                                  "name": "report_datetime_fromtime",
                                  "show": false
                              },
                              "report_datetime_fromdate": {
                                  "date_label": "From",
                                  "required": false,
                                  "disabled": false,
                                  "error_message": "Enter A From Date",
                                  "content_type": "DateComponent",
                                  "name": "report_datetime_fromdate",
                                  "show": false
                              }
                          }
                      },
                      "report_parameters": {
                          "show": false,
                          "title": "SELECT PARAMETERS",
                          "base_components": {
                              "report_parameter_program": {
                                  "option_components": null,
                                  "select_label": "Program",
                                  "required": false,
                                  "disabled": false,
                                  "error_message": null,
                                  "content_type": "SelectComponent",
                                  "name": "programs",
                                  "show": false
                              },
                              "report_parameter_contract": {
                                  "option_components": null,
                                  "select_label": "Contract",
                                  "required": false,
                                  "disabled": false,
                                  "error_message": null,
                                  "content_type": "SelectComponent",
                                  "name": "contracts",
                                  "show": false
                              },
                              "report_parameter_service": {
                                  "option_components": null,
                                  "select_label": "Service",
                                  "required": false,
                                  "disabled": false,
                                  "error_message": null,
                                  "content_type": "SelectComponent",
                                  "name": "services",
                                  "show": false
                              },
                              "report_parameter_client": {
                                  "input_label": "Client",
                                  "required": false,
                                  "disabled": false,
                                  "error_message": null,
                                  "content_type": "InputComponent",
                                  "name": "clients",
                                  "show": false,
                                  "default_text": "All"
                              },
                              "report_parameter_clientar": {
                                  "input_label": "Client AR#",
                                  "required": false,
                                  "disabled": false,
                                  "error_message": null,
                                  "content_type": "InputComponent",
                                  "name": "clientars",
                                  "show": false,
                                  "default_text": "All"
                              },
                              "report_parameter_supervisor": {
                                  "option_components": null,
                                  "select_label": "Supervisor",
                                  "required": false,
                                  "disabled": false,
                                  "error_message": null,
                                  "content_type": "SelectComponent",
                                  "name": "supervisors",
                                  "show": false
                              },
                              "report_parameter_department": {
                                  "option_components": null,
                                  "select_label": "Department",
                                  "required": false,
                                  "disabled": false,
                                  "error_message": null,
                                  "content_type": "SelectComponent",
                                  "name": "departments",
                                  "show": false
                              },
                              "report_parameter_exception": {
                                  "option_components": null,
                                  "select_label": "Exception",
                                  "required": false,
                                  "disabled": false,
                                  "error_message": null,
                                  "content_type": "SelectComponent",
                                  "name": "exceptions",
                                  "show": false
                              },
                              "report_parameter_order_sequence": {
                                  "option_components": null,
                                  "select_label": "Order Sequence",
                                  "required": false,
                                  "disabled": false,
                                  "error_message": null,
                                  "content_type": "SelectComponent",
                                  "name": "ordersequences",
                                  "show": false
                              }
                          }
                      },
                      "submit_button": {
                          "show": false,
                          "submit": {
                              "content_type": "submit",
                              "name": "submit",
                              "events": "submit",
                              "data": "Run Report"
                          }
                      }
                  }
              }
          }
          ];
      });

      $httpBackend.whenPOST('data/dashboard').respond(function (method, url, data) {
          console.log(data)
          if (data == 2) {
              // console.log("Argument Passed", data);
              return [200, {
                  authorized: authorized,
                  token: 'sdsa4d56sa4dsa4d564sadsa4d56sa46d5sa4d6',
                  dashboard: {
                      "charts": {
                          "visit_need_attention": {
                              "label": "Visit Need Attention",
                              "value": 6
                          },
                          "hours_allocated": {
                              "label": "Visits Need Approval",
                              "value": 16
                          }
                      },

                      "unknown_calls": {
                          "title": "Unknown Calls",
                          "headers": {
                              "date": { "label": "Date", "show": true },
                              "service": { "label": "Service", "show": true },
                              "employee": { "label": "Employee", "show": true },
                              "time_in": { "label": "In", "show": true },
                              "time_out": { "label": "Out", "show": true },
                              "hours": { "label": "Hours", "show": true },
                              "assign_to": { "label": "Assign To", "show": true },
                              "actions": { "label": "Actions", "show": true }
                          },
                          "data": [
                              {
                                  "date": "Sept 12, 2013",
                                  "service": "Service Name",
                                  "employee": "Employee Name",
                                  "time_in": "9:00am",
                                  "time_out": "4:00pm",
                                  "hours": 7,
                                  "assign_to": "Edward Jones",
                                  "actions": "Confirm",
                                  "show": true
                              },
                              {
                                  "date": "Sept 13, 2013",
                                  "service": "Service Name",
                                  "employee": "Bob Smith",
                                  "time_in": "9:00am",
                                  "time_out": "4:00pm",
                                  "hours": 7,
                                  "assign_to": "Edward Jones",
                                  "actions": "Confirm",
                                  "show": true
                              }
                          ]
                      },

                      "curr_auths": {
                          "title": "Current Authorizations",
                          "headers": {
                              "service": { "label": "Service", "show": true },
                              "start_date": { "label": "Start Date", "show": true },
                              "end_date": { "label": "End Date", "show": true },
                              "hrs_used": { "label": "Hours Used", "show": true },
                              "goto": { "label": "Go To", "show": true },
                              "remarks": { "label": "Remarks", "show": true }
                          },
                          "data": [
                                                                {
                                                                    "service": "Surgery",
                                                                    "start_date": "May 1, 2013",
                                                                    "end_date": "May 31, 2013",
                                                                    "hrs_used": {
                                                                        "start": "0",
                                                                        "median": "50",
                                                                        "end": "100",
                                                                        "value": "25",
                                                                        "status": "success"
                                                                    },
                                                                    "goto": "#",
                                                                    "remarks": {
                                                                        "code": "success",
                                                                        "label": "Active"
                                                                    },
                                                                    "details": {
                                                                        "reference": {
                                                                            "label": "Reference",
                                                                            "value": "Reference",
                                                                            "show": true
                                                                        },
                                                                        "units": {
                                                                            "label": "Units",
                                                                            "value": "Units",
                                                                            "show": true
                                                                        },
                                                                        "auth_type": {
                                                                            "label": "Auth Type",
                                                                            "value": "Auth Type",
                                                                            "show": true
                                                                        },
                                                                        "limit_type": {
                                                                            "label": "Limit Type",
                                                                            "value": "Limit Type",
                                                                            "show": true
                                                                        },
                                                                        "hours_used": {
                                                                            "per_year": {
                                                                                "total": {
                                                                                    "label": "Per Year Limited by Month",
                                                                                    "value": 600
                                                                                },
                                                                                "used": {
                                                                                    "label": "Hours Used",
                                                                                    "value": 96
                                                                                },
                                                                                "percentage": "16%"
                                                                            },
                                                                            "per_month": {
                                                                                "total": {
                                                                                    "label": "Per Month",
                                                                                    "value": 50
                                                                                },
                                                                                "used": {
                                                                                    "label": "Hours Used",
                                                                                    "value": 13
                                                                                },
                                                                                "percentage": "25%"
                                                                            }
                                                                        }
                                                                    }

                                                                },
                                                                 {
                                                                     "service": "Surgery",
                                                                     "start_date": "May 1, 2013",
                                                                     "end_date": "May 31, 2013",
                                                                     "hrs_used": {
                                                                         "start": "0",
                                                                         "median": "50",
                                                                         "end": "100",
                                                                         "value": "110",
                                                                         "status": "danger"
                                                                     },
                                                                     "goto": "#",
                                                                     "remarks": { "code": "important", "label": "Exceeded" },
                                                                     "details": {
                                                                         "reference": {
                                                                             "label": "Reference",
                                                                             "value": "Reference",
                                                                             "show": true
                                                                         },
                                                                         "units": {
                                                                             "label": "Units",
                                                                             "value": "Units",
                                                                             "show": true
                                                                         },
                                                                         "auth_type": {
                                                                             "label": "Auth Type",
                                                                             "value": "Auth Type",
                                                                             "show": true
                                                                         },
                                                                         "limit_type": {
                                                                             "label": "Limit Type",
                                                                             "value": "Limit Type",
                                                                             "show": true
                                                                         },
                                                                         "hours_used": {
                                                                             "per_year": {
                                                                                 "total": {
                                                                                     "label": "Per Year Limited by Month",
                                                                                     "value": 400
                                                                                 },
                                                                                 "used": {
                                                                                     "label": "Hours Used",
                                                                                     "value": 100
                                                                                 },
                                                                                 "percentage": "25%"
                                                                             },
                                                                             "per_month": {
                                                                                 "total": {
                                                                                     "label": "Per Month",
                                                                                     "value": 30
                                                                                 },
                                                                                 "used": {
                                                                                     "label": "Hours Used",
                                                                                     "value": 33
                                                                                 },
                                                                                 "percentage": "110%"
                                                                             }
                                                                         }
                                                                     }
                                                                 }
                          ]
                      }
                  }

              }
              ];
          } else {
              return [200, {
                  authorized: authorized,
                  token: 'sdsa4d56sa4dsa4d564sadsa4d56sa46d5sa4d6',
                  dashboard: {
                      "charts": {
                          "visit_need_attention": {
                              "label": "Visit Need Attention",
                              "value": 2
                          },
                          "hours_allocated": {
                              "label": "Visits Need Approval",
                              "value": 4
                          }
                      },

                      "unknown_calls": {
                          "title": "Unknown Calls",
                          "headers": {
                              "date": { "label": "Date", "show": true },
                              "service": { "label": "Service", "show": true },
                              "employee": { "label": "Employee", "show": true },
                              "time_in": { "label": "In", "show": true },
                              "time_out": { "label": "Out", "show": true },
                              "hours": { "label": "Hours", "show": true },
                              "assign_to": { "label": "Assign To", "show": true },
                              "actions": { "label": "Actions", "show": true }
                          },
                          "data": [
                              {
                                  "date": "Sept 01, 2013",
                                  "service": "Service Name",
                                  "employee": "Employee Name",
                                  "time_in": "9:00am",
                                  "time_out": "2:00pm",
                                  "hours": 5,
                                  "assign_to": "Edward Jones",
                                  "actions": "Confirm",
                                  "show": true
                              },
                              {
                                  "date": "Sept 02, 2013",
                                  "service": "Service Name",
                                  "employee": "Bob Smith",
                                  "time_in": "9:00am",
                                  "time_out": "2:00pm",
                                  "hours": 5,
                                  "assign_to": "Edward Jones",
                                  "actions": "Confirm",
                                  "show": true
                              },
                              {
                                  "date": "Sept 03, 2013",
                                  "service": "Service Name",
                                  "employee": "Jane Doe",
                                  "time_in": "9:00am",
                                  "time_out": "2:00pm",
                                  "hours": 5,
                                  "assign_to": "Edward Jones",
                                  "actions": "Confirm",
                                  "show": true
                              },
                              {
                                  "date": "Sept 04, 2013",
                                  "service": "Service Name",
                                  "employee": "Susan Winters",
                                  "time_in": "9:00am",
                                  "time_out": "2:00pm",
                                  "hours": 5,
                                  "assign_to": "Edward Jones",
                                  "actions": "Confirm",
                                  "show": true
                              }
                          ]
                      },

                      "curr_auths": {
                          "title": "Current Authorizations",
                          "headers": {
                              "service": { "label": "Service", "show": true },
                              "start_date": { "label": "Start Date", "show": true },
                              "end_date": { "label": "End Date", "show": true },
                              "hrs_used": { "label": "Hours Used", "show": true },
                              "goto": { "label": "Go To", "show": true },
                              "remarks": { "label": "Remarks", "show": true }
                          },
                          "data": [
                                                                {
                                                                    "service": "Service Name",
                                                                    "start_date": "May 1, 2013",
                                                                    "end_date": "May 31, 2013",
                                                                    "hrs_used": {
                                                                        "start": "0",
                                                                        "median": "50",
                                                                        "end": "100",
                                                                        "value": "89",
                                                                        "status": "success"
                                                                    },
                                                                    "goto": "#",
                                                                    "remarks": {
                                                                        "code": "success",
                                                                        "label": "Active"
                                                                    },
                                                                    "details": {
                                                                        "reference": {
                                                                            "label": "Reference",
                                                                            "value": "Reference",
                                                                            "show": true
                                                                        },
                                                                        "units": {
                                                                            "label": "Units",
                                                                            "value": "Units",
                                                                            "show": true
                                                                        },
                                                                        "auth_type": {
                                                                            "label": "Auth Type",
                                                                            "value": "Auth Type",
                                                                            "show": true
                                                                        },
                                                                        "limit_type": {
                                                                            "label": "Limit Type",
                                                                            "value": "Limit Type",
                                                                            "show": true
                                                                        },
                                                                        "hours_used": {
                                                                            "per_year": {
                                                                                "total": {
                                                                                    "label": "Per Year Limited by Month",
                                                                                    "value": 600
                                                                                },
                                                                                "used": {
                                                                                    "label": "Hours Used",
                                                                                    "value": 96
                                                                                },
                                                                                "percentage": "16%"
                                                                            },
                                                                            "per_month": {
                                                                                "total": {
                                                                                    "label": "Per Month",
                                                                                    "value": 50
                                                                                },
                                                                                "used": {
                                                                                    "label": "Hours Used",
                                                                                    "value": 13
                                                                                },
                                                                                "percentage": "89%"
                                                                            }
                                                                        }
                                                                    }

                                                                },
                                                                 {
                                                                     "service": "Service Name",
                                                                     "start_date": "May 1, 2013",
                                                                     "end_date": "May 31, 2013",
                                                                     "hrs_used": {
                                                                         "start": "0",
                                                                         "median": "50",
                                                                         "end": "100",
                                                                         "value": "110",
                                                                         "status": "danger"
                                                                     },
                                                                     "goto": "#",
                                                                     "remarks": { "code": "important", "label": "Exceeded" },
                                                                     "details": {
                                                                         "reference": {
                                                                             "label": "Reference",
                                                                             "value": "Reference",
                                                                             "show": true
                                                                         },
                                                                         "units": {
                                                                             "label": "Units",
                                                                             "value": "Units",
                                                                             "show": true
                                                                         },
                                                                         "auth_type": {
                                                                             "label": "Auth Type",
                                                                             "value": "Auth Type",
                                                                             "show": true
                                                                         },
                                                                         "limit_type": {
                                                                             "label": "Limit Type",
                                                                             "value": "Limit Type",
                                                                             "show": true
                                                                         },
                                                                         "hours_used": {
                                                                             "per_year": {
                                                                                 "total": {
                                                                                     "label": "Per Year Limited by Month",
                                                                                     "value": 400
                                                                                 },
                                                                                 "used": {
                                                                                     "label": "Hours Used",
                                                                                     "value": 100
                                                                                 },
                                                                                 "percentage": "25%"
                                                                             },
                                                                             "per_month": {
                                                                                 "total": {
                                                                                     "label": "Per Month",
                                                                                     "value": 30
                                                                                 },
                                                                                 "used": {
                                                                                     "label": "Hours Used",
                                                                                     "value": 33
                                                                                 },
                                                                                 "percentage": "110%"
                                                                             }
                                                                         }
                                                                     }

                                                                 }
                          ]
                      }
                  }

              }
              ];
          }

      });

      $httpBackend.whenPOST('data/visits/fields').respond(function (method, url, data) {

          var fields = {
              clients: visits.clients,
              employees: visits.employees,
              payors: visits.payors
          };


          return [200, {
              authorized: authorized,
              fields: fields
          }
                ];
      });

      $httpBackend.whenPOST('data/visits/filters').respond(function (method, url, data) {

          var filters = {
              filters: visits.filters
          };


          return [200, {
              authorized: authorized,
              filters: filters
          }
                ];
      });

      /*$httpBackend.whenPOST('data/visits/addvisit').respond(function (method, url, data) {
          var obj = JSON.parse(data);
          obj.id = visits.grid.length + 1;
          visits.grid.push(obj);

          return [200, {
              authorized: authorized
          }
                ];
      });*/

      /*$httpBackend.whenPOST('data/visits/ignorevisit').respond(function (method, url, data) {

          var visit = {};
          jQuery.grep(visits.grid, function (passesObj, index) {
              if (passesObj.id == data) {
                  passesObj.remarks.label = "Ignored";
                  passesObj.remarks.code = "4";
                  visit = passesObj;
              }
          });

          return [200, {
              authorized: authorized,
              visit: visit
          }
                ];
      });*/

      /*$httpBackend.whenPOST('data/visits/unignorevisit').respond(function (method, url, data) {

          var visit = {};
          jQuery.grep(visits.grid, function (passesObj, index) {
              if (passesObj.id == data) {
                  passesObj.remarks.label = "";
                  passesObj.remarks.code = "5";
                  visit = passesObj;
              }
          });

          return [200, {
              authorized: authorized,
              visit: visit
          }
                ];
      });*/

      $httpBackend.whenPOST('data/visits/getapprovedtotalhours').respond(function (method, url, data) {

          var totalHoursApproved = 0;
          var arVisitIds = JSON.parse(data);
          jQuery.grep(visits.grid, function (passesObj, index) {
              if (arVisitIds.indexOf(passesObj.id) != -1) {
                  totalHoursApproved = totalHoursApproved + parseInt(passesObj.hours) || 0;
              }
          });

          return [200, {
              authorized: authorized,
              totalHoursApproved: totalHoursApproved
          }
                ];
      });

      /*$httpBackend.whenPOST('data/visits/approve').respond(function (method, url, data) {

          var totalHoursApproved = 0;
          var arVisitIds = JSON.parse(data);
          jQuery.grep(visits.grid, function (passesObj, index) {
              if (arVisitIds.indexOf(passesObj.id) != -1) {
                  passesObj.approved = "false";
                  passesObj.remarks.code = "3";
                  passesObj.remarks.label = "Approved";
                  totalHoursApproved = totalHoursApproved + parseInt(passesObj.hours);
              }
          });

          return [200, {
              authorized: authorized,
              totalHoursApproved: totalHoursApproved
          }
                ];
      });*/

      /*$httpBackend.whenPOST('data/visits/unapprove').respond(function (method, url, data) {

          var visit = {};
          jQuery.grep(visits.grid, function (passesObj, index) {
              if (passesObj.id == data) {
                  passesObj.remarks.label = "";
                  passesObj.remarks.code = "5";
                  visit = passesObj;
              }
          });

          return [200, {
              authorized: authorized,
              visit: visit
          }
                ];
      });*/

      $httpBackend.whenPOST('data/visits/getvisit').respond(function (method, url, data) {

          var visit = {};
          jQuery.grep(visits.grid, function (passesObj, index) {
              if (passesObj.id == data) {
                  visit = passesObj;
              }
          });

          return [200, {
              authorized: authorized,
              visit: visit
          }
                ];
      });

      $httpBackend.whenPOST('data/visits/edit').respond(function (method, url, data) {

          var obj = JSON.parse(data);

          jQuery.grep(visits.grid, function (passesObj, index) {
              if (passesObj.id == obj.id) {


                  if (
                                        obj.employee == 4 ||
                                        obj.date == "" ||
                                        obj["in"] == "" ||
                                        obj.out == ""

                                    ) {
                      obj.remarks = { "label": "Incomplete", "code": "2" }
                  } else if (
                                        obj.remarks.code == 2 &&
                                        obj.employee != 4 &&
                                        obj.date != "" &&
                                        obj["in"] != "" &&
                                        obj.out != ""

                                ) {
                      obj.remarks = { "label": "", "code": "5" }
                  } else {

                  }


                  passesObj.id = obj.id;
                  passesObj.client = obj.client;
                  passesObj.employee = obj.employee;
                  passesObj.payor = obj.payor;
                  passesObj.status = obj.status;
                  passesObj.date = obj.date;
                  passesObj.service = obj.service;
                  passesObj["in"] = obj["in"];
                  passesObj.out = obj.out;
                  passesObj.hours = obj.hours;
                  passesObj.remarks = obj.remarks;
                  passesObj.notes = obj.notes;
                  passesObj.reason_code = obj.reason_code;
                  if (obj.reasons) { passesObj.reasons.push(obj.reasons); }

              }
          });

          return [200, {
              authorized: authorized
          }
                ];

      });

      $httpBackend.whenPOST('data/visits/filter').respond(function (method, url, data) {

          // Use passed ( data  )variable to filter
          var grid = visits.grid;
          var columns = visits.grid_columns;
          var noOfPages = 1;
          var params = JSON.parse(data);

          if (params.fromDate && params.toDate) {
              grid = jQuery.grep(grid, function (passesObj, index) {
                  var passedDate = new Date(passesObj.date);
                  var filterFromDate = new Date(params.fromDate);
                  var filterToDate = new Date(params.toDate);
                  return passedDate >= filterFromDate && passedDate <= filterToDate; // retain appropriate elements
              });
          }

          if (params.client) {
              grid = jQuery.grep(grid, function (passesObj, index) {
                  return passesObj.client == params.client; // retain appropriate elements
              });
          }

          if (params.payor) {
              grid = jQuery.grep(grid, function (passesObj, index) {
                  return passesObj.payor == params.payor; // retain appropriate elements
              });
          }
          if (params.employee) {
              if (params.employee == 3) {
              } else if (params.employee == 4) {
                  grid = jQuery.grep(grid, function (passesObj, index) {
                      return passesObj.employee == ""; // retain appropriate elements
                  });
              } else {
                  grid = jQuery.grep(grid, function (passesObj, index) {
                      return passesObj.employee == params.employee; // retain appropriate elements
                  });
              }

          }

          if (params.ignoreVisits == "false") {
              grid = jQuery.grep(grid, function (passesObj, index) {
                  return passesObj.remarks.code != "4"; // retain appropriate elements
              });
          }

          if (params.incompleteVisit == "false") {
              grid = jQuery.grep(grid, function (passesObj, index) {
                  return passesObj.remarks.code != "2"; // retain appropriate elements
              });
          }
          if (params.visitApprovalReady == "false") {
              grid = jQuery.grep(grid, function (passesObj, index) {
                  return passesObj.remarks.code != "5"; // retain appropriate elements
              });
          }
          if (params.approvedVisit == "false") {
              grid = jQuery.grep(grid, function (passesObj, index) {
                  return passesObj.remarks.code != "3"; // retain appropriate elements
              });
          }



          if (params.page) {
              var from = params.page * 2;
              from = from - 2;
              var to = grid.length;
              var rest = to - from;
              if (rest != 1) {
                  to = from + 2;
              }

              noOfPages = grid.length / 2
              noOfPages = Math.ceil(noOfPages);
              //grid = grid.slice(from,to);

          } else {
              var to = grid.length;
              var rest = to - 1;
              if (rest != 1) {
                  to = 0 + 2;
              }
              noOfPages = grid.length / 2
              noOfPages = Math.ceil(noOfPages);
              // console.log(to)
              // grid = grid.slice(0,to);
          }

          var totalHours = 0;
          for (var i = 0; i < grid.length; i++) {
              totalHours = totalHours + parseInt(grid[i].hours) || 0;
          }

          var dataGrid = {
              noOfPages: noOfPages,
              totalHours: totalHours,
              grid: grid,
              "grid_columns": columns
          }
          //return data;

          return [200, {
              authorized: authorized,
              returnedData: dataGrid
          }
                ];
      });
      $httpBackend.whenGET('view/visits').respond(function (method, url, data) {

          // Use passed ( data  )variable to filter
          var grid = realVISTS;
          var columns = visits.grid_columns;
          var noOfPages = 1;
          var params = JSON.parse(data);
          /*
          if (params.fromDate && params.toDate) {
              grid = jQuery.grep(grid, function (passesObj, index) {
                  var passedDate = new Date(passesObj.date);
                  var filterFromDate = new Date(params.fromDate);
                  var filterToDate = new Date(params.toDate);
                  return passedDate >= filterFromDate && passedDate <= filterToDate; // retain appropriate elements
              });
          }

          if (params.client) {
              grid = jQuery.grep(grid, function (passesObj, index) {
                  return passesObj.client == params.client; // retain appropriate elements
              });
          }

          if (params.payor) {
              grid = jQuery.grep(grid, function (passesObj, index) {
                  return passesObj.payor == params.payor; // retain appropriate elements
              });
          }
          if (params.employee) {
              if (params.employee == 3) {
              } else if (params.employee == 4) {
                  grid = jQuery.grep(grid, function (passesObj, index) {
                      return passesObj.employee == ""; // retain appropriate elements
                  });
              } else {
                  grid = jQuery.grep(grid, function (passesObj, index) {
                      return passesObj.employee == params.employee; // retain appropriate elements
                  });
              }

          }

          if (params.ignoreVisits == "false") {
              grid = jQuery.grep(grid, function (passesObj, index) {
                  return passesObj.remarks.code != "4"; // retain appropriate elements
              });
          }

          if (params.incompleteVisit == "false") {
              grid = jQuery.grep(grid, function (passesObj, index) {
                  return passesObj.remarks.code != "2"; // retain appropriate elements
              });
          }
          if (params.visitApprovalReady == "false") {
              grid = jQuery.grep(grid, function (passesObj, index) {
                  return passesObj.remarks.code != "5"; // retain appropriate elements
              });
          }
          if (params.approvedVisit == "false") {
              grid = jQuery.grep(grid, function (passesObj, index) {
                  return passesObj.remarks.code != "3"; // retain appropriate elements
              });
          }

          */

          if (params.page) {
              var from = params.page * 2;
              from = from - 2;
              var to = grid.length;
              var rest = to - from;
              if (rest != 1) {
                  to = from + 2;
              }

              noOfPages = grid.length / 2
              noOfPages = Math.ceil(noOfPages);
              //grid = grid.slice(from,to);

          } else {
              var to = grid.length;
              var rest = to - 1;
              if (rest != 1) {
                  to = 0 + 2;
              }
              noOfPages = grid.length / 2
              noOfPages = Math.ceil(noOfPages);
              // console.log(to)
              // grid = grid.slice(0,to);
          }

          var totalHours = 0;
          for (var i = 0; i < grid.length; i++) {
              totalHours = totalHours + parseInt(grid[i].hours) || 0;
          }

          var dataGrid = {
              noOfPages: noOfPages,
              totalHours: totalHours,
              data: grid,
              "grid_columns": columns
          }
          //return data;

          return [200, {
              authorized: authorized,
              returnedData: dataGrid
          }
                ];
      });

      $httpBackend.whenPOST('data/menus').respond(function (method, url, data) {

          switch (data) {
              case "/reporting":
                  return [200, {
                      menu: {
                          "template": "app/includes/page-tabs.html",
                          "links": [
                                      {
                                          "link_href": "#",
                                          "link_class": "active",
                                          "link_title": "",
                                          "img_src": "style/img/calendar.gif",
                                          "img_alt": "Daily Reports",
                                          "title_class": "tab-link-title",
                                          "title": "Daily Reports"
                                      },
                                      {
                                          "link_href": "#",
                                          "link_class": "",
                                          "link_title": "",
                                          "img_src": "style/img/calendar-month.gif",
                                          "img_alt": "Calendar",
                                          "title_class": "tab-link-title",
                                          "title": "Date Range"
                                      },
                                      {
                                          "link_href": "#",
                                          "link_class": "",
                                          "link_title": "",
                                          "img_src": "style/img/lightbulb.gif",
                                          "img_alt": ">Business Intelligence",
                                          "title_class": "tab-link-title",
                                          "title": "Business Intelligence"
                                      },
                                      {
                                          "link_href": "#",
                                          "link_class": "",
                                          "link_title": "",
                                          "img_src": "style/img/person.gif",
                                          "img_alt": ">Business Intelligence",
                                          "title_class": "tab-link-title",
                                          "title": "Power User"
                                      }
                          ]
                      }
                  }];
                  break;
              case "header":
                  return [200, {
                      menu: {
                          "template": "/app/includes/header.html",
                          "links": [
                                       {
                                           "link_href": "#home",
                                           "link_class": "",
                                           "link_title": "Santrx Documentation",
                                           "img_src": "",
                                           "img_alt": "",
                                           "title_class": "",
                                           "title": ""
                                       },
                                       {
                                           "link_href": "#about",
                                           "link_class": "",
                                           "link_title": "Santrax Training Videos",
                                           "img_src": "",
                                           "img_alt": "",
                                           "title_class": "",
                                           "title": ""
                                       },
                                       {
                                           "link_href": "",
                                           "link_class": "",
                                           "link_title": "",
                                           "img_src": "style/img/profile-pic.jpg",
                                           "img_alt": "Profile Pic",
                                           "title_class": "",
                                           "title": ""
                                       },
                                       {
                                           "link_href": "#",
                                           "link_class": "",
                                           "link_title": "Hi Paul",
                                           "img_src": "",
                                           "img_alt": "",
                                           "title_class": "",
                                           "title": ""
                                       },
                                       {
                                           "link_href": "#/login",
                                           "link_class": "",
                                           "link_title": "login",
                                           "img_src": "",
                                           "img_alt": "",
                                           "title_class": "",
                                           "title": ""
                                       },
                                       {
                                           "link_href": "#/logout",
                                           "link_class": "",
                                           "link_title": "logout",
                                           "img_src": "",
                                           "img_alt": "",
                                           "title_class": "",
                                           "title": ""
                                       }
                          ]

                      }
                  }];
                  break;

              case "footer":
                  return [200, {
                      menu: {
                          "template": "app/includes/footer.html",
                          "links": [
                                      {
                                          "link_href": "#",
                                          "link_class": "",
                                          "link_title": "Terms of Use",
                                          "img_src": "",
                                          "img_alt": "",
                                          "title_class": "",
                                          "title": ""
                                      },
                                      {
                                          "link_href": "#",
                                          "link_class": "",
                                          "link_title": "About",
                                          "img_src": "",
                                          "img_alt": "",
                                          "title_class": "",
                                          "title": ""
                                      },
                                      {
                                          "link_href": "#",
                                          "link_class": "",
                                          "link_title": "Contact Us",
                                          "img_src": "",
                                          "img_alt": "",
                                          "title_class": "",
                                          "title": ""
                                      }

                          ]
                      }
                  }];
                  break;
          }

      });
      //otherwise

      $httpBackend.whenGET(/.*/).passThrough();
      $httpBackend.whenPOST(/.*/).passThrough();
      $httpBackend.whenPUT(/.*/).passThrough();
  });